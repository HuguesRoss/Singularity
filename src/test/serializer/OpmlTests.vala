using Xml;
using Singularity;

namespace Singularity.Tests {
    public class OpmlTests : UnitTest {
        public string name { get { return "OPML"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("parse_valid_opml", testParseValidOpml),
                TestCase ("generate_valid_opml", testGenerateValidOpml),
            });
        }

        private static void testParseValidOpml () {
            Doc* document = Parser.parse_doc (
"""
<opml version="2.0">
    <head></head>
    <body>
        <outline text="Test Feed" type="rss" xmlUrl="https://www.example.com/test.rss" description="This is a test feed" htmlUrl="https://example.com" title="Test Feed"/>
        <outline text="Category">
            <outline text="Test Feed" type="rss" xmlUrl="https://www.example.com/test.rss" description="This is a test feed" htmlUrl="https://example.com" title="Test Feed 2"/>
        </outline>
    </body>
</opml>
""");

            var parser = new OPMLFeedDataSource ();
            assert (parser.parse_data (document));
            assert (parser.data.size == 2);

            var feed = parser.data[0] as Feed;
            assert (feed != null);
            assert (feed.title == "Test Feed");
            assert (feed.description == "This is a test feed");
            assert (feed.link == "https://www.example.com/test.rss");
            assert (feed.site_link == "https://example.com");

            var collection = parser.data[1] as FeedCollection;
            assert (collection != null);
            assert (collection.title == "Category");

            var feeds = collection.get_feeds ();
            assert (feeds.size == 1);
            assert (feeds[0].title == "Test Feed 2");
        }

        private static void testGenerateValidOpml () {
            var root = new FeedCollection.root ();
            root.add_node (new Feed () {
                title = "Test Feed",
                description = "This is a test",
                link = "https://www.example.com/test.rss",
                site_link = "https://example.com",
            });

            var collection = new FeedCollection ("Category");
            collection.add_node (new Feed () {
                title = "Test Feed 2",
                description = "This is a test",
                link = "https://www.example.com/test.rss",
                site_link = "https://example.com",
            });
            root.add_node (collection);

            var parser = new OPMLFeedDataSource ();
            Doc* document = parser.encode_data (root.nodes);
            assert (document != null);

            Xml.Node* root_node = document->get_root_element ();
            assert (root_node != null);
            assert (root_node->name == "opml");

            Xml.Node* child = root_node->children;
            assert (child != null);
            assert (child->name == "head");
            child = child->next;
            assert (child != null);
            assert (child->name == "body");

            child = child->children;
            assert (child != null);
            assert (child->name == "outline");
            assert (child->get_prop ("type") == "rss");
            assert (child->get_prop ("text") != null);
            assert (child->get_prop ("title") == "Test Feed");
            assert (child->get_prop ("description") == "This is a test");
            assert (child->get_prop ("xmlUrl") == "https://www.example.com/test.rss");
            assert (child->get_prop ("htmlUrl") == "https://example.com");

            child = child->next;
            assert (child != null);
            assert (child->name == "outline");
            assert (child->get_prop ("text") == "Category");

            child = child->children;
            assert (child != null);
            assert (child->name == "outline");
            assert (child->get_prop ("type") == "rss");
            assert (child->get_prop ("text") != null);
            assert (child->get_prop ("title") == "Test Feed 2");
            assert (child->get_prop ("description") == "This is a test");
            assert (child->get_prop ("xmlUrl") == "https://www.example.com/test.rss");
            assert (child->get_prop ("htmlUrl") == "https://example.com");
        }
    }
}
