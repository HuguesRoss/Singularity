using Sqlite;

namespace Singularity.Db {
    /** Represents a SQLite database */
    public class Database : Object, IDbTarget {
        /** Is the database connection open? */
        public bool is_open { get; private set; }

        /** Are there any pending requests to process? */
        public bool pending_requests { get {
            foreach (RequestProcessor p in _processors) {
                if (p.requests.length () > 0) {
                    return true;
                }
            }

            return false;
        }}

        /** The path to the database on disk */
        public string filepath { get; construct set; }

        internal Sqlite.Database db { get { return _db; } }
        private Sqlite.Database _db;

        public Database.from_path (string path, string schema_uri) {
            Object (filepath: path);

            Sqlite.Database.open_v2 (path, out _db, Sqlite.OPEN_READWRITE | Sqlite.OPEN_CREATE);

            // Check the DB version
            int version = get_version ();

            // If the version is 0, update the DB
            if (version == 0) {
                message ("Initializing database at %s\u2026", path);
                init_schema (schema_uri);
            }

            is_open = true;

            // If we're out of date, update the schema
            if (update_schema_version (schema_uri, version == 0) && version != 0) {
                message ("Updated Database version to %d\u2026", get_version ());
            }

            for (int i = 0; i < IRequest.Priority.COUNT; ++i) {
                _processors[i] = new RequestProcessor (_db_mutex, this);
            }
        }


        /**
         * Queue a request for the database
         *
         * @param req The request to queue
         * @param prio The request's priority. Higher-priority requests will skip lower-priority ones
         */
        public void queue_request (IRequest req, IRequest.Priority prio = IRequest.Priority.DEFAULT) {
            _processors[prio].requests.push (req);
        }

        /**
         * Queues a request, then yields until finished
         *
         * @param req The request to queue
         * @param prio The request's priority. Higher-priority requests will skip lower-priority ones
         */
        public async void execute_request_async (IRequest req, IRequest.Priority prio = IRequest.Priority.DEFAULT) {
            SourceFunc func = execute_request_async.callback;
            req.processing_complete.connect (() => {Idle.add ((owned)func);});
            queue_request (req, prio);

            yield;
        }

        /**
         * Verify and attempt to sequentially update the database schema version, if needed
         *
         * @param schema_uri The base uri where schema code is stored
         * @param first_run Are we running this on a fresh database?
         */
        private bool update_schema_version (string schema_uri, bool first_run) {
            // Compare db version with supported version
            int version = get_version ();
            int supported_version = 1;
            for (; ; supported_version++) {
                var script = File.new_for_uri ("%s/Update-to-%d.sql".printf (schema_uri, supported_version));
                if (!script.query_exists ()) {
                    supported_version--;
                    break;
                }
            }

            // Ensure that the version is less or equal to the supported version
            if (version > supported_version) {
                error ("Database version check failed! Database is version %d, but Singularity expected version %d or earlier.", version, supported_version);
            } else if (version == supported_version) {
                return false;
            }

            // If we reach this point, we know that the database needs to update

            // Create a backup copy of the db in /tmp
            File? db_file = File.new_for_path (filepath);
            File? backup = null;
            if (!first_run) {
                try {
                    FileIOStream _;
                    backup = File.new_tmp ("%s-XXXXXX.sqlite".printf (db_file.get_basename ()), out _);
                    db_file.copy (backup, FileCopyFlags.OVERWRITE);
                } catch (Error e) {
                    error ("Failed to perform pre-update database backup: %s", e.message);
                }
            }

            // Attempt to update
            for (version = version + 1; version <= supported_version; version++) {
                try {
                    if (load_and_run ("%s/Update-to-%d.sql".printf (schema_uri, version))) {
                        set_version (version);
                    } else {
                        throw new DbError.SCRIPT_LOAD_FAILED ("Failed to load version update script");
                    }
                } catch (GLib.Error e) {
                    is_open = false;

                    if (backup != null) {
                        // Try to restore a backup
                        try {
                            backup.move (db_file, FileCopyFlags.OVERWRITE);
                        } catch (Error e2) {
                            error ("Failed to update database to version %d: %s\nAdditionally, the automatic restore process failed: %s\nA backup of your old database may exist at [%s]\u2026 check it immediately!", version, e.message, e2.message, backup.get_path ());
                        }
                    } else if (first_run) {
                        try {
                            // Delete the invalid DB we created
                            db_file.@delete ();
                        } catch (Error e2) {
                            // We don't care much if this fails
                        }
                    }

                    // If we hit this, the backup was successful. Just print the original error.
                    error ("Failed to update database to version %d: %s", version, e.message);
                }
            }

            return true;
        }

        /**
         * Run the initial database schema creation script
         *
         * @param schema_uri The base uri where schema code is stored
         */
        private void init_schema (string schema_uri) {
            try {
                if (!load_and_run (schema_uri + "/Create.sql")) {
                    error ("Cannot initialize database schema: Failed to load %s/Create.sql", schema_uri);
                }
            } catch (GLib.Error e) {
                error ("Cannot initialize database schema: %s", e.message);
            }
        }

        /**
         * Load and run a script resource
         *
         * @param uri A uri pointing to the resource
         */
        private bool load_and_run (string uri) throws GLib.Error {
            File script = File.new_for_uri (uri);

            // Ensure that the script exists
            if (!script.query_exists ()) {
                return false;
            }

            // Load the contents of the data...
            uint8[] data;
            if (!script.load_contents (null, out data, null)) {
                return false;
            }

            // ...and execute
            exec ((string)data);

            return true;
        }

        internal void exec (string data, Sqlite.Callback? callback = null) throws DbError {
            string errmsg;
            _db.exec ((string)data, callback, out errmsg);
            if (errmsg != null) {
                throw new DbError.SCRIPT_EXECUTE_FAILED ("Failed to execute script\n%s\n%s", data, errmsg);
            }
        }

        private int get_version () {
            int version = -1;
            _db.exec ("PRAGMA user_version", (cols, values, names) => {
                version = int.parse (values[0]);
                return version;
            });

            return version;
        }

        private void set_version (int version) {
            _db.exec ("PRAGMA user_version = %d".printf (version));
        }

        private RequestProcessor[] _processors = new RequestProcessor[IRequest.Priority.COUNT] {};
        private GLib.Mutex _db_mutex = GLib.Mutex ();

        /** Calls database requests using a provided handle and mutex */
        private class RequestProcessor : Object, IDbTarget {
            /** The pending request queue for this processor */
            public AsyncQueue<IRequest> requests { get; set; }

            internal Sqlite.Database db { get { return _db.db; } }
            private Database _db;

            public RequestProcessor (GLib.Mutex m, Database db) {
                requests = new AsyncQueue<IRequest> ();
                processing_thread = new Thread<void*> (null, process);
                _db_mutex = m;
                _db = db;
            }

            /** Thread worker for processing requests */
            private void* process () {
                while (true) {
                    IRequest req = requests.pop ();
                    _db_mutex.lock ();
                    // Run request steps until it's done
                    if (req.use_transaction) {
                        try {
                            TransactionDbTarget transaction = new TransactionDbTarget (_db);
                            var status = IRequest.Status.CONTINUE;
                            while (status == IRequest.Status.CONTINUE) {
                                status = req.step (transaction);
                            }
                            if (status == IRequest.Status.COMPLETED) {
                                transaction.commit ();
                            } else {
                                transaction.rollback ();
                            }
                        } catch (DbError e) {
                            error (e.message);
                        }
                    } else {
                        while (req.step (this) == IRequest.Status.CONTINUE);
                    }
                    _db_mutex.unlock ();

                    req.processing_complete ();
                }
            }

            private Thread<void*> processing_thread;
            private unowned GLib.Mutex _db_mutex;
        }
    }
}
