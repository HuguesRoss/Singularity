using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/DownloadEntry.ui")]
    public class DownloadEntry : Widget {
        public Download download {
            get { return _download; }
            set {
                if (_download != value) {
                    if (_download != null) {
                        _download.notify["status"].disconnect (on_status_changed);
                    }

                    _download = value;

                    if (_download != null) {
                        _download.notify["status"].connect (on_status_changed);
                    }

                    on_status_changed ();
                    notify_property ("download");
                }
            }
        }
        private Download _download;

        public string status_icon {
            get {
                if (_download != null) {
                    switch (_download.status) {
                        case DownloadStatus.PENDING:
                            return "image-loading-symbolic";
                        case DownloadStatus.IN_PROGRESS:
                            return "";
                        case DownloadStatus.COMPLETED:
                            return "emblem-ok-symbolic";
                        case DownloadStatus.FAILED:
                            return "dialog-error-symbolic";
                        case DownloadStatus.CANCELLED:
                            return "process-stop-symbolic";
                    }
                }

                return "image-loading-symbolic";
            }
        }
        public string status_label {
            get {
                if (_download != null) {
                    switch (_download.status) {
                        case DownloadStatus.PENDING:
                            return "Pending…";
                        case DownloadStatus.IN_PROGRESS:
                            return "";
                        case DownloadStatus.COMPLETED:
                            return "Download Complete!";
                        case DownloadStatus.FAILED:
                            return "Download Failed";
                        case DownloadStatus.CANCELLED:
                            return "Cancelled";
                    }
                }

                return "Pending…";
            }
        }

        public Label label_widget { get { return status_widget; }}

        public bool has_action { get; private set; default = true; }
        public string action_icon { owned get; private set; default = "process-stop-symbolic"; }
        public string action_name { owned get; private set; default = "cancel"; }
        public string action_tooltip { owned get; private set; default = "<b>Cancel Download</b>"; }

        static construct {
            install_action ("cancel", null, w => {
                if (((DownloadEntry)w).download != null) {
                    ((DownloadEntry)w).download.cancel ();
                }
            });
            install_action ("open", null, w => {
                if (((DownloadEntry)w).download != null) {
                    var launcher = new FileLauncher (((DownloadEntry)w).download.destination);
                    launcher.launch.begin (w.get_root () as Window, null);
                }
            });
        }
        construct {
        }

        ~DownloadEntry () {
            WidgetUtils.cleanup_children (this);
        }

        private void on_status_changed () {
            notify_property ("status_icon");
            notify_property ("status_label");

            if (download == null || download.status == DownloadStatus.CANCELLED) {
                add_css_class ("dim-label");
                has_action = false;
            } else {
                remove_css_class ("dim-label");
                if (download.status == DownloadStatus.COMPLETED) {
                    action_icon = "document-open-symbolic";
                    action_name = "open";
                    action_tooltip = "<b>Open Downloaded File</b>";
                    status_widget.add_css_class ("success");
                    status_icon_widget.add_css_class ("success");
                } else if (download.status == DownloadStatus.FAILED) {
                    status_widget.add_css_class ("error");
                    status_icon_widget.add_css_class ("error");
                }
            }
        }

        [GtkChild]
        private unowned Image status_icon_widget;
        [GtkChild]
        private unowned Label status_widget;
    }
}
