using Gee;

namespace Singularity {
    /** Constructs HTML for the column view */
    public class ColumnViewBuilder : ViewBuilder, GLib.Object {
        public ColumnViewBuilder (IconCssBuilder icon_builder) {
            try {
                string default_css = resource_to_string ("Views/WebView/default.css");
                string column_css = resource_to_string ("Views/WebView/column.css");
                _iframe_header_noscript = "<style>%s</style>".printf (
                    resource_to_string ("Views/WebView/iframe.css")
                    );
                _iframe_header = "%s<script>%s</script>".printf (
                    _iframe_header_noscript,
                    UserScriptBuilder.load_source_from_resource ("resource://%s/Views/WebView/InnerIframeResizer.js".printf (APP_ID_PATH))
                    );
                style = "<style>\n%s\n%s\n%s</style>".printf (
                    icon_builder.get_css (),
                    default_css,
                    column_css
                    );
            } catch (Error e) {
                warning ("Failed to read style information: %s", e.message);
            }
        }

        public string build_page_html (Gee.List<Item> items) {
            var style_manager = Adw.StyleManager.get_default ();

            StringBuilder builder = new StringBuilder ();
            builder.append_printf ("<html %s><head>", style_manager.high_contrast ? "class=\"highcontrast\"" : "");
                try {
                    builder.append_printf ("<style>%s</style>", resource_to_string (style_manager.dark ? "Views/WebView/colors-dark.css" : "Views/WebView/colors-light.css"));
                } catch (Error e) {
                    warning ("Failed to read style information");
                }
                builder.append (style);
                builder.append ("</head>");

            if (items.size == 0) {
                builder.append ("</html>");
                return builder.str;
            }

            builder.append_printf ("<body onload=\"prepare ()\">%s</body></html>", build_item_html (items[0], 0));

            return builder.str;
        }

        private string build_item_html (Item item, int id) {
            StringBuilder footer_builder = new StringBuilder ("<footer>");
            if (item.attachments.size > 0) {
                footer_builder.append ("<section class=\"attachments-list\">");
                foreach (Attachment a in item.attachments) {
                    footer_builder.append (WebElementBuilders.build_attachment (a, item));
                }
                footer_builder.append ("</section>");
            }
            // TODO: Tags
            footer_builder.append_printf ("</footer>");

            StringBuilder builder = new StringBuilder ();
            builder.append_printf ("<article class=\"%s\" data-id=\"%d\" data-read=\"%s\" data-starred=\"%s\">",
                BUILDER_CLASS + " singularity-article",
                id,
                item.unread ? "false" : "true",
                item.starred ? "true" : "false");
            builder.append (WebElementBuilders.build_post_header (item, true));
            builder.append ("<hr/>");
            builder.append ("<section class=\"content\">");
            if (!StringUtils.null_or_empty (item.content)) {
                StringBuilder style_builder = new StringBuilder ();
                var style_manager = Adw.StyleManager.get_default ();
                try {
                    style_builder.append_printf ("<style>%s</style>", resource_to_string (style_manager.dark ? "Views/WebView/colors-dark.css" : "Views/WebView/colors-light.css"));
                } catch (Error e) {
                    warning ("Failed to read style information");
                }

                if (AppSettings.enable_javascript) {
                    style_builder.append_printf (_iframe_header, id);
                } else {
                    style_builder.append (_iframe_header_noscript);
                }

                string sandbox = AppSettings.enable_javascript
                    ? "sandbox=\"allow-scripts\""
                    : "sandbox=\"allow-same-origin\"";

                string content = WebElementBuilders.escape_srcdoc (WebElementBuilders.build_post_content (item, id, style_builder.str, style_manager.high_contrast));
                builder.append_printf ("<iframe class=\"content-frame\" %s credentialless src=\"%s\" srcdoc=\"%s\" onload=\"postLoadResizeIframe(this)\"></iframe>", sandbox, item.link, content);
            }
            builder.append ("</section>");
            builder.append (footer_builder.str);
            builder.append ("</article>");
            ++id;

            return builder.str;
        }

        private string style = "";
        private string _iframe_header = "";
        private string _iframe_header_noscript = "";
        private const string BUILDER_CLASS = "column";
    }
}
