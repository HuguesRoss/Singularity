using Xml;
using Singularity;
using WebFeed;

namespace Singularity.Tests {
    public class AtomTests : UnitTest {
        public string name { get { return "Atom"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("parse_valid_atom", testParseValidAtom),
                TestCase ("parse_atom_xhtml", testParseAtomXhtml),
            });
        }

        private static void testParseValidAtom () {
                // Excerpt of feed generated from author's blog
            IDocument document;
            assert (XmlDocument.create (
                "http://example.com",
"""<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" >
    <generator uri="https://jekyllrb.com/" version="4.2.0">Jekyll</generator>
    <link href="https://www.huguesross.net/feed.xml" rel="self" type="application/atom+xml" />
    <link href="https://www.huguesross.net/" rel="alternate" type="text/html" />
    <updated>2021-12-30T15:11:12+00:00</updated>
    <id>https://www.huguesross.net/feed.xml</id>
    <title type="html">Hugues Ross</title>
    <subtitle>The gallery site and blog of Hugues Ross, hobbyist and professional AAA Tools Programmer. Here you can find his art gallery, code projects, and discussions thereof.</subtitle>
    <entry>
        <title type="html">Website Update: Sidebars for Everybody!</title>
        <link href="https://www.huguesross.net/2021/12/sidebars-for-everybody.html" rel="alternate" type="text/html" title="Website Update: Sidebars for Everybody!" />
        <published>2021-12-30T00:00:00+00:00</published>
        <updated>2021-12-30T00:00:00+00:00</updated>
        <id>https://www.huguesross.net/2021/12/sidebars-for-everybody</id>
        <author>
        <name>Hugues Ross</name>
        </author>
        <summary type="html">About a year ago, I relaunched this site with a completely new layout. Today, we have a less ambitious but still important update!</summary>
    </entry>
</feed>
""", out document));

            var parser = new AtomFeedParser () {
                data = document,
            };
            assert (parser.parse_data ());

            WebFeed.Feed result = parser.feed;
            assert (result != null);

            assert (result.generator == "Jekyll");
            assert (result.title == "Hugues Ross");
            assert (result.description == "The gallery site and blog of Hugues Ross, hobbyist and professional AAA Tools Programmer. Here you can find his art gallery, code projects, and discussions thereof.");
            assert (result.feed_uri == "https://www.huguesross.net/feed.xml");
            assert (result.site_link == "https://www.huguesross.net/");

            WebFeed.Item[] items = parser.items.to_array ();
            assert (items.length == 1);

            WebFeed.Item item = items[0];
            assert (item.title == "Website Update: Sidebars for Everybody!");
            assert (item.uri == "https://www.huguesross.net/2021/12/sidebars-for-everybody.html");
            assert (item.content == "About a year ago, I relaunched this site with a completely new layout. Today, we have a less ambitious but still important update!");
            assert (item.authors.size == 1);
            assert (item.authors.any_match (a => a.name == "Hugues Ross"));
            assert (item.time_published.equal (new DateTime.utc (2021, 12, 30, 0, 0, 0)));
            assert (item.time_updated.equal (new DateTime.utc (2021, 12, 30, 0, 0, 0)));
        }

        private static void testParseAtomXhtml () {
            IDocument document;
            assert (XmlDocument.create (
                "http://example.com",
"""<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" >
    <id>https://www.example.com/feed.xml</id>
    <title>Test Feed</title>
    <entry>
        <title>Test Item</title>
        <id>https://www.example.com</id>
        <summary type="xhtml"><p>Test paragraph <a href="http://www.example.com">Test Link</a></p><p>2nd paragraph</p></summary>
    </entry>
</feed>
""", out document));

            var parser = new AtomFeedParser () {
                data = document,
            };
            assert (parser.parse_data ());

            WebFeed.Feed result = parser.feed;
            assert (result != null);

            WebFeed.Item[] items = parser.items.to_array ();
            assert (items.length == 1);

            WebFeed.Item item = items[0];
            assert (item.content == """<p>Test paragraph <a href="http://www.example.com">Test Link</a></p><p>2nd paragraph</p>""");
        }
    }
}
