using Singularity.Db;

namespace Singularity {
    /** DatabaseRequest for loading all feeds and collections */
    public class LoadFeedsRequest : SimpleRequest {
        public FeedCollection feeds { get; private set; default = new FeedCollection.root (); }

        construct {
            _name = "Load feeds";
        }

        public override Statement build_query (IDbTarget db) {
            return new Statement (db, QUERIES[request_index]);
        }

        public override IRequest.Status process_result (IDbResult res) {
            if (request_index == Status.FEEDS) {
                int type_column = res.find_column ("type");

                for (; !res.finished; res.next ()) {
                    FeedDataEntry n = null;
                    switch (res.fetch_int (type_column)) {
                        case FeedDataEntry.Contents.FEED:
                            n = new Feed.from_record (res);
                        break;
                        case FeedDataEntry.Contents.COLLECTION:
                            n = new FeedCollection.from_record (res);
                        break;
                    }
                    m_node_map[n.id] = n;
                    m_node_list.add (n);
                }

                foreach (FeedDataEntry n in m_node_list) {
                    if (n.parent_id == -1) {
                        feeds.add_node (n);
                    } else {
                        ((FeedCollection)m_node_map[n.parent_id]).add_node (n);
                    }
                }
            } else { // if (request_index == Status.AUTHORS) {
                for (; !res.finished; res.next ()) {
                    Person author = new Person.from_record (res);
                    int id = res.get_int ("feed_id");
                    FeedDataEntry node = m_node_map[id];
                    var feed = node as Feed;
                    if (feed != null) {
                        feed.authors.add (author);
                    }
                }
            }

            if (request_index >= LAST_STATUS)
                return IRequest.Status.COMPLETED;

            return IRequest.Status.CONTINUE;
        }

        private Gee.HashMap<int, FeedDataEntry> m_node_map = new Gee.HashMap<int, FeedDataEntry> ();
        private Gee.List<FeedDataEntry> m_node_list = new Gee.ArrayList<FeedDataEntry> ();

        private const string[] QUERIES = {
            // Get all feeds, along with their icons and unread counts
            """SELECT feeds.*, sum (items.unread) AS unread_count FROM feeds
                LEFT OUTER JOIN items ON items.parent_id = feeds.id
                GROUP BY feeds.id
                ORDER BY feeds.id""",
            // Get all feed authors matched to their feed's ID
            "SELECT feed_id,people.* FROM feeds INNER JOIN feed_authors ON feeds.id = feed_authors.feed_id INNER JOIN people ON people.guid = feed_authors.author_id",
        };
        private enum Status {
            FEEDS = 0,
            AUTHORS,
        }
        const Status LAST_STATUS = Status.AUTHORS;
    }
}
