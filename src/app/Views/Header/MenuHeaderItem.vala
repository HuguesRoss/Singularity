using Gtk;

namespace Singularity {
    public class MenuHeaderItem : Object, IHeaderItem {
        public MenuModel menu_model { get; construct set; }
        public string icon_name { owned get; set; }
        public string tooltip_markup { owned get; set; }
        public bool suggested { get; set; }
        public bool minor { get; set; }

        public MenuHeaderItem (MenuModel model) {
            Object (menu_model: model);
        }

        public Widget build_widget () {
            var button = new MenuButton () {
                menu_model = menu_model,
                icon_name = icon_name,
                tooltip_markup = tooltip_markup,
            };

            if (!minor) {
                button.add_css_class ("raised");
            }
            if (suggested) {
                button.add_css_class ("suggested-action");
            }

            return button;
        }
    }
}
