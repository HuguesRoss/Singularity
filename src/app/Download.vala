namespace Singularity {
    public enum DownloadStatus {
        PENDING,
        IN_PROGRESS,
        COMPLETED,
        FAILED,
        CANCELLED,
    }

    public sealed class Download : Object {
        public string filename { get; private set; default = ""; }
        public File destination { get; private set; }
        public double progress { get { return _download.estimated_progress; } }
        public DownloadStatus status {
            get { return _status; }
            private set {
                if (_status != value) {
                    _status = value;
                    notify_property ("status");
                }
            }
        }
        private DownloadStatus _status = DownloadStatus.PENDING;
        public bool active { get; set; default = true; }
        public string icon_name { get; set; default = "text-x-generic-symbolic"; }

        public Download (WebKit.Download download) {
            _download = download;
            _download.finished.connect (on_download_finished);
            _download.failed.connect (on_download_failed);
            _download.received_data.connect (on_progress_changed);
            _download.created_destination.connect (on_destination_changed);
        }

        ~Download () {
            _download.finished.disconnect (on_download_finished);
            _download.failed.disconnect (on_download_failed);
            _download.received_data.disconnect (on_progress_changed);
            _download.created_destination.disconnect (on_destination_changed);
        }

        public void cancel () {
            _download.cancel ();
        }

        private void on_download_failed (WebKit.DownloadError error) {
            if (error.code == WebKit.DownloadError.CANCELLED_BY_USER) {
                status = DownloadStatus.CANCELLED;
            } else {
                warning ("Download of %s to %s failed", _download.response.uri, _download.destination);
                status = DownloadStatus.FAILED;
            }

            active = false;
        }

        private void on_download_finished () {
            // This is called after failure as well--if we've already
            // handled the active state, don't mark as done.
            if (!active) {
                return;
            }

            message ("Download of %s to %s finished", _download.response.uri, _download.destination);
            status = DownloadStatus.COMPLETED;
            active = false;
        }

        private void on_progress_changed () {
            if (_download.response.mime_type != _mime_type) {
                _mime_type = _download.response.mime_type;
                icon_name = MimeTypeInfo.get_icon (_mime_type);
            }
            status = DownloadStatus.IN_PROGRESS;
            notify_property ("progress");
        }

        private void on_destination_changed (string dest_path) {
            destination = File.new_for_path (dest_path);
            string? new_name = destination.get_basename ();
            filename = new_name ?? filename;
        }

        private WebKit.Download _download;
        private string? _mime_type = null;
    }
}
