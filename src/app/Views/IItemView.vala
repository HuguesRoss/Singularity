using Gtk;

namespace Singularity {
    // Interface for widgets that display items
    public interface IItemView : Widget {
        public abstract string title { get; protected set; }
        public abstract string empty_description { get; protected set; }
        public abstract IconCssBuilder css_icons { get; set; }

        /**
         * Display the result of a search
         *
         * @param results The result object serving the items to display
         */
        public abstract void view_items (SearchResults results);

        /**
         * Focus this view
         */
        public abstract void focus_view ();

        /**
         * Refresh the content of this view
         */
        public abstract void refresh_view ();

        public void set_node_info (IFeedListNode node, ItemQuery query) {
            title = node.title;
            empty_description = query.unread_only
                ? "You've read everything in this %s"
                : "This %s is empty";
            empty_description = empty_description.printf (node.type_name);
        }

        /**
         * Mark all visible items as read
         */
        public abstract void read_all ();

        protected static async Gee.Collection<Item> get_items (SearchResults results, int cursor) {
            if (results == null) {
                return Gee.Collection.empty<Item> ();
            }

            return yield results.get_items (AppSettings.items_per_list, cursor);
        }

        public signal void items_viewed (Item[] i);
        public signal void item_read_toggle (Item i);
        public signal void item_star_toggle (Item i);
    }
}
