namespace Singularity {
    /** Helper class for URIs */
    public class UriUtils {
        /**
         * Try to parse a URI from a string
         *
         * @param u The uri string to parse
         * @param uri Contains the resulting URI, if successful
         *
         * @return Whether or not the URI could be parsed
         */
        public static bool try_parse (string u, out Uri uri) {
            uri = null;
            if (u == null) {
                return false;
            }

            try {
                uri = Uri.parse (u, UriFlags.ENCODED | UriFlags.PARSE_RELAXED);
                return true;
            } catch (UriError e) {
                return false;
            }
        }

        /**
         * Transform a uri string to match the output of GLib's Uris
         *
         * @param u The URI string
         *
         * @return A transformed URI string
         */
        public static string make_canon (string u) {
            Uri uri;
            if (!try_parse (u, out uri)) {
                return u;
            }
            return uri.to_string ();
        }

        /**
         * Extract the base from an absolute URI
         *
         * @param u The URI to parse
         * @param keep_path Whether or not to keep the non-file path segments of the URI
         *
         * @return The base URI, or "" if the input URI is invalid
         */
        public static string get_base (string u, bool keep_path = false) {
            try {
                string? scheme;
                string? userinfo;
                string? host;
                int port;
                string? path;
                if (Uri.split (u, UriFlags.ENCODED | UriFlags.PARSE_RELAXED, out scheme, out userinfo, out host, out port, out path, null, null)) {
                    if (!keep_path) {
                        string res = Uri.join (UriFlags.ENCODED | UriFlags.PARSE_RELAXED, scheme, userinfo, host, port, "", null, null);
                        if (!res.has_suffix ("/")) {
                            return res + "/";
                        }
                        return res;
                    } else {
                        if (!StringUtils.null_or_empty (path)
                                // Ends in a directory
                                && !path.has_suffix ("/")
                                // *Probably* a relative path segment such as . or ..
                                && !path.has_suffix (".")) {
                            int last_slash = path.last_index_of_char ('/');
                            string last_seg = (path.substring (last_slash + 1, -1));
                            if (last_seg.contains (".")) {
                                // Strip the filename if it exists
                                path = path.substring (0, last_slash) + "/";
                            }
                        }
                        string res = Uri.join (UriFlags.ENCODED | UriFlags.PARSE_RELAXED, scheme, userinfo, host, port, path, null, null);
                        if (!res.has_suffix ("/")) {
                            return res + "/";
                        }
                        return res;
                    }
                }
            } catch (UriError e) {
            }

            return "";
        }

        /**
         * Extract the scheme from an absolute URI
         *
         * @param u The URI to parse
         *
         * @return The URI scheme, or "" if it could not be extracted
         */
        public static string get_scheme (string u) {
            Uri uri;
            if (!try_parse (u, out uri)) {
                return "";
            }

            return uri.get_scheme ();
        }

        /**
         * Ensure that a URI is absolute, prepending the given base if needed
         *
         * @param u The URI to convert
         * @param base_uri The base URI
         *
         * @return An absolute URI
         */
        public static string? make_absolute (string u, string base_uri) {
            // Special case: null doesn't convert to anything, it's the absence of a URI
            if (u == null) {
                return null;
            }

            Uri uri;
            if (try_parse (u, out uri)) {
                return u;
            }

            Uri base_parsed;
            if (!try_parse (base_uri, out base_parsed)) {
                // Abort, the base is invalid
                return u;
            }

            if (u.has_prefix (".")) {
                string relative = u;
                string relative_path = base_parsed.get_path ();
                do {
                    if (relative.has_prefix ("../")) {
                        if (relative.length > 3) {
                            relative = relative.substring (3);
                        } else {
                            relative = "";
                        }
                        int index = relative_path.last_index_of_char ('/');
                        if (index != -1) {
                            relative_path = relative_path.substring (0, index + 1);
                        }
                    } else if (relative.has_prefix ("./")) {
                        if (relative.length > 2) {
                            relative = relative.substring (2);
                        } else {
                            relative = "";
                        }
                    } else if (relative.has_prefix ("/")) {
                        if (relative.length > 1) {
                            relative = relative.substring (1);
                        } else {
                            relative = "";
                        }
                    } else {
                        // Give up, this is invalid
                        return u;
                    }
                } while (relative.has_prefix (".") || relative.has_prefix ("/"));

                string? scheme;
                string? userinfo;
                string? host;
                int port;
                string? path;
                try {
                    if (Uri.split (base_uri, UriFlags.ENCODED | UriFlags.PARSE_RELAXED, out scheme, out userinfo, out host, out port, out path, null, null)) {
                        if (!relative_path.has_suffix ("/")) {
                            relative_path += "/";
                        }
                        path = "%s%s".printf (relative_path, relative);
                        return Uri.join (UriFlags.ENCODED | UriFlags.PARSE_RELAXED, scheme, userinfo, host, port, path, null, null);
                    }
                } catch (UriError e) { }
            }

            // Special case: Starting with ./, we need to preserve the base uri's path

            // Special case 2: Starting with ../, we need to preserve the base uri's path but go backwards...
            // FIXME: What about .././
            if (u.has_prefix ("./")) {
                return make_canon (base_uri + u.substring (1));
            }

            // Handle URIs missing just the scheme
            if (u.has_prefix ("//")) {
                return "%s:%s".printf (base_parsed.get_scheme (), u);
            }

            try {
                uri = Uri.parse_relative (base_parsed, u, UriFlags.ENCODED | UriFlags.PARSE_RELAXED);
                return uri.to_string ();
            } catch (UriError e) { }

            return u;
        }
    }
}
