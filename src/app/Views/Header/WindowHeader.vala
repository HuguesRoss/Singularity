using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/WindowHeader.ui")]
    public class WindowHeader : Widget {
        public string title {
            get { return _title; }
            set {
                if (_title != value) {
                    _title = value;
                    notify_property ("title");
                }
            }
        }
        private string _title = "???";

        public string subtitle {
            get { return _subtitle; }
            set {
                if (_subtitle != value) {
                    _subtitle = value;
                    notify_property ("title");
                }
            }
        }
        private string _subtitle;

        public MenuModel main_menu { get; construct; }

        public bool is_titlebar { get; set; default = true; }

        construct {
            var menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/MainMenu.ui");
            main_menu = menu_builder.get_object ("main_menu") as MenuModel;
        }

        ~WindowHeader () {
            WidgetUtils.cleanup_children (this);
        }
    }
}
