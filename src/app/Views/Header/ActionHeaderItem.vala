using Gtk;

namespace Singularity {
    public class ActionHeaderItem : Object, IHeaderItem {
        public string action_name { owned get; construct; }
        public Variant action_target { owned get; set; }
        public string icon_name { owned get; set; }
        public string tooltip_markup { owned get; set; }
        public bool suggested { get; set; }
        public bool minor { get; set; }

        public ActionHeaderItem (string action) {
            Object (action_name: action);
        }

        public Widget build_widget () {
            var button = new Button () {
                icon_name = icon_name,
                action_target = action_target,
                action_name = action_name,
                tooltip_markup = tooltip_markup,
            };

            if (!minor) {
                button.add_css_class ("raised");
            }
            if (suggested) {
                button.add_css_class ("suggested-action");
            }

            return button;
        }
    }
}
