using Gee;
using Xml;

namespace Singularity {
    const string OPML_VERSION = "2.0";

    /** DataSource for importing/exporting feeds from/to OPML files */
    public class OPMLFeedDataSource {
        /** Holds the output data from parsing */
        public Gee.List<FeedDataEntry> data { get { return _data; } }

        public FeedDataEntry @get (int index) {
            return _data.get (index);
        }

        public bool parse_data (Xml.Doc* doc) {
            Xml.Node* node = doc->get_root_element ();
            while (node != null && node->name != "opml")
                node = node->next;

            if (node == null)
                return false;

            node = node->children;
            while (node != null && node->name != "body")
                node = node->next;

            if (node == null) {
                return false;
            }

            FeedCollection root = new FeedCollection.root ();
            for (node = node->children; node != null; node = node->next) {
                if (node->type == Xml.ElementType.ELEMENT_NODE && node->name == "outline") {
                    parse_outline (node, root);
                }
            }

            _data = root.nodes;

            return true;
        }

        public Xml.Doc* encode_data (Gee.List<FeedDataEntry> to_encode) {
            Xml.Doc* export_doc = new Xml.Doc ();

            Ns* ns = null;
            Xml.Node* base_node = export_doc->new_node (ns, "opml");
            export_doc->set_root_element (base_node);
            base_node->new_prop ("version", OPML_VERSION);
            base_node->add_child (new Xml.Node (ns, "head"));
            Xml.Node* body_node = new Xml.Node (ns, "body");
            base_node->add_child (body_node);

            foreach (FeedDataEntry cn in to_encode) {
                encode_outline (ns, body_node, cn);
            }

            return export_doc;
        }

        private void parse_outline (Xml.Node* node, FeedCollection collection) {
            string title;
            string? url;

            title = node->get_prop ("title");
            if (title == null) {
                title = node->get_prop ("text");
                if (title == null)
                    return;
            }

            url = node->get_prop ("xmlUrl");

            if (url == null) {
                FeedCollection new_collection = new FeedCollection (title);
                for (Xml.Node* n = node->children; n != null; n = n->next)
                    parse_outline (n, new_collection);

                collection.add_node (new_collection);
            } else {
                Feed new_feed = new Feed () {
                    title = title,
                    link = url,
                    site_link = node->get_prop ("htmlUrl"),
                    description = node->get_prop ("description"),
                };

                collection.add_node (new_feed);
            }
        }

        private void encode_outline (Ns* ns, Xml.Node* node, FeedDataEntry cn) {
            // TODO: Write out the contents of cn
            Xml.Node* outline = new Xml.Node (ns, "outline");
            node->add_child (outline);

            if (cn is Feed) {
                Feed feed = cn as Feed;
                outline->new_prop ("type", "rss");
                outline->new_prop ("title", feed.title);
                outline->new_prop ("text", feed.title);
                outline->new_prop ("xmlUrl", feed.link);
                outline->new_prop ("htmlUrl", feed.site_link);
                outline->new_prop ("description", feed.description);
            } else {
                FeedCollection collection = cn as FeedCollection;
                outline->new_prop ("title", collection.title);
                outline->new_prop ("text", collection.title);
                foreach (FeedDataEntry child in collection.nodes) {
                    encode_outline (ns, outline, child);
                }
            }
        }

        protected Gee.List<FeedDataEntry> _data;
    }
}
