using Singularity.Db;

namespace Singularity {
    /** Represents a person, used for displaying feed & item authors */
    public class Person : WebFeed.Person {
        public string guid { get; private set; }

        public Person (string? name, string? uri = null, string? email = null) {
            this.name = name;
            this.uri = uri;
            this.email = email;
        }

        public Person.from_record (IDbResult r) {
            build_from_record (r);
        }

        /** Prepares and hashes this object's guid */
        public void prepare_for_db () {
            guid = md5_guid (to_string ());
        }

        /**
         * Gets the string representation of this person
         * The string is lazy-evaluated, subsequent calls are cheap
         *
         * @return The string representing this person
         */
        public override string to_string () {
            if (_str == null) {
                StringBuilder builder = new StringBuilder ();
                if (name != null)
                    builder.append (name);
                else if (uri != null)
                    builder.append (uri);

                if (email != null)
                    builder.append_printf (" (%s)", email);

                _str = builder.str;
            }
            return _str;
        }

        /** Populate data from a database record
         * @param r The record to read
         */
        protected void build_from_record (IDbResult r) {
            guid = r.get_string ("guid");
            name = r.get_string ("name");
            uri = r.get_string ("url");
            email = r.get_string ("email");
        }

        private string? _str;
    }
}
