using Gdk;

namespace Singularity {
    public class AllSubscriptionsNode : Object, IFeedListNode {
        public bool can_filter { get { return false; } }
        public bool can_rename { get { return false; } }
        public bool has_error { get { return false; } }
        public bool has_unread { get { return _unread_count > 0; } }
        public string icon_name { get { return "go-home-symbolic"; } }
        public Paintable? icon { get { return null; } }
        public string sort_key { get { return ""; } }
        public string title { get { return "All Subscriptions"; } }
        public string type_name { get { return "Collection"; } }
        public string description { get { return ""; } }

        private int _unread_count;
        public int unread_count { get { return _unread_count; } }

        public Gee.List<IFeedListNode> get_children () {
            return new Gee.ArrayList<IFeedListNode> ();
        }

        public ItemQuery query () {
            return new ItemQuery ();
        }
    }
}
