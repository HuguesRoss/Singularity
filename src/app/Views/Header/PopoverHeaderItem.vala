using Gtk;

namespace Singularity {
    public class PopoverHeaderItem<T> : Object, IHeaderItem {
        public string icon_name { owned get; set; }
        public string tooltip_markup { owned get; set; }
        public bool suggested { get; set; }
        public bool minor { get; set; }

        public signal void setup_button (MenuButton button);
        public signal void setup_popover (T popover);

        public Widget build_widget () {
            T pop = Object.new (typeof (T));
            setup_popover (pop);
            var button = new MenuButton () {
                popover = pop as Popover,
                icon_name = icon_name,
                tooltip_markup = tooltip_markup,
            };

            setup_button (button);

            if (!minor) {
                button.add_css_class ("raised");
            }
            if (suggested) {
                button.add_css_class ("suggested-action");
            }

            return button;
        }
    }
}
