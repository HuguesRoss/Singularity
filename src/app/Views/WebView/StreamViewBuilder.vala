using Gee;

namespace Singularity {
    /** Constructs HTML for the stream view */
    public class StreamViewBuilder : ViewBuilder, GLib.Object {
        public StreamViewBuilder (IconCssBuilder icon_builder) {
            try {
                string default_css = resource_to_string ("Views/WebView/default.css");
                string stream_css = resource_to_string ("Views/WebView/stream.css");
                style = "<style>\n%s\n%s\n%s\n</style>".printf (
                    icon_builder.get_css (),
                    default_css,
                    stream_css
                    );
                _iframe_header_noscript = "<style>%s</style>".printf (
                    resource_to_string ("Views/WebView/iframe.css")
                    );
                _iframe_header = "%s<script>%s</script>".printf (
                    _iframe_header_noscript,
                    UserScriptBuilder.load_source_from_resource ("resource://%s/Views/WebView/InnerIframeResizer.js".printf (APP_ID_PATH))
                    );
            } catch (Error e) {
                warning ("Failed to read style information");
            }
        }

        public string build_page_html (Gee.List<Item> items) {
            var style_manager = Adw.StyleManager.get_default ();

            StringBuilder builder = new StringBuilder ();
            builder.append_printf ("<html %s><head>", style_manager.high_contrast ? "class=\"highcontrast\"" : "");
            try {
                builder.append_printf ("<style>%s</style>", resource_to_string (style_manager.dark ? "Views/WebView/colors-dark.css" : "Views/WebView/colors-light.css"));
            } catch (Error e) {
                warning ("Failed to read style information");
            }
            builder.append (style);
            builder.append ("</head>");
            builder.append ("<body onload=\"prepare ()\">");
            int id = 0;
            foreach (Item i in items) {
                builder.append (build_item_html (i, id));
                ++id;
            }
            builder.append ("</body></html>");

            return builder.str;
        }

        public string build_item_html (Item item, int id) {
            StringBuilder builder = new StringBuilder ();
            StringBuilder content_builder = new StringBuilder ("<section class=\"content\">");
            StringBuilder footer_builder = new StringBuilder ("<footer>");

            if (item.icon != null) {
                content_builder.append_printf ("<img src=\"%s\" style=\"max-width: 25%; margin: 0 1em; float: left;\"/>", UriUtils.make_absolute (item.icon, item.owner.base_uri));
            }
            content_builder.append (item.content ?? "");
            content_builder.append ("</section>");

            if (item.attachments.size > 0) {
                footer_builder.append ("<section class=\"attachments-list\">");
                foreach (Attachment a in item.attachments) {
                    footer_builder.append (WebElementBuilders.build_attachment (a, item));
                }
                footer_builder.append ("</section>");
            }

            // TODO: Tags
            footer_builder.append_printf ("</footer>");

            builder.append_printf ("<article class=\"%s\" data-id=\"%d\" data-read=\"%s\" data-starred=\"%s\">",
                BUILDER_CLASS + " singularity-article singularity-frame",
                id,
                item.unread ? "false" : "true",
                item.starred ? "true" : "false");
            builder.append (WebElementBuilders.build_post_header (item));
            builder.append ("<hr/>");
            if (!StringUtils.null_or_empty (item.content)) {
                StringBuilder style_builder = new StringBuilder ();
                var style_manager = Adw.StyleManager.get_default ();
                try {
                    style_builder.append_printf ("<style>%s</style>", resource_to_string (style_manager.dark ? "Views/WebView/colors-dark.css" : "Views/WebView/colors-light.css"));
                } catch (Error e) {
                    warning ("Failed to read style information");
                }

                if (AppSettings.enable_javascript) {
                    style_builder.append_printf (_iframe_header, id);
                } else {
                    style_builder.append (_iframe_header_noscript);
                }

                string sandbox = AppSettings.enable_javascript
                    ? "sandbox=\"allow-scripts\""
                    : "sandbox=\"allow-same-origin\"";

                string content = WebElementBuilders.escape_srcdoc (WebElementBuilders.build_post_content (item, id, style_builder.str, style_manager.high_contrast));
                builder.append_printf ("<iframe class=\"content-frame\" %s credentialless src=\"%s\" srcdoc=\"%s\" onload=\"postLoadResizeIframe(this)\"></iframe>", sandbox, item.link, content);
            }
            builder.append (footer_builder.str);
            builder.append ("</article>");

            return builder.str;
        }

        private string style = "";
        private string _iframe_header = "";
        private string _iframe_header_noscript = "";
        private string iframe_style = "";
        private const string BUILDER_CLASS = "stream";
    }
}
