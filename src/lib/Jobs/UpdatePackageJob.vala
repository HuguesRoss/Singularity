using WebFeed;

namespace Singularity {
    public class UpdatePackageJob : Job {
        public DataLoaderFactory loader_factory { get; set; }
        public DocumentFactory document_factory { get; set; }
        public FeedParserFactory parser_factory { get; set; }
        public UpdateContext context { get; set; }
        public Feed feed { get; construct set; }
        public UpdatePackage package { get; private set; }

        public UpdatePackageJob (Feed f) {
            Object (feed: f);
        }

        protected override async Result run_internal () {
            CacheInfo? info = yield context.get_cache_info_for_uri (feed.link);
            var request = new DataLoadRequest () {
                uri = feed.link,
                cache_info = info,
            };
            var gen = new UpdateGenerator (
                feed,
                _loader_factory.get_for_request (request),
                _document_factory, _parser_factory, _context);
            package = yield gen.do_update ();

            if (package.usable) {
                context.set_cache_info_for_uri (package.feed.link, package.feed_cache_info);
            }

            return package.is_error ? Result.FAILED : Result.SUCCEEDED;
        }
    }
}
