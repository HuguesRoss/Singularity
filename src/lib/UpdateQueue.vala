using Gee;
using WebFeed;

namespace Singularity {
    /* This class takes in feeds, and schedules/executes their updates */
    public class UpdateQueue : Object {
        public int length { get { return _jobs.n_jobs; } }

        public UpdateQueue (DataLoaderFactory loader_factory, DocumentFactory document_factory, FeedParserFactory parser_factory, UpdateContext context) {
            // TODO: Load existing queued requests and put them at the front

            _loader_factory = loader_factory;
            _document_factory = document_factory;
            _parser_factory = parser_factory;
            _context = context;

            _jobs = new JobQueue (4, 500000); // 1/2 second
            _jobs.post_job.connect (post_update);
        }

        public void cancel_update (Feed f) {
            UpdatePackageJob? job = _feed_jobs[f.id];
            if (job != null) {
                job.cancel ();
                _feed_jobs.unset (f.id);
            }
        }

        public void cancel_all_updates () {
            foreach (var entry in _feed_jobs) {
                if (entry.value != null) {
                    entry.value.cancel ();
                }
            }

            _feed_jobs.clear ();
        }

        public void request_update (Feed f) {
            UpdatePackageJob? job = _feed_jobs[f.id];
            if (job == null || job.state > Job.State.RUNNING) {
                job = new UpdatePackageJob (f) {
                    loader_factory = _loader_factory,
                    document_factory = _document_factory,
                    parser_factory = _parser_factory,
                    context = _context,
                };
                _feed_jobs[f.id] = job;
                _jobs.request (job);
            }
        }

        public signal void update_processed (UpdatePackage update);

        private void post_update (Job job) {
            UpdatePackageJob? update_job = job as UpdatePackageJob;
            if (update_job == null || update_job.result == Job.Result.CANCELLED) {
                return;
            }

            update_processed (update_job.package);
        }

        private HashMap<ulong, UpdatePackageJob> _feed_jobs = new HashMap<ulong, UpdatePackageJob> ();
        private DataLoaderFactory _loader_factory;
        private DocumentFactory _document_factory;
        private FeedParserFactory _parser_factory;
        private UpdateContext _context;
        private JobQueue _jobs;
    }
}
