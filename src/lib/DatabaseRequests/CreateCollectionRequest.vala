using Singularity.Db;

namespace Singularity {
    // A DatabaseRequest for creating new FeedCollections
    public class CreateCollectionRequest : IRequest, GLib.Object {
        public FeedCollection node { get; construct; }
        public int parent_id { get; construct; }
        public override string name { get { return "Create collection"; } }
        public override bool use_transaction { get { return false; } }

        public CreateCollectionRequest (FeedCollection n, int p) {
            Object (node: n, parent_id: p);
        }

        public IRequest.Status step (IDbTarget db) {
            int64 id = db.execute_insert (
                "INSERT INTO `feeds` (`parent_id`, `type`, `title`) VALUES (:parent, :type, :title)",
                ":parent", typeof (int), parent_id,
                ":type", typeof (int), (int)FeedDataEntry.Contents.COLLECTION,
                ":title", typeof (string), node.title
                );
            node.prepare_for_db ((int)id);

            return IRequest.Status.COMPLETED;
        }
    }
}
