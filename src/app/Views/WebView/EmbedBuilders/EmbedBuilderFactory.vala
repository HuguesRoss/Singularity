using Gee;

namespace Singularity {
    /** Factory that exposes XML builders */
    public class EmbedBuilderFactory {
        /**
         * Registers a new builder type
         *
         * @param builder_type The builder type
         * @param namespace The XML namespace
         */
        public void register (Type builder_type) {
            if (!builder_type.is_a (typeof (IEmbedBuilder))) {
                error ("Trying to register %s as an embed builder, but it is not an IEmbedBuilder!", builder_type.name ());
            }
            if (!builder_type.is_instantiatable ()) {
                error ("Trying to register %s as an embed builder, but it cannot be instantiated!", builder_type.name ());
            }
            _builders.add (new Lazy<IEmbedBuilder> (() => (IEmbedBuilder)Object.new (builder_type)));
        }

        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         * @param can_download Indicates if the embedded attachment can still be downloaded, if valid
         *
         * @return true if the embed could be made, false if this attachment can't be embedded
         */
        public bool build_attachment_embed (Attachment attachment, Item item, out string embed, out bool can_download) {
            for (int i = _builders.size - 1; i >= 0; i--) {
                if (_builders[i].value.build_attachment_embed (attachment, item, out embed)) {
                    can_download = _builders[i].value.supports_download;
                    return true;
                }
            }

            embed = null;
            can_download = true;
            return false;
        }

        private ArrayList<Lazy<IEmbedBuilder>> _builders = new ArrayList<Lazy<IEmbedBuilder>> ();
    }
}
