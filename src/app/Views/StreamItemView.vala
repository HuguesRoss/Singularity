using Gtk;
using WebKit;

namespace Singularity {
    /**
    * ItemView that displays all items in a linear "stream"
    */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/StreamItemView.ui")]
    public class StreamItemView : Widget, IItemView {
        public string title {
            get { return _title; }
            protected set {
                if (_title != value) {
                    _title = value;

                    notify_property ("title");
                }
            }
        }
        private string _title;
        public string empty_description {
            get { return _empty_description; }
            protected set {
                if (_empty_description != value) {
                    _empty_description = value;

                    notify_property ("title");
                }
            }
        }
        private string _empty_description;
        public bool has_items { get; private set; }
        public IconCssBuilder css_icons {
            get { return _css_icons; }
            set {
                if (_css_icons != value) {
                    _css_icons = value;
                    m_builder = new StreamViewBuilder (_css_icons);
                    notify_property ("css-icons");
                }
            }
        }
        private IconCssBuilder _css_icons;

        public Widget title_widget { get; set; }
        public bool use_csd { get; set; }
        public Gee.Iterable<IHeaderItem> end_items { get; set; }

        construct {
            UserContentManager content_manager = web_view.user_content_manager;

            try {
                _link_script = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/StreamViewLink.js".printf (APP_ID_PATH));
                _resizer_script = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/OuterIframeResizer.js".printf (APP_ID_PATH));
                _resizer_script_nojs = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/OuterIframeResizerNoscript.js".printf (APP_ID_PATH));
            } catch (Error e) {
                error ("Can't read JS resources: %s", e.message);
            }

            content_manager.script_message_received.connect (message_received);
            content_manager.register_script_message_handler ("test", null);
        }

        ~StreamItemView () {
            WidgetUtils.cleanup_children (this);
        }

        /**
         * Display the result of a search
         *
         * @param results The result object serving the items to display
         */
        public void view_items (SearchResults results) {
            _requesting_items = false;
            _results = results;
            if (m_builder == null)
                return;

            setup_view.begin ();
        }

        /**
         * Focus this view
         */
        public void focus_view () {
            web_view.grab_focus ();
        }

        /**
         * Refresh the content of this view
         */
        public void refresh_view () {
            web_view.user_content_manager.remove_all_scripts ();
            web_view.user_content_manager.add_script (_link_script);
            web_view.user_content_manager.add_script (AppSettings.enable_javascript ? _resizer_script : _resizer_script_nojs);

            string html = m_builder.build_page_html (m_item_list);
            web_view.load_html (html, null);
        }

        /**
         * Mark all visible items as read
         */
        public void read_all () {
            web_view.evaluate_javascript.begin ("readAll ();", -1, null, null);

            items_viewed (m_item_list.to_array ());
        }

        [GtkCallback]
        private bool policy_decision (PolicyDecision decision, PolicyDecisionType type) {
            return WebPolicyHandler.policy_decision (decision, type);
        }

        private void message_received (JSC.Value @value) {
            if (!@value.is_string ())
                return;

            string command = @value.to_string ();

            if (command.has_prefix ("p:")) {
                search.begin ();
            } else if (command.has_prefix ("d:")) {
                if (command.length > 2) {
                    web_view.download_uri (command.substring (2));
                }
            } else {
                char cmd;
                int id;
                if (command.scanf ("%c:%d", out cmd, out id) != 2)
                    return;

                switch (cmd) {
                    case 'v': // Item viewed
                        items_viewed ({m_item_list[id]});
                    break;

                    case 's': // Star button pressed
                        item_star_toggle (m_item_list[id]);
                    break;

                    case 'r': // Read button pressed
                        item_read_toggle (m_item_list[id]);
                    break;
                }
            }
        }

        private async void setup_view () {
            var results = _results;
            var item_list = yield get_items (results, 0);

            if (_results != results) {
                return;
            }

            m_item_list = new Gee.ArrayList<Item> ();
            item_list.foreach ((i) => { m_item_list.add (i); return true; });
            has_items = m_item_list.size > 0;

            refresh_view ();
        }

        private async void search () {
            var results = _results;
            if (_requesting_items) {
                return;
            }

            _requesting_items = true;
            Gee.Collection<Item> items = yield get_items (results, m_item_list.size);

            // If the results no longer match, the content has changed and we should bail out
            if (_results != results) {
                return;
            }

            if (!items.is_empty) {
                yield add_items (items);
            }
            _requesting_items = false;
        }

        private async void add_items (Gee.Iterable<Item> items) {
            StringBuilder sb = new StringBuilder ("document.body.innerHTML += String.raw`");
            int starting_id = m_item_list.size;
            int i = m_item_list.size;
            foreach (Item item in items) {
                m_item_list.add (item);
                sb.append (m_builder.build_item_html (item, i));
                i++;
            }
            sb.append_printf ("`; prepareItems (%d);", starting_id);

            web_view.evaluate_javascript.begin (sb.str, -1, null, null);
        }

        private StreamViewBuilder m_builder;
        private Gee.List<Item> m_item_list = new Gee.ArrayList<Item> ();
        private SearchResults _results;
        private bool _requesting_items;
        private UserScript _link_script;
        private UserScript _resizer_script;
        private UserScript _resizer_script_nojs;

        [GtkChild]
        private unowned WebView web_view;
    }
}
