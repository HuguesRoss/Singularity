using Gdk;
using Gtk;

namespace Singularity {
    /**
     * Helper class for finding GTK+ icons on the disk
     */
    public class IconLocator {
        /**
         * Get the path to an icon file from the given name/size
         *
         * @param icon_name The name of the icon to lookup
         * @param icon_size The desired resolution of the icon, in pixels
         *
         * @return A filepath to the icon, or "" if no icon was found
         */
        public static string get_icon (string icon_name, int icon_size) {
            // TODO: Do something about window scaling and text direction?
            IconPaintable paintable = IconTheme.get_for_display (Display.get_default ()).lookup_icon (icon_name, {}, icon_size, 1, TextDirection.LTR, IconLookupFlags.FORCE_SYMBOLIC);
            if (paintable != null) {
                return paintable.file.get_path ();
            }

            return "";
        }

        public static Texture get_texture (string icon_name, int icon_size) throws Error {
            return Texture.from_filename (get_icon (icon_name, icon_size));
        }
    }
}
