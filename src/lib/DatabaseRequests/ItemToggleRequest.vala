/*
    Singularity - A web newsfeed aggregator
    Copyright (C) 2017  Hugues Ross <hugues.ross@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Singularity.Db;

namespace Singularity {
    // DatabaseRequest for toggling an item's unread/starred status
    public class ItemToggleRequest : SimpleRequest {
        public enum ToggleField {
            UNREAD = 0,
            STARRED
        }

        public string guid { get; construct; }
        public ToggleField field { get; construct; }

        public ItemToggleRequest (string i, ToggleField f) {
            Object (guid: i, field: f);

            _name = "Toggle %s".printf (FIELD_NAMES[field]);
        }

        public override Statement build_query (IDbTarget db) {
            var q = new Statement (db, "UPDATE `items` SET `%s` = 1 - `%s` WHERE guid = :guid"
                .printf (FIELD_NAMES[field], FIELD_NAMES[field]));
            q[":guid"] = guid;

            return q;
        }

        private const string[] FIELD_NAMES = { "unread", "starred" };
    }
}
