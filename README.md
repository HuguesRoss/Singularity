# Singularity
Singularity is a simple newsfeed aggregator written with Vala and GTK3. It has support for RSS, RDF, and Atom, and JSON-Feed, but doesn't have full support for their specs yet. It can also import/export OPML files, and use Firefox cookies to access feeds that require a login.

## Building
Clone this repository, making sure to get all submodules in the process. Then perform the following steps:
```
mkdir build
meson init build
cd build
ninja
```

The code should compile successfully, and the compiled executable can be found at `build/src/app/singularity`

## Installing
Installing Singularity is simple, just run `ninja install`. Depending on the configuration of your system, you may need root access to do so.

## Contact
For questions, requests, and other communications regarding this software, you can contact the original creator at huguesross@posteo.com

## License
Singularity is licenced under the GNU General Public License version 3 or later. For a copy of the license, see the LICENSE file in this repository. If this file was not provided to you, then you can find a copy of it [here](https://www.gnu.org/licenses/gpl-3.0.html)
