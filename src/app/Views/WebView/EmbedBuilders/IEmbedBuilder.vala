namespace Singularity {
    /** Interface for attachment embed generators */
    public interface IEmbedBuilder : Object {
        public virtual bool supports_download { get { return true; } }
        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         *
         * @return true if the embed could be made, false if this attachment isn't supported
         */
        public abstract bool build_attachment_embed (Attachment attachment, Item item, out string embed);
    }
}
