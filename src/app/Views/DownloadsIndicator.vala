using Gdk;
using Gtk;

namespace Singularity {
    public class DownloadsIndicator : Widget {
        public string icon_name {
            owned get { return _image.icon_name; /*return _icon_name;*/ }
            set { _image.icon_name = value; }
        }

        public DownloadsPresenter presenter {
            get { return _presenter; }
            set {
                if (_presenter != value) {
                    if (_presenter != null) {
                        _presenter.notify["pending-count"].disconnect (on_visuals_changed);
                        _presenter.notify["download-progress"].disconnect (on_visuals_changed);
                    }

                    _presenter = value;

                    if (_presenter != null) {
                        _presenter.notify["pending-count"].connect (on_visuals_changed);
                        _presenter.notify["download-progress"].connect (on_visuals_changed);
                    }

                    notify_property ("presenter");
                    on_visuals_changed ();
                }
            }
        }
        private DownloadsPresenter _presenter;

        construct {
            layout_manager = new BinLayout ();

            _image = new Image ();
            _image.set_parent (this);
        }

        ~DownloadsIndicator () {
            _image.unparent ();
        }

        public override void snapshot (Gtk.Snapshot s) {
            base.snapshot (s);

            if (presenter == null || presenter.pending_count <= 0) {
                return;
            }

            int w = get_width ();
            int h = get_height ();

            var ctx = s.append_cairo ({ {(w - h) * 0.5f, 0}, {h, h} });
            ctx.set_line_width (2);

            ctx.set_source_rgba (0, 0, 0, 0.5);
            ctx.arc (w * 0.5f, h * 0.5f, h * 0.5f - 1, 0, Math.PI * 2.0);
            ctx.stroke ();

            Gdk.RGBA color = get_color ();
            ctx.set_source_rgba (color.red, color.green, color.blue, color.alpha);
            ctx.arc (w * 0.5f, h * 0.5f, h * 0.5f - 1, degtorad (-90), degtorad (360 * presenter.download_progress - 90));

            ctx.stroke ();
        }

        private void on_visuals_changed () {
            _image.visible = presenter == null || presenter.pending_count <= 0;
            queue_draw ();
        }

        private static double degtorad (double deg) {
            return deg * (Math.PI / 180.0);
        }

        private Image _image;
    }
}
