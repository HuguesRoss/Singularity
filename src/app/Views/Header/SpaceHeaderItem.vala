using Gtk;

namespace Singularity {
    public class SpaceHeaderItem : Object, IHeaderItem {
        public Widget build_widget () {
            var separator = new Separator (Orientation.HORIZONTAL);
            separator.add_css_class ("spacer");

            return separator;
        }
    }
}
