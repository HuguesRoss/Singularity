namespace Singularity.Db {
    /** An IDbTarget representing a single SQLite transaction */
    internal class TransactionDbTarget : Object, IDbTarget {
        internal Sqlite.Database db { get { return _db.db; } }
        private Database _db;

        public TransactionDbTarget (Database db) throws DbError {
            _db = db;
            _db.exec ("BEGIN TRANSACTION");
        }

        public void commit () throws DbError {
            _db.exec ("COMMIT TRANSACTION");
        }

        public void rollback () throws DbError {
            _db.exec ("ROLLBACK TRANSACTION");
        }
    }
}
