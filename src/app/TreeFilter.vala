using Gtk;

namespace Singularity {
    public class TreeFilter : Filter {
        public Filter filter { get; construct; }

        public TreeFilter (Filter child_filter) {
            Object (filter: child_filter);
            filter.changed.connect (changed);
        }

        ~TreeFilter () {
            filter.changed.disconnect (changed);
        }

        public override bool match (Object? item) {
            if (!(item is TreeListRow)) {
                return false;
            }

            var row = (TreeListRow)item;

            bool is_match = filter.match (row.item);
            if (!is_match) {
                if (row.item is IFeedListNode && !((IFeedListNode)row.item).can_filter) {
                    is_match = true;
                } else if (row.children != null) {
                    for (uint i = 0; i < row.children.get_n_items (); i++) {
                        is_match = match (row.get_child_row (i));
                        if (is_match) {
                            break;
                        }
                    }
                }
            }

            return is_match;
        }
    }
}
