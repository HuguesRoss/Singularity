using Singularity.Db;

namespace Singularity {
    /** DatabaseRequest for loading all info about cached urls */
    public class LoadUrlCacheRequest : SimpleRequest {
        public Gee.HashMap<string, CacheInfo> urls { get; construct; }

        construct {
            _name = "Load Cached URL Info";
            urls = new Gee.HashMap<string, CacheInfo> ();
        }

        public override Statement build_query (IDbTarget db) {
            return new Statement (db, "SELECT * FROM cached_urls");
        }

        public override IRequest.Status process_result (IDbResult res) {
            int url_column = res.find_column ("url");
            int good_result_column = res.find_column ("last_good_result");
            int modified_column = res.find_column ("last_modified");
            int etag_column = res.find_column ("etag");
            for (; !res.finished; res.next ()) {
                var info = new CacheInfo () {
                    url = res.fetch_string (url_column),
                    last_good_result = new DateTime.from_unix_utc (res.fetch_int (good_result_column)),
                    last_modified = res.fetch_string (modified_column),
                    etag = res.fetch_string (etag_column),
                };
                urls[info.url] = info;
            }

            return IRequest.Status.COMPLETED;
        }
    }
}
