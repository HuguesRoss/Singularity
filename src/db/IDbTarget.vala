namespace Singularity.Db {
    /** Interface for obejcts that can execute SQLite statements */
    public interface IDbTarget : Object {
        internal abstract Sqlite.Database db { get; }

        /**
         * Execute an INSERT statement and return the last insert rowid
         *
         * @param sql The SQL statement to execute
         * @param ... The arguments to bind, if any. Specify in order of
         *            - string key
         *            - Type value_type
         *            - var value
         *
         * @return the last inserted rowid
         */
        public int64 execute_insert (string sql, ...) {
            var list = va_list ();
            var q = new Statement (this, sql);

            string? key = list.arg ();

            while (key != null) {
                Type type = list.arg ();
                if (type == typeof (string)) {
                    q.set_string (key, list.arg ());
                } else if (type == typeof (int)) {
                    q.set_int (key, list.arg ());
                } else {
                    error ("Type %s not supported", type.name ());
                }

                key = list.arg ();
            }

            return q.execute_insert ();
        }
    }
}
