using Singularity.Db;
using Gee;

namespace Singularity {
    public class Subscriptions : Object {
        public ItemCache cache { get; set; }

        public Subscriptions (Database db) {
            _db = db;
        }

        public signal void subscription_added (Feed f);
        public signal void subscription_removed (Feed f);

        public async FeedCollection load () {
            assert (!_loaded);
            _loaded = true;

            var req = new LoadFeedsRequest ();
            yield _db.execute_request_async (req, IRequest.Priority.HIGH);
            foreach (Feed f in req.feeds.get_feeds ()) {
                _subscriptions[f.link] = f;
                _cache.insert_feed (f);
            }

            return req.feeds;
        }

        public Feed get_by_uri (string link) {
            return _subscriptions[link];
        }

        public async bool try_add (Feed feed) {
            if (_subscriptions.has_key (feed.link)) {
                return false;
            }

            yield _db.execute_request_async (new SubscribeRequest (feed), IRequest.Priority.MEDIUM);
            cache.insert_feed (feed);
            _subscriptions[feed.link] = feed;

            subscription_added (feed);
            return true;
        }

        public async bool try_remove (Feed feed) {
            if (!_subscriptions.has_key (feed.link)) {
                return false;
            }

            yield _db.execute_request_async (new UnsubscribeRequest (feed), IRequest.Priority.HIGH);
            // TODO: Remove from cache?
            _subscriptions.unset (feed.link);

            subscription_removed (feed);
            return true;
        }

        public Iterator<Feed> iterator () {
            return _subscriptions.values.iterator ();
        }

        private HashMap<string, Feed> _subscriptions = new HashMap<string, Feed> ();
        private Database _db;
        private bool _loaded;
    }
}
