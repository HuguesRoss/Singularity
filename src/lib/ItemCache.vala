using Gee;
using Singularity.Db;

namespace Singularity {
    public class ItemCache : Object {
        public ItemCache (Database db) {
            _db = db;
        }

        public async SearchResults search_async (ItemQuery query, int limit = 0, int cursor = 0) {
            var search_request = new SearchItemsRequest (query, this);
            yield _db.execute_request_async (search_request, IRequest.Priority.HIGH);
            return new SearchResults (query, this, search_request.result);
        }

        public Feed? get_feed (int id) {
            return _feeds[id];
        }

        public bool try_get (string guid, out Item? item) {
            item = _items[guid];
            return item != null;
        }

        public async Item load_item (string guid) {
            var load_request = new LoadItemRequest (guid, this);
            yield _db.execute_request_async (load_request, IRequest.Priority.HIGH);
            return load_request.item;
        }

        public void insert (Item item) {
            _items[item.guid] = item;
        }
        public void insert_feed (Feed feed) {
            _feeds[feed.id] = feed;
        }

        private HashMap<string, Item?> _items = new HashMap<string, Item?> ();
        private HashMap<int, Feed?> _feeds = new HashMap<int, Feed?> ();
        private Database _db;
    }
}
