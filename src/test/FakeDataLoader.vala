using Xml;

namespace Singularity.Tests {
    /** Data loader that returns predetermined data for testing purposes */
    public class FakeDataLoader : Object, IDataLoader {
        /** The error message for this request, if any */
        public string? error_message { get; protected set; }

        /** The request URI */
        public DataLoadRequest request { get; set; }

        /** The contextual data for this update */
        public UpdateContext update_context { get; set; }

        public FakeDataLoader (string? data) {
            _data = data;
        }

        /** Returns the base URI that this request is loading from */
        public string get_base_uri () {
            return "https://example.com";
        }

        /**
         * Sends the request asynchronously
         *
         * @return true if the request succeeded; otherwise, false
         */
        public async DataLoadResponse send_async () {
            return new DataLoadResponse () { bytes = new Bytes (_data.data) };
        }

        private string? _data;
    }
}
