using Gee;
using Gtk;

namespace Singularity {
    public class MasonryView : Widget, Accessible, Orientable, Scrollable {
        public uint n_columns {
            get { return _n_columns; }
            set {
                if (_n_columns != value) {
                    assert (value > 0);

                    _n_columns = value;

                    refresh_all_items ();
                    notify_property ("n-columns");
                }
            }
        }
        private uint _n_columns = 1;

        public Adjustment hadjustment {
            get { return _vadjustment; }
            set construct {
                if (_hadjustment != value) {
                    if (_hadjustment != null && _orientation == Orientation.HORIZONTAL) {
                        _hadjustment.value_changed.disconnect (on_scroll);
                    }

                    _hadjustment = value;

                    if (_hadjustment != null && _orientation == Orientation.HORIZONTAL) {
                        _hadjustment.value_changed.connect (on_scroll);
                    }
                    notify_property ("hadjustment");
                }
            }
        }
        private Adjustment _hadjustment;

        public ScrollablePolicy hscroll_policy { get; set; }

        public SelectionModel model {
            get { return _model; }
            set {
                if (_model != value) {
                    if (_model != null) {
                        _model.items_changed.disconnect (on_items_changed);
                    }

                    _model = value;

                    if (_model != null) {
                        _model.items_changed.connect (on_items_changed);
                    }

                    refresh_all_items ();
                    notify_property ("model");
                }
            }
        }
        private SelectionModel _model;

        public Orientation orientation {
            get { return _orientation; }
            set {
                if (_orientation != value) {
                    _orientation = value;
                    if (_orientation == Orientation.HORIZONTAL) {
                        vadjustment.value_changed.disconnect (on_scroll);
                        hadjustment.value_changed.connect (on_scroll);
                    } else {
                        hadjustment.value_changed.disconnect (on_scroll);
                        vadjustment.value_changed.connect (on_scroll);
                    }
                    refresh_all_items ();
                    notify_property ("orientation");
                }
            }
        }
        private Orientation _orientation = Orientation.VERTICAL;

        public Adjustment vadjustment {
            get { return _vadjustment; }
            set construct {
                if (_vadjustment != value) {
                    if (_vadjustment != null && _orientation == Orientation.VERTICAL) {
                        vadjustment.value_changed.disconnect (on_scroll);
                    }

                    _vadjustment = value;

                    if (_vadjustment != null && _orientation == Orientation.VERTICAL) {
                        vadjustment.value_changed.connect (on_scroll);
                    }
                    notify_property ("vadjustment");
                }
            }
        }
        private Adjustment _vadjustment;

        public ScrollablePolicy vscroll_policy { get; set; }

        public uint spacing {
            get { return _spacing; }
            set {
                if (_spacing != value) {
                    _spacing = value;
                    queue_resize ();
                    notify_property ("spacing");
                }
            }
        }
        private uint _spacing = 6;

        public int min_column_height {
            get {
                if (_column_heights.length == 0) {
                    return 0;
                }

                int out_height = int.MAX;
                foreach (int height in _column_heights) {
                    out_height = int.min (height, out_height);
                }

                return out_height;
            }
        }

        public signal void activate (uint position);

        public signal Widget create_widget (Object item);

        static construct {
            set_css_name ("masonryview");
        }

        construct {
            //focusable = true;
        }

        ~MasonryView () {
            WidgetUtils.cleanup_children (this);
        }

        public Iterable<MasonryItemWrapper> get_column_widgets (uint column) {
            if (column > _column_widgets.length) {
                return new Gee.ArrayList<MasonryItemWrapper> ();
            }

            return _column_widgets[column];
        }

        public override bool focus (DirectionType direction) {
            MasonryItemWrapper? child = get_focus_child () as MasonryItemWrapper;
            if (child == null) {
                return false;
            }

            int x;
            int y = -1;
            for (x = 0; x < n_columns; x++) {
                y = _column_widgets[x].index_of (child);
                if (y != -1) {
                    break;
                }
            }

            // Couldn't find the wrapper
            if (y == -1) {
                return false;
            }

            Graphene.Rect bounds;
            if (!child.compute_bounds (this, out bounds)) {
                return false;
            }
            float top_y = bounds.get_y ();
            float center_y = bounds.get_center ().y;

            MasonryItemWrapper? next_focus = null;
            switch (direction) {
                case DirectionType.LEFT:
                    if (x > 0) {
                        Graphene.Rect sibling_bounds;
                        for (int i = _column_widgets[x - 1].size - 1; i >= 0; i--) {
                            MasonryItemWrapper w = _column_widgets[x - 1][i];
                            if (w.compute_bounds (this, out sibling_bounds) && (sibling_bounds.get_y () <= top_y || sibling_bounds.get_bottom_right ().y < center_y)) {
                                next_focus = w;
                                break;
                            }
                        }
                    }
                    break;
                case DirectionType.RIGHT:
                    if (x < n_columns - 1) {
                        Graphene.Rect sibling_bounds;
                        foreach (MasonryItemWrapper w in _column_widgets[x + 1]) {
                            if (w.compute_bounds (this, out sibling_bounds) && (sibling_bounds.get_y () >= top_y || sibling_bounds.get_bottom_right ().y > center_y)) {
                                next_focus = w;
                                break;
                            }
                        }

                        if (next_focus == null && !_column_widgets[x + 1].is_empty) {
                            next_focus = _column_widgets[x + 1].last ();
                        }
                    }
                    break;
                case DirectionType.DOWN:
                    if (y < _column_widgets[x].size - 1) {
                        next_focus = _column_widgets[x][y + 1];
                    }
                    break;
                case DirectionType.UP:
                    if (y > 0) {
                        next_focus = _column_widgets[x][y - 1];
                    }
                    break;
                case DirectionType.TAB_FORWARD:
                    if (y < _column_widgets[x].size - 1) {
                        next_focus = _column_widgets[x][y + 1];
                    } else if (x < n_columns - 1 && _column_widgets[x + 1].size > 0) {
                        next_focus = _column_widgets[x + 1].first ();
                    }
                    break;
                case DirectionType.TAB_BACKWARD:
                    if (y > 0) {
                        next_focus = _column_widgets[x][y - 1];
                    } else if (x > 0) {
                        // Previous columns are guaranteed to have
                        // something, so no need to check
                        next_focus = _column_widgets[x - 1].last ();
                    }
                    break;
            }

            if (next_focus == null) {
                return false;
            }

            next_focus.child.grab_focus ();
            return true;
        }

        public override SizeRequestMode get_request_mode () {
            if (orientation == Orientation.VERTICAL) {
                return SizeRequestMode.HEIGHT_FOR_WIDTH;
            } else {
                return SizeRequestMode.WIDTH_FOR_HEIGHT;
            }
        }

        public override void size_allocate (int width, int height, int baseline) {
            _cross_width = orientation == Orientation.HORIZONTAL ? height : width;
            Adjustment adj = orientation == Orientation.HORIZONTAL ? hadjustment : vadjustment;

            if (n_columns == 0) {
                return;
            }

            int column_width = (_cross_width - ((int)spacing * ((int)n_columns - 1))) / (int)n_columns;
            int max_column_height = 0;
            //warning ("%d: %d / %d - (%d * %d)", column_width, _cross_width, (int)n_columns, (int)spacing, (int)n_columns - 1);
            for (int i = 0; i < n_columns; i++) {
                allocate_column (i, column_width, (int)adj.value);
                max_column_height = int.max (_column_heights[i], max_column_height);
            }

            adj.configure (adj.value, 0, max_column_height, 10, height, height);
            notify_property ("min-column-height");
        }

        private void allocate_column (int column, int column_width, int scroll) {
            int marker = (int)spacing * -1; // Start negative so that the first iteration sends it forward
            foreach (Widget w in _column_widgets[column]) {
                marker += (int)spacing;

                int nat;
                w.measure (orientation, column_width, null, out nat, null, null);
                if (orientation == Orientation.HORIZONTAL) {
                    Allocation a = {
                        marker - scroll,
                        column * column_width + ((int)spacing * column),
                        nat,
                        column_width,
                    };
                    w.allocate_size (a, -1);
                } else {
                    //message ("%d %d", column_width, nat);
                    Allocation a = {
                        column * column_width + ((int)spacing * column),
                        marker - scroll,
                        column_width,
                        nat,
                    };
                    w.allocate_size (a, -1);
                }
                marker += nat;
            }
            _column_heights[column] = int.max (marker, 0);
        }

        private void on_items_changed (uint position, uint removed, uint added) {
            if (removed == 0 && position == model.get_n_items () - added) {
                // If we're adding items to the end, just add items without removing the old ones
                uint i = position;
                Object? item = model.get_item (i);
                while (item != null) {
                    create_item (i, item);

                    i++;
                    item = model.get_item (i);
                }

                queue_allocate ();
            } else {
                refresh_all_items ();
            }
        }

        private void refresh_all_items () {
            WidgetUtils.cleanup_children (this);
            _column_heights = new int[n_columns];
            _column_widgets = new ArrayList<MasonryItemWrapper>[n_columns];

            for (int i = 0; i < n_columns; i++) {
                _column_heights[i] = 0;
                _column_widgets[i] = new ArrayList<MasonryItemWrapper> ();
            }

            if (model == null) {
                return;
            }

            uint i = 0;
            Object? item = model.get_item (i);
            while (item != null) {
                create_item (i, item);

                i++;
                item = model.get_item (i);
            }

            queue_allocate ();
        }

        private void create_item (uint position, Object item) {
            int index = 0;
            int min_height = int.MAX;

            for (int i = 0; i < n_columns; i++) {
                if (_column_heights[i] < min_height) {
                    index = i;
                    min_height = _column_heights[i];
                }
            }

            var wrapper = new MasonryItemWrapper (position, item, create_widget (item));

            wrapper.set_parent (this);
            _column_widgets[index].add (wrapper);

            _column_heights[index] += STANDARD_HEIGHT;
        }

        private void on_scroll () {
            queue_allocate ();
        }

        private int[] _column_heights;
        private ArrayList<MasonryItemWrapper>[] _column_widgets;
        private int _cross_width;

        private const int STANDARD_HEIGHT = 70;
    }
}
