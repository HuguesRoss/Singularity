using Gtk;
using Gdk;

namespace Singularity {
    public enum ViewType {
        STREAM = 0,
        COLUMN,
        GRID;

        /**
         * Convert this enum into a readable string
         *
         * @return The readable string representation of this view type
         */
        public string to_string () {
            switch (this) {
                case ViewType.STREAM:
                    return "view_stream";
                case ViewType.COLUMN:
                    return "view_column";
                case ViewType.GRID:
                    return "view_grid";
            }

            // Invalid
            return "";
        }
    }

    // The primary application view
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/MainView.ui")]
    public class MainView : Gtk.Widget {
        public bool important_view {
            get { return _important_view; }
            set {
                if (_important_view != value) {
                    _important_view = value;
                    display_node.begin ();
                }
            }
        }
        private bool _important_view = true;
        public bool search_mode {
            get { return _search_mode; }
            set {
                if (_search_mode != value) {
                    _search_mode = value;
                    // TODO
                }
            }
        }
        private bool _search_mode = false;

        public bool subscriptions_loading {
            get { return _subscriptions_loading; }
            set {
                if (_subscriptions_loading != value) {
                    _subscriptions_loading = value;

                    if (_subscriptions_loading) {
                        notifications.add_toast (update_progress);
                    } else {
                        update_progress.dismiss ();
                    }

                    notify_property ("subscriptions-loading");
                }
            }
        }
        private bool _subscriptions_loading;

        public bool decorated { get; set; default = true; }

        public string title {
            owned get { return _title; }
            set {
                if (_title != value) {
                    _title = value;
                    if (window != null) {
                        window.title = title;
                    }
                }
            }
        }
        private string _title;

        public string page_title { get; set; }
        public string subtitle { get; set; }
        public float load_percentage { get; set; }

        public MainPresenter presenter { get; construct set; }

        public TextureCache icons { get; set; }

        public UpdateQueueWrapper updates { get; set; }

        public ApplicationWindow? window { get { return this.root as ApplicationWindow; } }

        /** The type of feed view that is currently active */
        public ViewType view_type {
            get { return _view_type; }
            set {
                if (_view_type != value) {
                    _view_type = value;
                    AppSettings.last_view = (int)_view_type;
                    refresh_view ();
                }
            }
        }
        private ViewType _view_type;

        public DownloadsPresenter downloads { get; construct set; }

        public IconCssBuilder css_icons { get; construct set; }

        /**
         * The direction that items are sorted in
         */
        public SortType sort_type {
            get { return _sort_type == 1 ? SortType.ASCENDING : SortType.DESCENDING; }
            set {
                int mod = value == SortType.ASCENDING ? 1 : -1;
                if (_sort_type != mod) {
                    _sort_type = mod;
                    display_node.begin ();
                }
            }
        }
        private int _sort_type = -1;

        public Gee.Iterable<IHeaderItem> feed_header_items_start { get; construct; }
        public Gee.Iterable<IHeaderItem> feed_header_items_end { get; construct; }
        public Gee.Iterable<IHeaderItem> welcome_header_items { get; construct; }
        public Gee.Iterable<IHeaderItem> item_header_items { get; construct; }
        public Gee.Iterable<IHeaderItem> view_header_items { get; construct; }

        /**
         * Get the MainView child of a window. Always use this instead of
         * Gtk.Window.child, because Adw.ApplicationWindow will return
         * the wrong value otherwise!
         *
         * @param Window the window to check
         *
         * @return The MainView, or none if it wasn't found
         */
        public static MainView find_in_parent (Window window) {
            if (window is Adw.ApplicationWindow) {
                return window.content as MainView;
            }

            return window.child as MainView;
        }

        construct {
            Gtk.IconTheme.get_for_display (get_display ()).add_resource_path ("%s/icons".printf (APP_ID_PATH));

            var style_manager = Adw.StyleManager.get_default ();

            var style_css = new CssProvider ();
            style_css.load_from_resource ("%s/Views/widgets.css".printf (APP_ID_PATH));
            StyleContext.add_provider_for_display (get_display (), style_css, STYLE_PROVIDER_PRIORITY_USER);

            _high_contrast_css = new CssProvider ();
            _high_contrast_css.load_from_resource ("%s/Views/high_contrast.css".printf (APP_ID_PATH));
            if (style_manager.high_contrast) {
                StyleContext.add_provider_for_display (get_display (), _high_contrast_css, STYLE_PROVIDER_PRIORITY_USER);
            }
            style_manager.notify["high-contrast"].connect (refresh_theme);

            var menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/MainMenu.ui");
            var main_menu = menu_builder.get_object ("main_menu") as MenuModel;

            var view_menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/ViewMenu.ui");
            var view_menu = view_menu_builder.get_object ("view_menu") as MenuModel;

            feed_header_items_start = new Gee.ArrayList<IHeaderItem>.wrap ({
                new ActionHeaderItem ("feeds.subscribe") {
                    tooltip_markup = "<b>Add Subscription</b> (Ctrl+N)",
                    icon_name = "list-add-symbolic",
                    suggested = true,
                },
            });

            feed_header_items_end = new Gee.ArrayList<IHeaderItem>.wrap ({
                new ActionHeaderItem ("view.filter") {
                    tooltip_markup = "<b>Search Feeds</b> (Ctrl+F)",
                    icon_name = "edit-find-symbolic",
                    minor = true,
                },
            });

            item_header_items = new Gee.ArrayList<IHeaderItem>.wrap ({
                new MenuHeaderItem (main_menu) { icon_name = "open-menu-symbolic", },
            });

            _attachments_item = new PopoverHeaderItem<AttachmentsPopover> () {
                tooltip_markup = "<b>Downloads…</b> (Ctrl+Shift+Y)",
                icon_name = "folder-download-symbolic",
            };
            _attachments_item.setup_button.connect (setup_attachments_button);
            _attachments_item.setup_popover.connect (setup_attachments_pop);

            view_header_items = new Gee.ArrayList<IHeaderItem>.wrap ({
                new MenuHeaderItem (main_menu) { icon_name = "open-menu-symbolic", },
                _attachments_item,
                new SpaceHeaderItem (),
                new MenuHeaderItem (view_menu) { icon_name = "view-more-symbolic", },
                new ActionHeaderItem ("view.read-all") {
                    tooltip_markup = "<b>Mark all as read</b> (Ctrl+M)",
                    icon_name = "object-select-symbolic",
                },
            });

            welcome_header_items = new Gee.ArrayList<IHeaderItem>.wrap ({
                new MenuHeaderItem (main_menu) { icon_name = "open-menu-symbolic", },
                _attachments_item,
            });

            var filter_list = new GLib.ListStore (typeof (Gtk.FileFilter));
            filter_list.append (opml_filter);
            filter_list.append (all_filter);

            import_dlg.filters = filter_list;
            export_dlg.filters = filter_list;

            about_action = new GLib.SimpleAction ("about", null);
            about_action.activate.connect (() => {
                var about_window = new Adw.AboutDialog () {
                    application_icon = "net.huguesross.Singularity",
                    version = "0.6-dev",
                    application_name = "Singularity",
                    developer_name = "Hugues Alexandre Ross",
                    comments = "A simple webfeed aggregator",
                    website = "https://www.huguesross.net/code/singularity",
                    issue_url = "https://codeberg.org/HuguesRoss/Singularity/issues",
                    license_type = Gtk.License.GPL_3_0,
                    copyright = "Copyright © 2014-2024 Hugues Alexandre Ross",
                };
                about_window.add_legal_section (
                    "Bookmark Icon (Modified from the Adwaita icon theme)",
                    "Copyright © 2023 GNOME Project",
                    Gtk.License.LGPL_3_0,
                    null
                );

                about_window.present (this);
            });

            preferences_action = new GLib.SimpleAction ("preferences", null);
            preferences_action.activate.connect (() => {
                var settings = new SettingsWindow ();
                settings.settings_saved.connect (needs_restart => {
                    grid_view.use_masonry_layout = settings.use_masonry_layout;

                    if (needs_restart) {
                        settings.close ();
                        restart_requested ();
                    }

                    // NOTE: We need to call this *after* restart_requested, because switching the header widget will potentially cause the title to disappear otherwise.
                    //       I'm guessing something isn't being transferred correctly to the new window...hard to say for certain though
                    refresh_theme ();
                });
                settings.present (this);
            });

            downloads = new DownloadsPresenter (WebKit.NetworkSession.get_default ());

            setup_css_icons.begin ();
        }

        public MainView (SingularityApp owner_app, MainPresenter p, ItemCache cache) {
            Object (presenter: p);
            app = owner_app;
            icons = owner_app.update_context.icons;
            _cache = cache;

            _view_type = (ViewType)AppSettings.last_view;
            refresh_view ();

            view_group = new SimpleActionGroup ();
            var filter_action = new GLib.PropertyAction ("filter", feed_pane, "is-filtering");
            view_group.add_action (filter_action);

            var read_all_action = new SimpleAction ("read-all", null);
            read_all_action.activate.connect (() => m_item_view.read_all ());
            view_group.add_action (read_all_action);

            view_group.add_action (new PropertyAction ("important", this, "important_view"));

            view_group.add_action (new PropertyAction ("sort_type", this, "sort_type"));

            /* view_group.add_action (new PropertyAction ("search_mode", this, "search_mode")); */

            view_group.add_action (new PropertyAction ("view_type", this, "view_type"));

            var downloads_action = new SimpleAction ("downloads", null);
            downloads_action.activate.connect (() => {
                MenuButton? button = _attachments_menu_buttons.first_match (b => b.get_mapped ());
                if (button != null) {
                    button.popover.popup ();
                }
            });
            view_group.add_action (downloads_action);


            feeds_group = new SimpleActionGroup ();
            var subscribe_action = new SimpleAction ("subscribe", null);
            subscribe_action.activate.connect (() => {
                var subscribe_dlg = new SubscribeDialog () {
                    loader_factory = app.loader_factory,
                    document_factory = app.document_factory,
                    parser_factory = app.parser_factory,
                    loader_factory = app.loader_factory,
                    feed_locators = app.feed_locators,
                    context = app.update_context,
                    subscriptions = app.subscriptions,
                };
                subscribe_dlg.new_subscription.connect ((new_feed, items) => {
                    app.subscribe_to_feed_async.begin (new_feed, items != null, null, items);
                });
                subscribe_dlg.present (this);
            });
            feeds_group.add_action (subscribe_action);

            var bookmarks_action = new SimpleAction ("bookmarks", null);
            bookmarks_action.activate.connect (() => feed_pane.select_bookmarks ());
            feeds_group.add_action (bookmarks_action);

            var import_action = new SimpleAction ("import", null);
            import_action.activate.connect (import_feeds);
            feeds_group.add_action (import_action);

            var export_action = new SimpleAction ("export", null);
            export_action.activate.connect (export_feeds);
            feeds_group.add_action (export_action);

            feed_pane.init (this, app);
            feed_pane.notify["selected"].connect (() => {
                display_node.begin ();
            });

            grid_view.use_masonry_layout = AppSettings.use_masonry_layout;

            updates = app.updates;
            updates.update_failed.connect (package => {
                _pending_error_packages.push (package);
                Idle.add_once (add_errors);
            });

            app.load_status_changed.connect ( (val) => {
                if (val == SingularityApp.LoadStatus.COMPLETED) {
                    post_load ();
                }
            });

            app.subscribe_done.connect ( (f) => {
                if (view_stack.visible_page.tag == "welcome") {
                    view_stack.replace_with_tags (new string[] {_view_type.to_string () });
                    m_item_view.refresh_view ();
                    feed_pane.expand_base ();
                }
            });

            if (!app.init_success) {
                view_stack.replace_with_tags (new string[] { "loading" });
            } else {
                post_load ();
            }

            key_controller = new EventControllerKey () {
                propagation_phase = PropagationPhase.CAPTURE,
            };
            key_controller.key_pressed.connect (on_key_press);
            ((Widget)this).add_controller (key_controller);
        }

        /**
         * Add a new feed update error
         *
         * @param f The feed that failed to update
         * @param message The error message
         */
        public void add_errors () {
            try {
                _pending_error_packages.lock ();
                UpdatePackage package = _pending_error_packages.try_pop_unlocked ();
                while (package != null) {
                    errors_list.add_error (package.feed, package.message);
                    package = _pending_error_packages.try_pop_unlocked ();
                }
            } finally {
                _pending_error_packages.unlock ();
            }
        }

        public async void display_node () {
            update_title_from_selection ();
            WidgetUtils.pop_temp_navigation_pages (view_stack);

            IFeedListNode? node = feed_pane.selected_node;

            // Prepare the item list
            if (node != null) {

                var query = node.query ();
                query.unread_only = query.unread_only ?? important_view;
                query.ascending = sort_type == SortType.ASCENDING;

                _results = yield _cache.search_async (query, AppSettings.items_per_list, 0);
                display_search_results ();
            }
        }

        public void refresh_theme () {
            m_item_view.refresh_view ();
            var style_manager = Adw.StyleManager.get_default ();
            StyleContext.remove_provider_for_display (get_display (), _high_contrast_css);
            if (style_manager.high_contrast) {
                StyleContext.add_provider_for_display (get_display (), _high_contrast_css, STYLE_PROVIDER_PRIORITY_USER);
            }
            refresh_header (!AppSettings.disable_csd);
        }

        public bool on_close_requested (Window window) {
            if (downloads.pending_count == 0) {
                return false;
            }

            new WaitForDownloadsDialog (downloads).present (this);
            return true;
        }

        public signal void restart_requested ();
        public signal void unsub_requested (Feed? feed);
        public signal void new_collection_requested (FeedCollection? parent);
        public signal void rename_node_requested (CollectionNode? node, string title);
        public signal void delete_collection_requested (FeedCollection? collection);

        private void refresh_header (bool is_title) {
            decorated = !is_title;

#if IS_DEV_BUILD
            if (decorated) {
                window.remove_css_class ("devel");
            } else {
                window.add_css_class ("devel");
            }
#endif
        }

        private void post_load () {
            feed_pane.expand_base ();
            if (app.has_subscriptions) {
                view_stack.replace_with_tags (new string[] {_view_type.to_string () });
                display_node.begin ();
            } else {
                view_stack.replace_with_tags (new string[] {"welcome"});
            }
        }

        /**
         * Update the UI in response to a feed view change
         */
        private void refresh_view () {
            var page = view_stack.find_page (_view_type.to_string ());
            m_item_view = page.child as IItemView;

            if (get_mapped ()) {
                if (view_stack.visible_page.tag != "welcome") {
                    view_stack.replace (new Adw.NavigationPage[] { page });
                }

                display_search_results ();
            }
        }

        private void display_search_results () {
            m_item_view.set_node_info (feed_pane.selected_node, _results.query);
            m_item_view.view_items (_results);
            m_item_view.focus_view ();
        }

        /**
         * Update the HeaderBar's title and description from the last selected node
         *
         * If the node is null, this just falls back to the application name
         */
        private void update_title_from_selection () {
            IFeedListNode selected_node = feed_pane.selected_node;
            if (selected_node != null) {
                page_title = selected_node.title;
                subtitle = selected_node.description;
            } else {
                title = "Singularity";
                subtitle = "";
            }
        }

        private async void export_feeds () {
            try {
                var export_file = yield export_dlg.save (window, null);
                if (export_file != null) {
                    app.opml_export (export_file);
                }
            } catch (Error e) {
                if (!e.matches (Gtk.DialogError.quark (), Gtk.DialogError.DISMISSED) && !e.matches (Gtk.DialogError.quark (), Gtk.DialogError.CANCELLED)) {
                    warning ("Unable to export OPML file: %s", e.message);
                }
            }
        }

        private async void import_feeds () {
            try {
                var import_file = yield import_dlg.open (window, null);
                if (import_file != null) {
                    app.opml_import (import_file);
                }
            } catch (Error e) {
                if (!e.matches (Gtk.DialogError.quark (), Gtk.DialogError.DISMISSED) && !e.matches (Gtk.DialogError.quark (), Gtk.DialogError.CANCELLED)) {
                    warning ("Unable to import OPML file: %s", e.message);
                }
            }
        }

        private void setup_attachments_button (MenuButton button) {
            button.child = new DownloadsIndicator () {
                icon_name = button.icon_name,
                presenter = downloads,
            };
            button.update_property (
                AccessibleProperty.LABEL, "Downloads",
                AccessibleProperty.DESCRIPTION, "Show the list of downloads for this session. Shortcut Control Shift Y",
                -1);
            _attachments_menu_buttons.add (button);
        }
        private void setup_attachments_pop (AttachmentsPopover popover) {
            popover.presenter = downloads;
        }

        private async void setup_css_icons () {
            var icon_builder = new IconCssBuilder ();

            yield icon_builder.add_icon_for_file ("star-filled", File.new_for_uri ("resource://%s/icons/scalable/emblems/bookmarks-filled-symbolic.svg".printf (APP_ID_PATH)));
            yield icon_builder.add_icon_for_file ("star-empty", File.new_for_uri ("resource://%s/icons/scalable/emblems/bookmarks-empty-symbolic.svg".printf (APP_ID_PATH)));
            yield icon_builder.add_icon_for_file ("read", File.new_for_uri ("resource://%s/icons/scalable/emblems/shape-circle-empty-symbolic.svg".printf (APP_ID_PATH)));
            yield icon_builder.add_icon_for_file ("unread", File.new_for_uri ("resource://%s/icons/scalable/emblems/shape-circle-filled-symbolic.svg".printf (APP_ID_PATH)));

            string[] icons = {
                "audio-x-generic-symbolic",
                "image-x-generic-symbolic",
                "video-x-generic-symbolic",
                "font-x-generic-symbolic",
                "x-office-document-symbolic",
                "package-x-generic-symbolic",
                "application-x-executable-symbolic",
                "x-office-calendar-symbolic",
                "x-office-spreadsheet-symbolic",
                "x-office-presentation-symbolic",
                "text-x-generic-symbolic",
            };
            foreach (string icon in icons) {
                yield icon_builder.add_themed_icon (icon);
            }

            css_icons = icon_builder;
        }

        [GtkCallback]
        void on_items_viewed (Item[] items) {
            if (items.length == 0) {
                return;
            }

            presenter.view_items.begin (items);
        }

        [GtkCallback]
        void on_item_read_toggle (Item item) {
            app.toggle_unread (item);
        }

        [GtkCallback]
        void on_item_star_toggle (Item item) {
            app.toggle_star (item);
        }

        [GtkCallback]
        void on_setup () {
            var builder = new Builder.from_resource (APP_ID_PATH + "/Views/Shortcuts.ui");
            var shortcuts = builder.get_object ("shortcuts-window") as ShortcutsWindow;
            shortcuts.set_transient_for (window);
            shortcuts.section_name = "general";
            shortcuts.view_name = null;
            window.set_help_overlay (shortcuts);


            window.add_action (about_action);
            window.add_action (preferences_action);
            window.insert_action_group ("view", view_group);
            window.insert_action_group ("feeds", feeds_group);

            window.title = title;

            refresh_header (!AppSettings.disable_csd);
        }

        [GtkCallback]
        void on_dismiss_update () {
            if (subscriptions_loading) {
                // FIXME: Apparently this was never implemented???
            }
        }

        private bool on_key_press (uint keyval, uint keycode, ModifierType state) {
            if ((state & ModifierType.ALT_MASK) != 0) {
                bool next_unread = (state & ModifierType.SHIFT_MASK) != 0;

                switch (keyval) {
                    case Gdk.Key.Up:
                        feed_pane.prev_feed (next_unread);
                        return true;
                    case Gdk.Key.Down:
                        feed_pane.next_feed (next_unread);
                        return true;
                }
            } else {
                switch (keyval) {
                    case Gdk.Key.Escape:
                        if (WidgetUtils.pop_temp_navigation_pages (view_stack)) {
                            return true;
                        }
                        break;
                }
            }

            return false;
        }

        [GtkChild]
        private unowned Adw.NavigationView view_stack;
        [GtkChild]
        private unowned FeedPane feed_pane;
        [GtkChild]
        private unowned ErrorsList errors_list;

        [GtkChild]
        private unowned FileDialog import_dlg;
        [GtkChild]
        private unowned FileDialog export_dlg;
        [GtkChild]
        private unowned FileFilter all_filter;
        [GtkChild]
        private unowned FileFilter opml_filter;
        [GtkChild]
        private unowned GridItemView grid_view;

        [GtkChild]
        private unowned Adw.ToastOverlay notifications;

        [GtkChild]
        private unowned Adw.Toast update_progress;

        private SingularityApp app;

        private IItemView m_item_view;

        private EventControllerKey key_controller;

        private ItemCache _cache;

        private SearchResults _results;

        private CssProvider _high_contrast_css;

        private SimpleAction about_action;
        private SimpleAction preferences_action;
        private SimpleActionGroup view_group;
        private SimpleActionGroup feeds_group;
        private AsyncQueue<UpdatePackage> _pending_error_packages = new AsyncQueue<UpdatePackage> ();
        private PopoverHeaderItem<AttachmentsPopover> _attachments_item;
        private Gee.ArrayList<MenuButton> _attachments_menu_buttons = new Gee.ArrayList<MenuButton> ();
    }
}
