using Gdk;

namespace Singularity {
    public class BookmarksNode : Object, IFeedListNode {
        public bool can_filter { get { return false; } }
        public bool can_rename { get { return false; } }
        public bool has_error { get { return false; } }
        public bool has_unread { get { return false; } }
        public string icon_name { get { return "bookmarks-empty-symbolic"; } }
        public Paintable? icon { get { return null; } }
        public string sort_key { get { return ""; } }
        public string title { get { return "Bookmarks"; } }
        public string type_name { get { return "Collection"; } }
        public string description { get { return ""; } }

        public int unread_count { get { return 0; } }

        public Gee.List<IFeedListNode> get_children () {
            return new Gee.ArrayList<IFeedListNode> ();
        }

        public ItemQuery query () {
            return new ItemQuery () {
                unread_only = false,
                starred_only = true,
            };
        }
    }
}
