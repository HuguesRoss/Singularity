using Gtk;

namespace Singularity {
    /** Helper functions for TreeView/TreeModel */
    public class TreeHelpers {
        /**
         * Gets the last sibling of the given TreeIter
         *
         * @param model The owner TreeModel
         * @param iter The iter to query
         *
         * @return The last sibling of iter
         */
        public static TreeIter last_sibling (TreeModel model, TreeIter iter) {
            TreeIter out_iter = iter;
            bool has_next = true;
            for (TreeIter i = iter.copy (); has_next; has_next = model.iter_next (ref i)) {
                out_iter = i;
            }

            return out_iter;
        }

        /**
         * Gets the last (lowest) visible row of the given TreeView
         *
         * @param tree The tree view to query
         *
         * @return The last visible child of the tree
         */
        public static TreeIter last_visible (TreeView tree) {
            TreeIter iter;
            tree.model.get_iter_first (out iter);
            iter = last_sibling (tree.model, iter);

            while (tree.is_row_expanded (tree.model.get_path (iter))) {
                tree.model.iter_children (out iter, iter);
                iter = last_sibling (tree.model, iter);
            }

            return iter;
        }

        /**
         * Gets the next visible row after the given TreeIter
         *
         * @param tree The tree to query
         * @param iter The TreeIter to follow
         *
         * @return A TreeIter pointing to the next row, or the first row if the
         *         given iter was the last
         */
        public static TreeIter next_visible (TreeView tree, TreeIter iter) {
            TreeIter parent;
            TreeIter children;
            bool has_child = tree.model.iter_children (out children, iter);
            bool has_parent = tree.model.iter_parent (out parent, iter);
            if (has_child && tree.is_row_expanded (tree.model.get_path (iter))) {
                iter = children;
            } else if (!tree.model.iter_next (ref iter)) {
                if (has_parent && tree.model.iter_next (ref parent)) {
                    iter = parent;
                } else {
                    tree.model.get_iter_first (out iter);
                }
            }

            return iter;
        }

        /**
         * Gets the previous visible row after the given TreeIter
         *
         * @param tree The tree to query
         * @param iter The TreeIter to follow
         *
         * @return A TreeIter pointing to the previous row, or the last row if the
         *         given iter was the first
         */
        public static TreeIter prev_visible (TreeView tree, TreeIter iter) {
            TreeIter parent;
            TreeIter children;
            bool has_parent = tree.model.iter_parent (out parent, iter);
            if (!tree.model.iter_previous (ref iter)) {
                if (has_parent) {
                    iter = parent;
                } else {
                    iter = TreeHelpers.last_visible (tree);
                }
            } else {
                bool has_child = tree.model.iter_children (out children, iter);
                if (has_child && tree.is_row_expanded (tree.model.get_path (iter))) {
                    iter = TreeHelpers.last_sibling (tree.model, children);
                }
            }

            return iter;
        }
    }
}
