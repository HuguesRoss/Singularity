namespace Singularity.Db {
    /** The interface used for higher-level database IO */
    public interface IRequest : GLib.Object {
        /** The request priority. Higher priorities will be executed by the database before lower ones */
        public enum Priority {
            INVALID = -1,
            LOW,
            MEDIUM,
            HIGH,
            COUNT,
            DEFAULT = LOW
        }

        /** The status of a request. Used by the database to know when a request is finished */
        public enum Status {
            COMPLETED = 0,
            FAILED,
            CONTINUE,
            COUNT,
            DEFAULT = COMPLETED
        }

        /** Whether or not this request should be treated as a single DB transation */
        public abstract bool use_transaction { get; }

        /** The name of the request */
        public abstract string name { get; }

        /**
         * Called when the request is complete
         */
        public signal void processing_complete ();

        /**
         * Perform one statement of the query
         *
         * @param target The target of the query
         *
         * @return A Status object indicating the current status of this query
         */
        public abstract Status step (IDbTarget target);
    }
}
