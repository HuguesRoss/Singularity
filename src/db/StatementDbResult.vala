using Gee;

namespace Singularity.Db {
    /** IDbResult wrapping a Sqlite Statement */
    internal class StatementDbResult : Object, IDbResult {
        public bool finished { get { return _last_status != Sqlite.ROW; } }

        public StatementDbResult (Sqlite.Statement statement, int status) {
            _statement = statement;
            _last_status = status;
        }

        /**
         * Fetch a blob from the given column of the current row
         *
         * @param column The column ID
         *
         * @return A uint8[] containing the blob data
         */
        public uint8[] fetch_blob (int column) {
            ensure_column (column, "Failed to retrieve blob value at %d".printf (column));

            int bytes = _statement.column_bytes (column);
            var buffer = new Array<uint8> ();
            buffer.append_vals (_statement.column_blob (column),
            bytes);
            return buffer.data;
        }

        /**
         * Fetch an int from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public int fetch_int (int column) {
            ensure_column (column, "Failed to retrieve int value at %d".printf (column));
            return _statement.column_int (column);
        }

        /**
         * Fetch an int64 from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public int64 fetch_int64 (int column) {
            ensure_column (column, "Failed to retrieve int64 value at %d".printf (column));
            return _statement.column_int64 (column);
        }

        /**
         * Fetch a double from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public double fetch_double (int column) {
            ensure_column (column, "Failed to retrieve double value at %d".printf (column));
            return _statement.column_double (column);
        }

        /**
         * Fetch a string from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's text value
         */
        public string fetch_string (int column) {
            ensure_column (column, "Failed to retrieve string value at %d".printf (column));
            return _statement.column_text (column);
        }

        public int find_column (string key) {
            if (_columns.has_key (key)) {
                return _columns[key];
            }

            for (int i = 0; i < _statement.column_count (); i++) {
                string name = _statement.column_name (i);
                _columns[name] = i;
                if (name == key) {
                    return i;
                }
            }

            return -1;
        }

        /**
         * Fetch a blob from the given named column of the current row
         *
         * @param key The column name
         *
         * @return A uint8[] containing the blob data
         */
        public uint8[] get_blob (string key) {
            return fetch_blob (find_column (key));
        }

        /**
         * Fetch an int from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public int get_int (string key) {
            return fetch_int (find_column (key));
        }

        /**
         * Fetch an int64 from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public int64 get_int64 (string key) {
            return fetch_int64 (find_column (key));
        }

        /**
         * Fetch a double from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public double get_double (string key) {
            return fetch_double (find_column (key));
        }

        /**
         * Fetch a string from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's text value
         */
        public string get_string (string key) {
            return fetch_string (find_column (key));
        }

        /**
         * Advance the cursor to the next row.
         * Users should check finished afterwards to ensure there is more data to read
         */
        public void next () {
            if (!finished) {
                _last_status = _statement.step ();
            }
        }

        /**
         * Check that the column at a given index exists, and report an error
         * if it doesn't.
         *
         * @param column The column to check for
         * @param message The error message to report if the column is missing
         */
        private void ensure_column (int column, string message) {
            if (column < 0 || column > _statement.column_count ()) {
                error (message);
            }
        }

        private int _last_status = Sqlite.ROW;
        private unowned Sqlite.Statement _statement;
        private HashMap<string, int> _columns = new HashMap<string, int> ();
    }
}
