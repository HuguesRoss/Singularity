using Gtk;

namespace Singularity {
    [GtkTemplate (ui="/net/huguesross/Singularity/Views/SettingsWindow.ui")]
    class SettingsWindow : Adw.PreferencesDialog {
        public string link_command { get; set; default = "xdg-open %s"; }
        public File? cookie_db_file {
            get { return _cookie_db_file; }
            set {
                if (_cookie_db_file != value) {
                    _cookie_db_file = value;

                    if (_cookie_db_file == null) {
                        cookie_db_button.label = "Choose…";
                    } else {
                        cookie_db_button.label = _cookie_db_file.get_basename ();
                    }
                }
            }
        }
        private File? _cookie_db_file;

        public AppThemes theme_id { get; set; }
        public bool use_masonry_layout { get; set; }
        public bool update_on_startup { get; set; default = true; }
        public bool disable_csd { get; set; }
        public bool enable_javascript { get; set; }
        public AppScrollbars scroll { get; set; }
        public uint auto_update_preset {
            get { return _auto_update_preset; }
            set {
                if (_auto_update_preset != value) {
                    _auto_update_preset = value;
                    switch (_auto_update_preset) {
                        case 1:
                            auto_update_time = 5;
                            break;
                        case 2:
                            auto_update_time = 10;
                            break;
                        case 3:
                            auto_update_time = 30;
                            break;
                        case 4:
                            auto_update_time = 60;
                            break;
                    }
                    notify_property ("auto_update_preset");
                    notify_property ("has_custom_update_time");
                }
            }
        }
        private uint _auto_update_preset = 2;
        public bool has_custom_update_time { get { return auto_update_preset == 5; } }
        public int auto_update_time { get; set; default = 10; }
        public bool adaptive_rate_limit { get; set; default = false; }
        public string save_label { owned get; set; default = "Save"; }

        public signal void settings_saved (bool needs_restart);

        public SettingsWindow () {
            link_command = AppSettings.link_command;

            update_on_startup = AppSettings.start_update;
            adaptive_rate_limit = AppSettings.adaptive_rate_limit;
            disable_csd = AppSettings.disable_csd;
            _current_disable_csd = disable_csd;
            scroll = AppSettings.scroll;
            if (!AppSettings.auto_update) {
                auto_update_preset = 0;
            } else {
                switch (AppSettings.auto_update_freq) {
                    case 5:
                        auto_update_preset = 1;
                        break;
                    case 10:
                        auto_update_preset = 2;
                        break;
                    case 30:
                        auto_update_preset = 3;
                        break;
                    case 60:
                        auto_update_preset = 4;
                        break;
                    default:
                        auto_update_time = (int)AppSettings.auto_update_freq;
                        break;
                }
            }
            if (StringUtils.null_or_empty (AppSettings.cookie_db_path)) {
                cookie_db_file = null;
            } else {
                cookie_db_file = File.new_for_path (AppSettings.cookie_db_path);
            }
            enable_javascript = AppSettings.enable_javascript;

            theme_id = AppSettings.theme_id;

            use_masonry_layout = AppSettings.use_masonry_layout;

            var reset_action = new SimpleAction ("reset", VariantType.STRING);
            reset_action.activate.connect (reset_group);
            var save_action = new SimpleAction ("save", null);
            save_action.activate.connect (save);
            var group = new SimpleActionGroup ();
            group.add_action (reset_action);
            group.add_action (save_action);
            insert_action_group ("preferences", group);

            notify.connect ((spec) => {
                if (spec.owner_type == typeof (SettingsWindow)) {
                    save_label = (disable_csd != _current_disable_csd) ? "Save & Restart" : "Save";
                    add_toast (save_toast);
                }
            });
        }
        ~SettingsWindow () {
            WidgetUtils.cleanup_children (this);
        }

        [GtkCallback]
        private async void choose_db () {
            var filter = new FileFilter () { name = "Database Files", };
            filter.add_pattern ("*.sqlite");
            var dialog = new FileDialog () {
                title = "Select a Cookie Database File",
                accept_label = "Select",
                initial_file = cookie_db_file,
                modal = true,
                default_filter = filter,
            };
            try {
                cookie_db_file = yield dialog.open (Window.get_toplevels ().get_item (0) as Window, null);
            } catch (Error e) {
                warning (e.message);
            }
        }

        private void reset_group (Variant? param) {
            if (param == null) {
                return;
            }

            string group_id = param.get_string ();
            switch (group_id) {
                case "general":
                    update_on_startup = true;
                    auto_update_preset = 2;
                    link_command = "xdg-open %s";
                    cookie_db_file = null;
                    break;
                case "appearance":
                    theme_id = AppThemes.SYSTEM;
                    disable_csd = false;
                    scroll = AppScrollbars.SYSTEM;
                    break;
                case "security":
                    enable_javascript = false;
                    break;
                case "experimental":
                    use_masonry_layout = false;
                    adaptive_rate_limit = false;
                    break;
            }
        }

        private void save () {
            AppSettings.auto_update = auto_update_preset != 0;
            AppSettings.auto_update_freq = auto_update_time;
            AppSettings.theme_id = theme_id;
            AppSettings.start_update = update_on_startup;
            AppSettings.adaptive_rate_limit = adaptive_rate_limit;
            AppSettings.disable_csd = disable_csd;
            AppSettings.scroll = scroll;
            AppSettings.link_command = link_command;
            AppSettings.cookie_db_path = cookie_db_file != null ? cookie_db_file.get_path () : "";
            AppSettings.use_masonry_layout = use_masonry_layout;
            AppSettings.enable_javascript = enable_javascript;
            AppSettings.save ();

            settings_saved (disable_csd != _current_disable_csd);
        }

        [GtkCallback]
        public bool format_time (SpinButton spin) {
            int hours = ((int)spin.value) / 60;
            int minutes = ((int)spin.value) % 60;

            spin.text = "%02d:%02d".printf (hours, minutes);
            return true;
        }

        [GtkCallback]
        public int parse_time (SpinButton spin, out double new_value) {
            string[] values = spin.text.split_set (":");

            int hours = 0;
            int minutes = 0;
            if (values.length != 2 || !int.try_parse (values[0], out hours) || !int.try_parse (values[1], out minutes)) {
                new_value = 0;
                return INPUT_ERROR;
            }

            new_value = hours * 60 + minutes;

            return 1; // true
        }

        [GtkChild]
        private unowned Button cookie_db_button;
        [GtkChild]
        private unowned Adw.Toast save_toast;
        private bool _current_disable_csd;
    }
}
