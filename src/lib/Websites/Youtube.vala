namespace Singularity.Websites {
    /** Website-specific functions for youtube.com */
    public class Youtube : Object {
        /**
         * Generate an embed uri for a particular video
         *
         * @param video_id The id of the video
         *
         * @return The embed link
         */
        public static string embed_uri (string video_id) {
            return "https://www.youtube.com/embed/%s".printf (video_id);
        }

        /**
         * Check whether or not this is a youtube video link
         *
         * @param uri The uri to check
         * @param video_id Holds the video id, if this is a video link
         *
         * @return true if the uri is a video link; otherwise, false
         */
        public static bool matches_video_link (string uri, out string video_id) {
            try {
                string host;
                string path;
                string query;
                Uri.split (uri, UriFlags.ENCODED | UriFlags.PARSE_RELAXED, null, null, out host, null, out path, out query, null);
                if ((host == "www.youtube.com" || host == "youtube.com") && path == "/watch" && query.has_prefix ("v=")) {
                    video_id = query.substring (2);
                    return true;
                }
            } catch (Error _) { /* Not an error, just ignore this URL */ }

            video_id = null;
            return false;
        }
    }
}
