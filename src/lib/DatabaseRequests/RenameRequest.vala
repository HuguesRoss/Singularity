/*
    Singularity - A web newsfeed aggregator
    Copyright (C) 2017  Hugues Ross <hugues.ross@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Singularity.Db;

namespace Singularity {
    // A DatabaseRequest for renaming FeedCollections
    public class RenameRequest : SimpleRequest {
        public FeedDataEntry node { get; construct; }
        public string title { get; construct; }

        public RenameRequest (FeedDataEntry n, string t) {
            Object (node: n, title: t);
            _name = "Rename %s to %s".printf (node.title, title);
        }

        public override Statement build_query (IDbTarget db) {
            var q = new Statement (db, "UPDATE feeds SET 'title' = :title WHERE id = :id");
            q[":title"] = title;
            q[":id"] = node.id;
            return q;
        }
    }
}
