using Adw;
using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/Dialogs/WaitForDownloadsDialog.ui")]
    public class WaitForDownloadsDialog : Adw.AlertDialog {
        public DownloadsPresenter presenter { get; construct set; }
        public FilterListModel remaining_downloads { get; private set; }

        public WaitForDownloadsDialog (DownloadsPresenter dl) {
            Object (presenter: dl);
            remaining_downloads = new FilterListModel (
                presenter.downloads,
                new BoolFilter (
                    new PropertyExpression (typeof (Download), null, "active")
                    )
                );
            presenter.notify["pending-count"].connect (on_pending_count_changed);
            add_responses ("cancel", "Wait! I Changed My Mind!",
                           "exit", "Exit Now",
                           null);
            set_response_appearance ("exit", ResponseAppearance.DESTRUCTIVE);
        }

        ~WaitForDownloadsDialog () {
            _presenter.notify["pending-count"].disconnect (on_pending_count_changed);
            WidgetUtils.cleanup_children (this);
        }

        [GtkCallback]
        private void on_response (string response) {
            if (response == "exit") {
                GLib.Application.get_default ().quit ();
            }
        }

        private void on_pending_count_changed () {
            if (presenter.pending_count == 0) {
                GLib.Application.get_default ().quit ();
            } else {
                remaining_downloads.filter.changed (FilterChange.MORE_STRICT);
            }
        }
    }
}
