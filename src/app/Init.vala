namespace Singularity {
    public static void init () {
        typeof (DownloadEntry).ensure ();
        typeof (WebKit.WebView).ensure ();
        typeof (HeaderPanel).ensure ();
        typeof (StreamItemView).ensure ();
        typeof (ColumnItemView).ensure ();
        typeof (GridItemView).ensure ();
        typeof (FeedListItem).ensure ();
        typeof (VerticalPicture).ensure ();
    }
}
