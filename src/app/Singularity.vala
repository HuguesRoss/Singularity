using Gee;
using Singularity.Db;
using WebFeed;

namespace Singularity {
    const string APP_ID = "net.huguesross.singularity";
    const string APP_ID_PATH = "/net/huguesross/Singularity";

    // The primary application class
    public class SingularityApp : Adw.Application {
        public enum LoadStatus {
            NOT_STARTED,
            STARTED,
            COMPLETED,
            FAILED,
            COUNT
        }

        // Public section ---------------------------------------------------------
        public bool init_success { get; private set; }
        public bool has_subscriptions { get { return !m_feed_store.get_feeds ().is_empty; } }

        private MainLoop ml;
        public uint timeout_value = 600;
        public bool update_running = true;
        public uint update_next = 600;

        public UpdateContext update_context { get; private set; }
        public DataLoaderFactory loader_factory { get; private set; }
        public DocumentFactory document_factory { get; private set; }
        public FeedLocatorCollection feed_locators { get; private set; }
        public FeedParserFactory parser_factory { get; private set; }
        public XmlModuleFactory module_factory { get; private set; }
        public Subscriptions subscriptions { get; private set; }
        public UpdateQueueWrapper updates { get; private set; }

        public SingularityApp () {
            Object (application_id: APP_ID);

            this.startup.connect (start_run);
            this.activate.connect (activate_response);
            this.shutdown.connect (cleanup);
        }

        public void opml_import (File file) {
            Xml.Doc* doc = Xml.Parser.parse_file (file.get_path ());
            if (doc == null) {
                warning ("Can't parse XML file");
                return;
            }
            OPMLFeedDataSource opml = new OPMLFeedDataSource ();
            opml.parse_data (doc);
            foreach (var node in opml.data) {
                if (node is Feed) {
                    subscribe_to_feed_async.begin ((Feed)node, false);
                } else {
                    add_collection ((FeedCollection)node);
                }
            }
            delete doc; // FIXME: Some stray docs may be floating around from older xml code. Kill them.
        }

        public void opml_export (File file) {
            OPMLFeedDataSource opml = new OPMLFeedDataSource ();
            Xml.Doc* doc = opml.encode_data (m_feed_store.get_root ().nodes);

            FileStream fstream = FileStream.open (file.get_path (), "w");
            doc->dump (fstream);
        }

        public void update_settings () {
            AppSettings.save ();
            update_context.cookie_db_path = AppSettings.cookie_db_path;
            update_context.cache_valid_minutes = AppSettings.force_update_freq;
            if (AppSettings.auto_update && !update_running) {
                update_running = true;
                update_next = timeout_value;
                /* Timeout.add_seconds (timeout_value, update); */
            }
        }

        public void exit () {
        }

        public FeedListTreeStore get_feed_store () {
            return m_feed_store;
        }

        /**
        * Asynchronously save a new subscription to the DB and update the UI
        *
        * @param f The new feed to save
        * @param loaded Whether or not the feed's content has been loaded
        * @param parent (Optional) The parent node to save the feed under
        * @param items (Optional) The feed's items to save, if any
        */
        public async void subscribe_to_feed_async (Feed f, bool loaded, FeedCollection? parent = null, Gee.List<Item?>? items = null) {
            if (!yield subscriptions.try_add (f)) {
                return;
            }

            CollectionNode node = new CollectionNode (f);
            node.data.parent = parent;
            m_feed_store.append_node (node);

            if (!loaded) {
                updates.request_update (f);
            } else if (items != null) {
                // Save the package to the db
                var new_package = new UpdatePackage.success (f, items);
                yield m_database.execute_request_async (new UpdatePackageRequest (new_package) { use_owner_id = false, }, IRequest.Priority.MEDIUM);
                foreach (Item i in items) {
                    _cache.insert (i);
                }

                // Update the feed's unread count from the db
                var ureq = new GetUnreadCountRequest (new_package.feed);
                yield m_database.execute_request_async (ureq, IRequest.Priority.MEDIUM);
            }

            subscribe_done (f);
        }

        /**
        * Asynchronously save updates to the DB and update the UI when finished
        *
        * @param update A successful UpdatePackage
        */
        public async void save_updates_async (UpdatePackage update) {
            yield m_database.execute_request_async (new UpdatePackageRequest (update), IRequest.Priority.DEFAULT);

            // Update the feed's unread count from the db
            var ureq = new GetUnreadCountRequest (update.feed);
            yield m_database.execute_request_async (ureq, IRequest.Priority.MEDIUM);

            foreach (Item i in update.new_items) {
                _cache.insert (i);
            }
        }

        public void add_collection (FeedCollection c, FeedCollection? parent = null) {
            CollectionNode node = new CollectionNode (c);
            node.data.parent = parent;

            SubscribeRequest req = new SubscribeRequest (c);
            m_database.execute_request_async.begin (req, IRequest.Priority.MEDIUM, () => {
                m_feed_store.append_node (node, m_feed_store.get_node (parent));

                // Notify any newly-subscribed feeds
                foreach (Feed f in c.get_feeds ()) {
                    _cache.insert_feed (f);
                    subscribe_done (f);
                }
            });
        }

        public void check_for_updates (bool force = false) {
            if (m_feed_store == null)
                return;

            Gee.Collection<Feed> feeds_collection = m_feed_store.get_feeds ();
            if (!force) {
                // TODO: We should see if we can make a class that wraps Iterator with an Iterable, so we can avoid this list-copying mess
                var new_collection = new Gee.ArrayList<Feed> ();
                new_collection.add_all_iterator (feeds_collection.filter (feed => force || should_update_feed (feed)));
                feeds_collection = new_collection;
            }

            updates.request_updates (feeds_collection);
        }

        public void toggle_unread (Item i) {
            ItemToggleRequest req = new ItemToggleRequest (i.guid, ItemToggleRequest.ToggleField.UNREAD);
            m_database.execute_request_async.begin (req, IRequest.Priority.HIGH, () => {
                if (i.owner != null) {
                    m_database.queue_request (new GetUnreadCountRequest (i.owner), IRequest.Priority.MEDIUM);
                }
            });

            i.unread = !i.unread;
        }

        public void toggle_star (Item i) {
            ItemToggleRequest req = new ItemToggleRequest (i.guid, ItemToggleRequest.ToggleField.STARRED);
            m_database.queue_request (req);

            i.starred = !i.starred;
        }

        // Signals ----------------------------------------------------------------
        public signal void load_status_changed (LoadStatus status);
        public signal void subscribe_done (Feed f);

        // Private section --------------------------------------------------------
        private Database m_database;
        private ItemCache _cache;
        private FeedListTreeStore? m_feed_store = null;

        private void start_run () {
            AppSettings.load (APP_ID);

            module_factory = new XmlModuleFactory ();
            module_factory.register (typeof (AtomXmlModule), AtomXmlModule.NAMESPACE);
            module_factory.register (typeof (DublinCoreXmlModule), DublinCoreXmlModule.NAMESPACE);
            module_factory.register (typeof (MediaRssXmlModule), MediaRssXmlModule.NAMESPACE);
            module_factory.register (typeof (Rss1XmlModule), Rss1XmlModule.NAMESPACE);
            module_factory.register (typeof (RdfContentXmlModule), RdfContentXmlModule.NAMESPACE);
            module_factory.register (typeof (YoutubeXmlModule), YoutubeXmlModule.NAMESPACE);

            loader_factory = new DataLoaderFactory (typeof (HttpDataLoader));
            // Not shipped by default, uncomment for testing on local files
            // loader_factory.register_for_scheme ("file", typeof (FileDataLoader));

            document_factory = new DocumentFactory ();
            document_factory.register (typeof (TextDocument), TextDocument.create);
            document_factory.register (typeof (HtmlDocument), HtmlDocument.create); // Must be registered before XmlDocument to only catch HTML that XmlDocument can't handle
            document_factory.register (typeof (JsonDocument), JsonDocument.create);
            document_factory.register (typeof (XmlDocument), XmlDocument.create);

            feed_locators = new FeedLocatorCollection ();
            feed_locators.register (new HtmlFeedLocator ());

            parser_factory = new FeedParserFactory ();
            parser_factory.register (typeof (RdfFeedParser), (data, out parser) => {
                parser = null;
                if (RdfFeedParser.is_rdf_feed (data)) {
                    parser = create_xml_parser (typeof (RdfFeedParser), data);
                }

                return parser != null;
            });
            parser_factory.register (typeof (RssFeedParser), (data, out parser) => {
                parser = null;
                if (RssFeedParser.is_rss_feed (data)) {
                    parser = create_xml_parser (typeof (RssFeedParser), data);
                }

                return parser != null;
            });
            parser_factory.register (typeof (AtomFeedParser), (data, out parser) => {
                parser = null;
                if (AtomFeedParser.is_atom_feed (data)) {
                    parser = create_xml_parser (typeof (AtomFeedParser), data);
                }

                return parser != null;
            });
            parser_factory.register (typeof (JsonFeedParser), (data, out parser) => {
                parser = null;
                if (JsonFeedParser.is_json_feed (data)) {
                    parser = new JsonFeedParser () {
                        data = data
                    };
                }

                return parser != null;
            });

            WebElementBuilders.embed_factory.register (typeof (ImageEmbedBuilder));
            WebElementBuilders.embed_factory.register (typeof (VideoEmbedBuilder));
            WebElementBuilders.embed_factory.register (typeof (AudioEmbedBuilder));
            WebElementBuilders.embed_factory.register (typeof (MediaSiteEmbedBuilder));

            m_database = new Database.from_path (AppSettings.Arguments.database_path, "resource://%s/schemas".printf (APP_ID_PATH));
            _cache = new ItemCache (m_database);
            subscriptions = new Subscriptions (m_database) {
                cache = _cache,
            };
            update_context = new UpdateContext (m_database) {
                cookie_db_path = AppSettings.cookie_db_path,
                cache_valid_minutes = AppSettings.force_update_freq,
                icons = new TextureCache (),
                xml_modules = module_factory,
            };
            load_status_changed (LoadStatus.STARTED);
            updates = new UpdateQueueWrapper (
                new UpdateQueue (loader_factory, document_factory, parser_factory, update_context)
            );
            updates.update_ready.connect (package => save_updates_async.begin (package));
            updates.update_failed.connect ((package) => m_feed_store.set_failed (package.feed.id));

            GLib.SimpleAction update_action = new GLib.SimpleAction ("check_full", null);
            update_action.activate.connect (() => { check_for_updates (true); });
            add_action (update_action);

            GLib.SimpleAction quit_action = new GLib.SimpleAction ("quit", null);
            quit_action.activate.connect (this.quit);
            add_action (quit_action);
            set_accels_for_action ("view.view_type::stream", { "<Control>1" });
            set_accels_for_action ("view.view_type::column", { "<Control>2" });
            set_accels_for_action ("view.view_type::grid", { "<Control>3" });

            load_feeds_async.begin (() => {
                if (AppSettings.start_update) {
                    check_for_updates ();
                }

                load_status_changed (LoadStatus.COMPLETED);
                init_success = true;
                create_window ();
            });
        }

        private void create_window () {
            unowned GLib.List<Gtk.Window> windows = get_windows ();
            if (windows.is_empty ()) {
                var view = new MainView (this, new MainPresenter (m_database), _cache);
                view.unsub_requested.connect ((f) => {
                    if (f != null) {
                        subscriptions.try_remove.begin (f, (obj, res) => {
                            if (subscriptions.try_remove.end (res)) {
                                m_feed_store.remove_data (f);
                                updates.cancel_update (f);
                            }
                        });
                    }
                });

                view.delete_collection_requested.connect ((c) => {
                    if (c != null) {
                        DeleteCollectionRequest req = new DeleteCollectionRequest (c);
                        m_database.execute_request_async.begin (req, IRequest.Priority.HIGH, () => {
                                m_feed_store.remove_data (c);
                            });
                        }
                    });

                view.new_collection_requested.connect ((parent) => {
                    int parent_id = (parent == null) ? -1 : parent.id;
                    CollectionNode parent_node = m_feed_store.get_node (parent) as CollectionNode;
                    if (parent_node == null) {
                        parent_id = -1;
                    }
                    var req = new CreateCollectionRequest (new FeedCollection ("Untitled"), parent_id);
                    m_database.execute_request_async.begin (req, IRequest.Priority.HIGH, () => {
                        m_feed_store.append_node (new CollectionNode (req.node), parent_node);
                    });
                });

                view.restart_requested.connect (() => {
                    var old_window = view.get_root () as Gtk.ApplicationWindow;
                    if (old_window is Adw.ApplicationWindow) {
                        old_window.close_request.disconnect (((MainView)((Adw.ApplicationWindow)old_window).content).on_close_requested);
                        ((Adw.ApplicationWindow)old_window).content = null;
                    } else {
                        old_window.close_request.disconnect (((MainView)old_window.child).on_close_requested);
                        old_window.child = null;//view.unparent ();
                    }

                    var window = build_window (view);
                    this.add_window (window);
                    window.present ();

                    old_window.close ();
                });

                var window = build_window (view);
                this.add_window (window);
                window.present ();
            } else {
                var window = windows.nth_data (0);
                window.present ();
            }
        }

        private Gtk.ApplicationWindow build_window (MainView content) {
            Gtk.ApplicationWindow window;
            if (AppSettings.disable_csd) {
                window = new Gtk.ApplicationWindow (this) { child = content, };
            } else {
                window = new Adw.ApplicationWindow (this) { content = content, };
            }

            window.default_width = 1600;
            window.default_height = 900;
            window.icon_name = "net.huguesross.Singularity";
            window.close_request.connect (content.on_close_requested);

            return window;
        }

        private void activate_response () {
            if (m_feed_store == null) {
                m_feed_store = new FeedListTreeStore ();
                m_feed_store.parent_changed.connect ((node, id) =>
                {
                    UpdateParentRequest req = new UpdateParentRequest (node, id);
                    m_database.execute_request_async.begin (req, IRequest.Priority.HIGH);
                });

                m_feed_store.rename_requested.connect ((node, title) => {
                    RenameRequest req = new RenameRequest (node.data, title);
                    m_database.execute_request_async.begin (req, IRequest.Priority.HIGH, () => {
                        node.data.title = title;
                    });
                });
            }

            if (!AppSettings.Arguments.background) {
                if (!init_success) {
                    ml = new MainLoop ();
                    TimeoutSource timer = new TimeoutSource (200);
                    if (!AppSettings.start_update)
                        check_for_updates ();
                    timer.set_callback ( () => {
                        // FIXME: This might drop sometimes? Needs testing.
                        if (!init_success) {
                            create_window ();
                        }
                        ml.quit ();
                        ml = null;
                        return false;
                    });
                    timer.attach (ml.get_context ());
                    ml.run ();
                }
            } else {
                ml = new MainLoop ();
                if (AppSettings.Arguments.background_update) {
                    TimeoutSource timer = new TimeoutSource (1000);
                    if (!AppSettings.start_update)
                        check_for_updates ();
                    timer.set_callback ( () => {
                        // FIXME: This might drop sometimes? Needs testing.
                        if (init_success && updates.is_updating && m_database.pending_requests) {
                            ml.quit ();
                            return false;
                        }
                        return true;
                    });
                    timer.attach (ml.get_context ());
                } else if (AppSettings.Arguments.list_feeds) {
                    TimeoutSource timer = new TimeoutSource (1000);
                    timer.set_callback ( () => {
                        if (init_success) {
                            foreach (Feed f in m_feed_store.get_feeds ())
                                stdout.printf ("%s\n", f.to_string ());
                            ml.quit ();
                            return false;
                        }
                        return true;
                    });
                    timer.attach (ml.get_context ());
                } else if (AppSettings.Arguments.subscribe_url != null) {
                    TimeoutSource timer = new TimeoutSource (1000);
                    bool started_subscribe = false;
                    bool finished_subscribe = false;
                    timer.set_callback ( () => {
                        if (init_success) {
                            if (!started_subscribe) {
                                started_subscribe = true;
                                Feed to_build = new Feed ();
                                to_build.link = AppSettings.Arguments.subscribe_url;
                                subscribe_done.connect ((f) => { finished_subscribe = true; });
                                subscribe_to_feed_async.begin (to_build, false);
                            } else if (finished_subscribe) {
                                ml.quit ();
                                return false;
                            }
                        }
                        return true;
                    });
                    timer.attach (ml.get_context ());
                } else {
                    // We don't want to hang here
                    return;
                }
                ml.run ();
            }
        }

        private void cleanup () {
            AppSettings.save ();
        }

        /**
        * Check if we should update the feed, according to current update settings and check timestamps
        * This check is ignored for manual user updates
        *
        * @param feed The feed to update
        * @return true if the update should be performed
        */
        private bool should_update_feed (Feed feed) {
            var now = new DateTime.now_utc ();
            TimeSpan since_last_update = now.difference (feed.last_update);

            // Are we before the global cutoff?
            if (feed.last_update.add_minutes ((int)AppSettings.auto_update_freq).compare (now) > 0)
                return false;

            // Is adaptive limiting available?
            if (AppSettings.adaptive_rate_limit && !feed.last_new_content.equal (new DateTime.from_unix_utc (0))) {
                TimeSpan since_new_content = now.difference (feed.last_new_content);

                // Cutoffs and intervals for adaptive limiting. The goal is to be conservative with check reduction,
                // to avoid missing or delaying posts by an unacceptable duration. We always check once a week minimum.
                //
                // Just for good measure, we round our cutoffs up and our intervals down to check *slightly* more often--the goal is
                // to reduce the number of feed requests without impacting the user experience at all.
                const TimeSpan[] CUTOFFS = {
                    TimeSpan.DAY * 366, // A full year with no updates, possible for sporadic feeds but probably on hiatus
                    TimeSpan.DAY * 62,  // 2 months, past the usual for monthly posting
                    TimeSpan.DAY * 8,   // 8 days, past the usual for weekly posting
                    TimeSpan.DAY * 4,   // 4 days, past the usual for Mon-Wed-Fri
                };
                const TimeSpan[] INTERVALS = {
                    TimeSpan.DAY * 6,   // Once a week (minus 1)
                    TimeSpan.DAY * 3,   // Twice a week
                    TimeSpan.HOUR * 23, // Once a day
                    TimeSpan.HOUR * 7,  // Twice a day, minus 1 (assuming the user is asleep for ~8 hours a day)
                };

                // Check each cutoff starting from the longest to the shortest.
                // If the last update is at or past this cutoff, we check at the corresponding interval
                for (int i = 0; i < CUTOFFS.length && i < INTERVALS.length; ++i) {
                    if (since_new_content >= CUTOFFS[i]) {
                        return since_last_update >= INTERVALS[i];
                    }
                }
            }

            // Good to go!
            return true;
        }

        /**
        * Load the full feed & item data
        */
        private async void load_feeds_async () {
            var feeds = yield subscriptions.load ();
            if (m_feed_store == null) {
                m_feed_store = new FeedListTreeStore.from_collection (feeds);
            } else {
                m_feed_store.append_root_collection (feeds);
            }

            // Low priority -- load the icons. This isn't critical, so we can do it asynchronously
            foreach (Feed f in subscriptions) {
                load_icon.begin (f);
            }
        }

        private IFeedParser create_xml_parser (Type type, IDocument data) {
            return (IFeedParser)Object.new (type, "data", data, "modules", module_factory, "estimate_ratings", true);
        }

        private async void load_icon (Feed f) {
            var ireq = new LoadIconRequest (f);
            yield m_database.execute_request_async (ireq, IRequest.Priority.LOW);
            if (ireq.icon != null) {
                f.icon = ireq.icon;
            }
        }
    }
}
