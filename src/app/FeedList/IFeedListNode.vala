using Gdk;
using Gee;

namespace Singularity {
    public interface IFeedListNode : Object {
        public abstract bool can_filter { get; }
        public abstract bool can_rename { get; }
        public abstract bool has_error { get; }
        public abstract bool has_unread { get; }
        public abstract string icon_name { get; }
        public abstract Paintable? icon { get; }
        public abstract string sort_key { get; }
        public abstract string title { get; }
        public abstract string type_name { get; }
        public abstract string description { get; }
        public abstract int unread_count { get; }

        public signal void rename (string name);

        public abstract Gee.List<IFeedListNode> get_children ();
        public abstract ItemQuery query ();
        public virtual void refresh_unread () { /* No-op */ }
    }
}
