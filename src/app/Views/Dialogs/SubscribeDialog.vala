using Adw;
using Gtk;
using WebFeed;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/Dialogs/SubscribeDialog.ui")]
    public class SubscribeDialog : Adw.Dialog {
        public bool has_results { get { return _error_type != LocateFeedsJob.ErrorType.NO_ERROR || feeds.n_items > 0; } }
        public string view_state { owned get { return _error_type == LocateFeedsJob.ErrorType.NO_ERROR ? "feeds" : "error"; } }
        public string error_message { owned get { return ERROR_MESSAGES[_error_type]; } }
        public string error_details { owned get; private set; }

        public GLib.ListStore feeds { get { return _feeds; } }
        private GLib.ListStore _feeds = new GLib.ListStore (typeof (SubscriptionOption));

        public bool is_loading { get; private set; }

        public unowned DataLoaderFactory loader_factory { get; set; }
        public unowned DocumentFactory document_factory { get; set; }
        public unowned FeedParserFactory parser_factory { get; set; }
        public unowned FeedLocatorCollection feed_locators { get; set; }
        public unowned UpdateContext context { get; set; }
        public unowned Subscriptions subscriptions { get; set; }

        public signal void new_subscription (Feed feed, Gee.List<Item?> items);

        construct {
            var sub_action = new SimpleAction ("add", VariantType.UINT32);
            sub_action.activate.connect (id => {
                var option = _feeds.get_object (id.get_uint32 ()) as SubscriptionOption;
                if (option == null) {
                    return;
                }

                option.subscribe_state = "subscribing";

                new_subscription (option.feed, option.items);
            });

            var sub_group = new SimpleActionGroup ();
            sub_group.add_action (sub_action);
            insert_action_group ("subscription", sub_group);

            // 1 thread, job interval of .5 seconds. We also have a built-in delay for user entry, so this delay only applies to loading feeds after finding them
            _jobs = new JobQueue (1, 500000);
        }

        private void set_error (LocateFeedsJob.ErrorType error = LocateFeedsJob.ErrorType.NO_ERROR, string details = "") {
            is_loading = false;
            _error_type = error;
            error_details = details;

            notify_property ("error-message");
            notify_property ("view-state");

            if (error != LocateFeedsJob.ErrorType.NO_ERROR) {
                warning ("%s: %s", error_message, details);
            }

            notify_property ("has-results");
        }

        private async void check_url (string url) {
            is_loading = true;
            var job = new LocateFeedsJob (url) {
                loader_factory = loader_factory,
                document_factory = document_factory,
                parser_factory = parser_factory,
                feed_locators = feed_locators,
            };
            var result = yield _jobs.request_and_wait (job);
            if (result == Job.Result.CANCELLED) {
                return;
            }

            if (result == Job.Result.FAILED) {
                set_error (job.error_type, job.error_message);
                return;
            }

            foreach (var feed in job.feeds) {
                if (feed.loaded) {
                    feeds.append (new SubscriptionOption.loaded (feed.feed, feed.items, context.icons, subscriptions));
                } else {
                    feeds.append (new SubscriptionOption (feed.feed, _jobs, context.icons, loader_factory, document_factory, parser_factory, subscriptions));
                }
            }

            if (_feeds.n_items == 0) {
                set_error (LocateFeedsJob.ErrorType.NO_FEEDS, "This page doesn't list any feeds you can subscribe to");
            } else {
                set_error ();
            }
        }

        [GtkCallback]
        private void on_url_changed (Editable source) {

            bool was_checking = _jobs.cancel_all ();
            feeds.remove_all ();

            _url = source.get_chars ();
            if (StringUtils.null_or_empty (_url)) {
                set_error ();
                return;
            }

            if (_waiting) {
                return;
            }

            if (!was_checking) {
                check_url.begin (_url);
            } else {
                _waiting = true;
                Timeout.add_seconds (2, () => {
                    check_url.begin (_url);
                    _waiting = false;
                    return false;
                });
            }
        }

        [GtkCallback]
        private void on_close () {
            url_entry.set_text ("");
        }

        [GtkCallback]
        private Variant uint_to_variant (uint val) {
            return new Variant.uint32 (val);
        }

        private LocateFeedsJob.ErrorType _error_type;
        private JobQueue _jobs;

        private bool _waiting;
        private string _url;

        [GtkChild]
        private unowned Entry url_entry;

        private const string[] ERROR_MESSAGES = {
            "",
            "Unknown Address",
            "Connection Error",
            "Unknown Content",
            "No Feeds Here!",
        };
    }

    internal class SubscriptionOption : Object {
        public Feed feed { get; private set; }
        public string state { owned get; private set; }
        public string subscribe_state { owned get; set; }
        public Gdk.Paintable? icon_paintable { get { return feed.icon; } }
        public TextureCache texture_cache { get; private set; }
        public unowned DataLoaderFactory loader_factory { get; set; }
        public unowned DocumentFactory document_factory { get; set; }
        public unowned Subscriptions subscriptions { get; set; }
        public unowned FeedParserFactory parser_factory { get; set; }

        public string items_count { owned get {
            if (_error) { return "Can't load. Link might be broken?"; }

            if (_feed_loading) { return "Loading\u2026"; }

            if (items.size == 0) { return "Empty"; }

            if (items.size == 1) { return "1 item"; }

            return "%d items".printf (items.size);
        } }

        public string title { owned get { return feed.title; }}

        public string link { owned get { return feed.link; }}

        public string description { owned get { return feed.description; }}

        public Gee.ArrayList<Item> items { get { return _feed_loading ? null : _items; }}
        private Gee.ArrayList<Item> _items = new Gee.ArrayList<Item> ();

        public SubscriptionOption (WebFeed.Feed feed_src, JobQueue jobs, TextureCache icons, DataLoaderFactory loaders, DocumentFactory documents, FeedParserFactory parsers, Subscriptions subs) {
            loader_factory = loaders;
            document_factory = documents;
            parser_factory = parsers;
            texture_cache = icons;
            subscriptions = subs;

            feed = new Feed () {
                link = feed_src.feed_uri,
            };
            feed.update_contents (feed_src);

            state = "loading";
            refresh_sub_state ();
            subscriptions.subscription_added.connect (refresh_sub_state);
            subscriptions.subscription_removed.connect (refresh_sub_state);
            load_feed.begin (jobs);
        }

        public SubscriptionOption.loaded (WebFeed.Feed feed_src, Gee.Collection<WebFeed.Item> items_src, TextureCache icons, Subscriptions subs) {
            texture_cache = icons;
            subscriptions = subs;
            _feed_loading = false;

            feed = new Feed () {
                link = feed_src.feed_uri,
            };
            feed.update_contents (feed_src);

            foreach (WebFeed.Item item in items_src) {
                if (UpdateGenerator.ensure_item_guid (item)) {
                    _items.add (new Item.from_webfeed (item));
                }
            }

            state = "loading";
            refresh_sub_state ();
            subscriptions.subscription_added.connect (refresh_sub_state);
            subscriptions.subscription_removed.connect (refresh_sub_state);
            load_icon.begin ();
        }

        ~SubscriptionOption () {
            subscriptions.subscription_added.disconnect (refresh_sub_state);
            subscriptions.subscription_removed.disconnect (refresh_sub_state);
        }

        /**
         * Try to load the content of the feed linked to this item
         */
        private async void load_feed (JobQueue jobs) {
            var job = new LoadFeedJob (feed.link) {
                feed = feed,
                loader_factory = loader_factory,
                document_factory = document_factory,
                parser_factory = parser_factory,
            };
            var result = yield jobs.request_and_wait (job);
            _items = job.items;
            _feed_loading = false;
            _error = result == Job.Result.FAILED;
            notify_property ("items-count");

            if (result == Job.Result.SUCCEEDED) {
                yield load_icon ();
            }
        }

        /**
         * Try to load the feed icon, if one exists
         */
        private async void load_icon () {
            if (feed.icon_uri != null) {
                feed.icon_uri = UriUtils.make_absolute (feed.icon_uri, feed.base_uri);
                feed.icon = yield texture_cache.get_for_uri (feed.icon_uri);
                notify_property ("icon-paintable");
            }

            state = "loaded";
        }

        private void refresh_sub_state () {
            subscribe_state = subscriptions.get_by_uri (feed.link) == null ? "ready" : "subscribed";
        }

        private bool _feed_loading = true;
        private bool _error = false;
    }
}
