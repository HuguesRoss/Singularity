using Gtk;

namespace Singularity {
    // TreeStore for holding CollectionNodes.
    // Provides additional helper functions for managing nodes.
    public class FeedListTreeStore : TreeStore {
        public const string FEED_CATEGORY_STRING = "All Subscriptions";

        public delegate void TreeForeachFunc (TreeListRow? row);
        public delegate void FeedForeachFunc (Feed feed);

        public TreeListModel tree_model { get; construct set; }

        construct {
            root_collection.custom_icon = "go-home-symbolic";

            var root = new GLib.ListStore (typeof (IFeedListNode));
            root.append (root_node);
            root.append (bookmarks_node);
            child_map[-1] = root;
            tree_model = new TreeListModel (root, false, false, (node) => {
                if (node is CollectionNode) {
                    var collection_node = (CollectionNode)node;
                    if (collection_node.data is FeedCollection && collection_node.data != root_collection) {
                        if (!child_map.has_key (collection_node.id)) {
                            child_map[collection_node.id] = new GLib.ListStore (typeof (IFeedListNode));
                        }
                        return child_map[collection_node.id];
                    }
                }

                return null;
            });

            // Expand the root node
            TreeListRow? root_row = tree_model.get_child_row (0);
            if (root_row != null) {
                root_row.expanded = true;
            }
        }

        public FeedListTreeStore.from_collection (FeedCollection fc) {
            append_root_collection (fc);
        }

        public signal void parent_changed (FeedDataEntry node, int id);
        public signal void rename_requested (CollectionNode node, string new_name);

        // Adds the contents of a FeedCollection to the root node.
        public void append_root_collection (FeedCollection fc) {
            foreach (var n in fc.nodes) {
                append_internal (new CollectionNode (n));
            }
        }

        public FeedCollection get_root () {
            return root_collection;
        }

        public void @foreach (TreeForeachFunc fn) {
            for (int i = 0; i < tree_model.n_items; i++) {
                fn (tree_model.get_row (i));
            }
        }

        // Adds a single node, with the option of providing its parent.
        // If the parent is null, the root node will be used.
        public void append_node (CollectionNode node, IFeedListNode? parent = null) {
            append_internal (node, parent);

            if (parent is CollectionNode) {
                ((CollectionNode)parent).add_child (node);
            }
        }

        // Moves node src into dest, keeping the hierarchy of src intact.
        // If dest is null, src will be moved to the root
        public bool move_node (CollectionNode src, CollectionNode? dest) {
            if (src.data == root_collection || !node_map.has_key (src.data.id) || (dest != null && !node_map.has_key (dest.data.id)))
                return false;

            FeedCollection c;
            if (dest == null) {
                c = root_collection;
            } else {
                if (dest.data is Feed)
                    c = dest.data.parent;
                else
                    c = dest.data as FeedCollection;
            }

            if (src.data == c)
                return false;

            CollectionNode parent_node = get_node (c);

            reparent (src, parent_node);

            parent_changed (src.data, c.id);

            return true;
        }

        public bool contains (int id) {
            return get_node_from_id (id) != null;
        }

        // Returns the FeedDataEntry corresponding to the given id.
        public FeedDataEntry? get_data_from_id (int id) {
            CollectionNode node = get_node_from_id (id);
            if (node != null)
                return node.data;

            return null;
        }

        // Returns the CollectionNode corresponding to the given id.
        public CollectionNode? get_node_from_id (int id) {
            return node_map[id];
        }

        public CollectionNode? get_node (FeedDataEntry? entry) {
            if (entry == null) {
                return null;
            }
            return node_map[entry.id];
        }

        // Removes the given CollectionNode from the store.
        public void remove_node (CollectionNode n) {
            var parent_node = node_map[n.data.parent_id];
            GLib.ListStore parent_store = child_map[n.data.parent_id];
            uint pos = -1;
            if (!parent_store.find (n, out pos)) {
                warning ("Cannot remove %s from %s", n.data.to_string (), n.data.parent.to_string ());
                return;
            }

            if (parent_node != null) {
                parent_node.remove_child (n);
            }

            if (child_map.has_key (n.id)) {
                foreach (IFeedListNode child in n.get_children ()) {
                    if (n is CollectionNode) {
                        move_node ((CollectionNode)child, node_map[n.data.parent_id]);
                    }
                }
            }

            n.rename.disconnect (rename);
            parent_store.remove (pos);
            node_map.unset (n.id);
            child_map.unset (n.id);
        }

        // Removes the given FeedDataEntry from the store.
        public void remove_data (FeedDataEntry d) {
            remove_node (node_map[d.id]);
        }

        private void append_internal (CollectionNode node, IFeedListNode? parent = null) {
            if (node_map.has_key (node.id))
                return;

            if (parent is CollectionNode) {
                child_map[((CollectionNode)parent).id].append (node);
            } else {
                root_collection.add_node (node.data);
                child_map[-1].append (node);
            }

            node.rename.connect (rename);
            node_map.set (node.id, node);
            if (node.data is FeedCollection && !child_map.has_key (node.id)) {
                child_map[node.id] = new GLib.ListStore (typeof (IFeedListNode));
            }

            foreach (IFeedListNode n in node.get_children ()) {
                if (n is CollectionNode) {
                    append_internal ((CollectionNode)n, node);
                }
            }

            // Update parent unread count
            (parent ?? root_node).refresh_unread ();
        }

        private void rename (IFeedListNode node, string new_name) {
            if (node is CollectionNode) {
                rename_requested ((CollectionNode)node, new_name);
            }
        }

        // Updates the given node's state to be failed
        // TODO: This should be replaced by a better system for tracking sucess/failure
        public void set_failed (int id) {
            if (id != -1) {
                CollectionNode n = get_node_from_id (id);
                if (n == null)
                    return;
                n.set_failed ();
            }
        }

        public Gee.Collection<Feed> get_feeds () {
            return root_collection.get_feeds ();
        }

        // A recursive subtree copy, used in move_node
        private void reparent (CollectionNode src, CollectionNode? dest) {
            GLib.ListStore parent_store = child_map[src.data.parent_id];
            uint pos = -1;
            if (!parent_store.find (src, out pos)) {
                warning ("Cannot reparent %s: Unable to locate node id", src.data.to_string ());
                return;
            }

            if (dest != null && !(dest.data is FeedCollection)) {
                warning ("Cannot reparent %s from %s to %s", src.data.to_string (), src.data.parent.to_string (), dest.data.to_string ());
                return;
            }

            var parent_node = node_map[src.data.parent_id];

            if (parent_node != null) {
                parent_node.remove_child (src);
            }

            parent_store.remove (pos);

            if (dest == null) {
                src.data.parent.remove_node (src.data);
                child_map[-1].append (src);
            } else {
                ((FeedCollection)dest.data).add_node (src.data);
                child_map[dest.id].append (src);
                dest.add_child (src);
            }
        }

        private Gee.HashMap<int, CollectionNode> node_map = new Gee.HashMap<int, CollectionNode> ();
        private Gee.HashMap<int, GLib.ListStore> child_map = new Gee.HashMap<int, GLib.ListStore> ();
        private IFeedListNode root_node = new AllSubscriptionsNode ();
        private IFeedListNode bookmarks_node = new BookmarksNode ();
        private FeedCollection root_collection = new FeedCollection (FEED_CATEGORY_STRING) {
            sort_key = "",
        };
    }
}
