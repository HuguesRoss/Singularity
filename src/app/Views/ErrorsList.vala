using Gtk;
using Gee;

namespace Singularity {
    /**
     * Widget that displays feed update errors
     */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/ErrorsList.ui")]
    public class ErrorsList : Widget {
        /**
         * The number of recorded feeds with update errors
         */
        public uint error_count { get { return errors_by_id.size; } }
        public bool has_errors { get { return error_count > 0; } }
        public string errors_label { owned get { return "%u error%s".printf (error_count, error_count > 1 ? "s" : ""); } }

        construct {
            errors_box.set_header_func (ListBoxHelpers.header_separator);
        }

        ~ErrorsList () {
            WidgetUtils.cleanup_children (this);
        }

        /**
         * Add a new feed error
         *
         * @param f The feed that has the error
         * @param message The error message
         */
        public void add_error (Feed f, string message) {
            errors_by_id[f.id] = message;

            var label = new ErrorLabel (f, message);
            label.margin_top = 6;
            label.margin_bottom = 6;
            label.margin_start = 6;
            label.margin_end = 6;
            errors_box.insert (label, -1);

            notify_property ("error_count");
            notify_property ("errors_label");
            notify_property ("has_errors");
        }

        [GtkCallback]
        private void row_clicked (ListBoxRow row) {
            var label = row.child as ErrorLabel;
            if (label != null) {
                label.on_copy ();
            }
        }

        private HashMap<uint, string> errors_by_id = new HashMap<uint, string> ();

        [GtkChild]
        private unowned ListBox errors_box;
    }
}
