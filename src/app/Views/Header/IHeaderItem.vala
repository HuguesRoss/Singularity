using Gtk;

namespace Singularity {
    public interface IHeaderItem : Object {
        public abstract Widget build_widget ();
    }
}
