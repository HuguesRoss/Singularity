using Xml;
using Singularity;

namespace Singularity.Tests {
    public class StringTests : UnitTest {
        public string name { get { return "String"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("test_filesize_conversion", testFilesizeConversion),
            });
        }

        private static void testFilesizeConversion () {
            ulong[] sizes = {
                0,
                1026,
                87007203,
                59167250,
                (ulong)2891451861,
                ulong.MAX,
            };
            string[] results = {
                "0b",
                "1Kb",
                "83Mb",
                "56.4Mb",
                "2.7Gb",
                "16384Pb",
            };

            for (int i = 0; i < sizes.length; ++i) {
                string output = StringUtils.to_legible_filesize (sizes[i]);
                message ("%lu: %s == %s", sizes[i], results[i], output);
                assert (results[i] == output);
            }
        }
    }
}
