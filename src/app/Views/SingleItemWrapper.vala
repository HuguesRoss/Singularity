using Gtk;
//using WebKit;

namespace Singularity {
    /**
    * ItemView that displays items in a grid.
    * Clicking any grid square causes the full item to pop up over the view.
    */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/SingleItemWrapper.ui")]
    public class SingleItemWrapper : Widget {
        public Item item {
            get { return _item; }
            set {
                if (_item != value) {
                    _item = value;
                    //refresh ();
                }
            }
        }
        private Item _item;

        public string title { get {
            if (_item == null) {
                return "";
            }
            return _item.title;
        } }

        public IconCssBuilder css_icons {
            get { return _css_icons; }
            set {
                if (_css_icons != value) {
                    _css_icons = value;
                    notify_property ("css-icons");
                }
            }
        }
        private IconCssBuilder _css_icons;

        public MenuModel main_menu { get; construct; }

        public bool use_csd { get; set; }
        public Gee.Iterable<IHeaderItem> end_items { get; set; }
        public Gee.Iterable<IHeaderItem> start_items { get; construct; }

        public signal void toggle_star ();
        public signal void toggle_read ();

        construct {
            var menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/MainMenu.ui");
            main_menu = menu_builder.get_object ("main_menu") as MenuModel;

            start_items = new Gee.ArrayList<IHeaderItem>.wrap ({
                new ActionHeaderItem ("navigation.pop") {
                    tooltip_markup = "<b>Go Back</b> (Esc)",
                    icon_name = "go-previous-symbolic",
                    minor = true,
                },
            });

            _key_controller = new EventControllerKey () {
                propagation_phase = PropagationPhase.CAPTURE,
            };
            _key_controller.key_pressed.connect (on_key_press);
            add_controller (_key_controller);
        }

        public void refresh_view () {
            item_view.refresh ();
        }

        private bool on_key_press (uint keyval, uint keycode, Gdk.ModifierType state) {
            if (state == Gdk.ModifierType.NO_MODIFIER_MASK) {
                switch (keyval) {
                    case Gdk.Key.m:
                        toggle_read ();
                        refresh_view ();
                        return true;
                    case Gdk.Key.b:
                        toggle_star ();
                        refresh_view ();
                        return true;
                }
            }

            return false;
        }

        [GtkCallback]
        private void on_toggle_read () {
            toggle_read ();
        }

        [GtkCallback]
        private void on_toggle_star () {
            toggle_star ();
        }

        [GtkChild]
        private unowned SingleItemView item_view;

        private EventControllerKey _key_controller;
    }
}
