using Gee;
using Singularity.Db;

namespace Singularity {
    /**
     * DatabaseRequest for marking one or more items as read
     * NOTE: Does not change the value on the object itself--that must be done in the main thread afterwards
     */
    public class ItemViewRequest : IRequest, GLib.Object {
        /** Use a SQLite transaction for the full request? */
        public bool use_transaction { get { return true; } }

        /** The name of the request */
        public string name { get { return _name; } }
        private string _name;

        public ItemViewRequest (Item[] i) {
            _items = i;
        }

        protected Status step (IDbTarget db) {
            var q = new Statement (db, "UPDATE items SET 'unread' = 0 WHERE guid = :guid");

            foreach (Item item in _items) {
                q[":guid"] = item.guid;
                q.execute ();
                q.reset ();
            }

            return Status.COMPLETED;
        }

        private unowned Item[] _items;
    }
}
