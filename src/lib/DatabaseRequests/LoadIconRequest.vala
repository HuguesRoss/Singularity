using Singularity.Db;
using Gee;

namespace Singularity {
    /** DatabaseRequest for loading a feed's icon */
    public class LoadIconRequest : SimpleRequest {
        public Gdk.Texture icon { get; private set; }

        construct {
            _name = "Load icon";
        }

        public LoadIconRequest (Feed feed) {
            _feed = feed;
        }

        public override Statement build_query (IDbTarget db) {
            var q = new Statement (db, "SELECT * from icons WHERE id = :id");
            q[":id"] = _feed.id;

            return q;
        }

        public override IRequest.Status process_result (IDbResult res) {
            int row_data = res.find_column ("data");
            int row_width = res.find_column ("width");
            int row_height = res.find_column ("height");
            int row_bits = res.find_column ("bits");
            int row_rowstride = res.find_column ("rowstride");
            int row_alpha = res.find_column ("alpha");

            if (res.finished) {
                return IRequest.Status.FAILED;
            }

            uint8[] data = res.fetch_blob (row_data);
            if (data != null) {
                int width = res.fetch_int (row_width);
                int height = res.fetch_int (row_height);
                int bits = res.fetch_int (row_bits);
                int stride = res.fetch_int (row_rowstride);
                bool has_alpha = res.fetch_int (row_alpha) == 1;
                if (bits == 0) {
                    try {
                        icon = Gdk.Texture.from_bytes (new Bytes (data));
                    } catch (Error e) {
                        warning ("Failed to load icon for %s: %s", _feed.to_string (), e.message); }
                        return IRequest.Status.FAILED;
                } else {
                    icon = Gdk.Texture.for_pixbuf (new Gdk.Pixbuf.from_data (data, Gdk.Colorspace.RGB, has_alpha, bits, width, height, stride));
                }
            }
            return IRequest.Status.COMPLETED;
        }

        private Feed _feed;
    }
}
