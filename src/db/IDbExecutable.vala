namespace Singularity.Db {
    /** Interface for objects that can be executed for a database result */
    public interface IDbExecutable : Object {
        /** Execute this object's database code and get the result */
        public abstract IDbResult execute ();

        /**
         * Execute an INSERT statement and return the last insert rowid
         *
         * @return the last inserted rowid
         */
        public abstract int64 execute_insert ();
    }
}
