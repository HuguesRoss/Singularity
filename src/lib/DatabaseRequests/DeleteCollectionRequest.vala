/*
    Singularity - A web newsfeed aggregator
    Copyright (C) 2017  Hugues Ross <hugues.ross@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Singularity.Db;

namespace Singularity {
    // DatabaseRequest for deleting collections
    public class DeleteCollectionRequest : SimpleRequest {
        public FeedCollection collection { get; private set; }

        private const int DELETE_COLLECTION = 0;
        private const int RELINK_CHILDREN = 1;
        private const int LAST = RELINK_CHILDREN;

        public DeleteCollectionRequest (FeedCollection c) {
            base (true);
            collection = c;
            _name = "Delete collection";
        }

        public override Statement build_query (IDbTarget db) {
            if (request_index == DELETE_COLLECTION) {
                var q = new Statement (db, "DELETE FROM `feeds` WHERE `id` = :id");
                q[":id"] = collection.id;
                return q;
            } else if (request_index == RELINK_CHILDREN) {
                var q = new Statement (db, "UPDATE feeds SET parent_id = :new_id WHERE parent_id = :old_id");
                q[":new_id"] = collection.parent_id;
                q[":old_id"] = collection.id;
                return q;
            }

            error ("Invalid request stage %d", request_index);
        }

        public override IRequest.Status process_result (IDbResult res) {
            return request_index < LAST ? IRequest.Status.CONTINUE : IRequest.Status.COMPLETED;
        }
    }
}
