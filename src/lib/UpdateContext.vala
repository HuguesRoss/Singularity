using Singularity.Db;

namespace Singularity {
    /** Simple container for holding external data relevant to feed updates */
    public class UpdateContext {
        /** The user's preferred cookie database, if there is one */
        public Soup.CookieJarDB cookies { get { return _cookies; } }

        /** The path to the user's cookie database */
        public string cookie_db_path {
            get { return _cookies_db_path; }
            set {
                if (_cookies_db_path != value) {
                    _cookies = null;
                    _cookies_db_path = value;
                    if (!StringUtils.null_or_empty (_cookies_db_path)) {
                        _cookies = new Soup.CookieJarDB (cookie_db_path, true);
                    }
                    if (_icons != null) {
                        _icons.cookies = _cookies;
                    }
                }
            }
        }

        public TextureCache icons {
            get { return _icons; }
            set {
                if (_icons != value) {
                    _icons = value;
                    if (_icons != null && _cookies != null) {
                        _icons.cookies = _cookies;
                    }
                }
            }
        }

        public WebFeed.XmlModuleFactory xml_modules { get; set; }

        public int cache_valid_minutes { set { _cache_valid_span = value * TimeSpan.MINUTE; } }
        private int64 _cache_valid_span = -1;

        public UpdateContext (Database db) {
            _database = db;
        }

        public async CacheInfo? get_cache_info_for_uri (string uri) {
            // Lock to prevent a race condition where we ask the db twice
            if (_cached_urls_loaded) {
                _lock.lock ();
                CacheInfo? info = _cached_urls[uri];
                _lock.unlock ();
                return info;
            }

            if (_lock.trylock ()) {
                if (!_cached_urls_loading) {
                    _cached_urls_loading = true;
                    _lock.unlock ();

                    var req = new LoadUrlCacheRequest ();
                    yield _database.execute_request_async (req);
                    _lock.lock ();
                    _cached_urls = req.urls;
                    _lock.unlock ();
                    _cached_urls_loaded = true;

                    urls_loaded ();
                } else {
                    _lock.unlock ();
                }
            }

            if (!_cached_urls_loaded) {
                SourceFunc func = get_cache_info_for_uri.callback;
                urls_loaded.connect (() => { Idle.add ((owned)func); });
                yield;
            }

            _lock.lock ();
            CacheInfo? info = _cached_urls[uri];
            _lock.unlock ();

            if (info != null && _cache_valid_span != -1 && new DateTime.now_utc ().difference (info.last_good_result) > _cache_valid_span) {
                return null;
            }
            return info;
        }

        public void set_cache_info_for_uri (string uri, CacheInfo? info) {
            _lock.lock ();
            _cached_urls[uri] = info;
            _lock.unlock ();
        }

        private signal void urls_loaded ();

        private Gee.HashMap<string, CacheInfo> _cached_urls;
        private Database _database;
        private bool _cached_urls_loading = false;
        private bool _cached_urls_loaded = false;
        private Soup.CookieJarDB _cookies;
        private TextureCache _icons;
        private string _cookies_db_path;
        private Mutex _lock; // Locks _cached_urls_loading / _cached_urls_loaded / _cached_urls
    }
}
