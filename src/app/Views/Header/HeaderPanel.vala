using Gee;
using Gtk;

namespace Singularity {
    public class HeaderPanel : Widget {
        public bool hosts_window_controls {
            get { return _hosts_window_controls; }
            set {
                if (_hosts_window_controls != value) {
                    _hosts_window_controls = value;
                    if (_bar is HeaderBar) {
                        ((HeaderBar)_bar).show_title_buttons = _hosts_window_controls;
                    }
                }
            }
        }
        private bool _hosts_window_controls = true;
        public bool use_csd {
            get { return _use_csd; }
            set {
                if (_use_csd != value) {
                    _use_csd = value;
                    build_bar ();
                }
            }
        }
        private bool _use_csd = true;
        // public string title { owned get; set; }
        public Widget title_widget {
            get { return _title_widget; }
            set {
                if (_title_widget != value) {
                    if (_title_widget != null) {
                        _title_widget.unparent ();
                    }

                    _title_widget = value;

                    if (_title_widget != null) {
                        update_title ();
                    }
                }
            }
        }
        private Widget _title_widget;

        /** Gets/sets the primary child widget */
        public Widget child {
            get { return _child; }
            set {
                if (_child != value) {
                    if (_child != null) {
                        _child.unparent ();
                    }

                    _child = value;

                    if (_child != null) {
                        _child.set_parent (this);
                    }
                }
            }
        }
        private Widget _child;

        public Iterable<IHeaderItem> start_items {
            get { return _start_items; }
            set {
                if (_start_items != value) {
                    _start_items = value;

                    if (_bar != null) {
                        build_start_items ();
                    }
                }
            }
        }
        private Iterable<IHeaderItem> _start_items;

        public Iterable<IHeaderItem> end_items {
            get { return _end_items; }
            set {
                if (_end_items != value) {
                    _end_items = value;

                    if (_bar != null) {
                        build_end_items ();
                    }
                }
            }
        }
        private Iterable<IHeaderItem> _end_items;

        static construct {
            set_css_name ("headerpanel");
        }

        construct {
            layout_manager = new BoxLayout (Orientation.VERTICAL);

            var menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/MainMenu.ui");
            MenuModel main_menu = menu_builder.get_object ("main_menu") as MenuModel;

            build_bar ();
        }

        ~HeaderPanel () {
            WidgetUtils.cleanup_children (this);
        }

        private void update_title () {
            HeaderBar header = _bar as HeaderBar;
            ActionBar actions = _bar as ActionBar;

            if (header != null) {
                header.title_widget = title_widget;
            } else {
                actions.set_center_widget (title_widget);
            }
        }

        private void build_bar () {
            if (_child != null) {
                _child.unparent ();
            }
            if (_bar != null) {
                _bar.unparent ();
            }
            if (_title_widget != null) {
                _title_widget.unparent ();
            }

            if (use_csd) {
                _bar = new HeaderBar () {
                    show_title_buttons = _hosts_window_controls,
                };
            } else {
                _bar = new ActionBar ();
            }
            build_start_items ();
            update_title ();
            build_end_items ();

            _bar.set_parent (this);

            if (_child != null) {
                _child.set_parent (this);
            }
        }

        private void build_start_items () {
            HeaderBar header = _bar as HeaderBar;
            ActionBar actions = _bar as ActionBar;

            if (_start_items != null) {
                foreach (IHeaderItem item in _start_items) {
                    if (header != null) {
                        header.pack_start (item.build_widget ());
                    } else {
                        actions.pack_start (item.build_widget ());
                    }
                }
            }
        }

        private void build_end_items () {
            HeaderBar header = _bar as HeaderBar;
            ActionBar actions = _bar as ActionBar;

            if (_end_items != null) {
                foreach (IHeaderItem item in _end_items) {
                    if (header != null) {
                        header.pack_end (item.build_widget ());
                    } else {
                        actions.pack_end (item.build_widget ());
                    }
                }
            }
        }

        private Widget _bar;
    }
}
