using Xml;
using Singularity;
using WebFeed;

namespace Singularity.Tests {
    public class RssTests : UnitTest {
        public string name { get { return "RSS"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("parse_valid_rss", testParseValidRss),
                TestCase ("parse_atom_link", testParseAtomLink),
                TestCase ("parse_wrong_timestamp_format", testParseWrongTimestampFormat),
            });
        }

        private static void testParseValidRss () {
            IDocument document;
            assert (XmlDocument.create (
                    "http://example.com",
"""<?xml version="1.0"?>
<rss version="2.0">
   <channel>
      <title>Hugues Ross</title>
      <link>https://www.huguesross.net/</link>
      <description>The gallery site and blog of Hugues Ross, hobbyist and professional AAA Tools Programmer. Here you can find his art gallery, code projects, and discussions thereof.</description>
      <language>en-us</language>
      <pubDate>Thu, 30 Dec 2021 15:11:00</pubDate>
      <lastBuildDate>Thu, 30 Dec 2021 15:11:00</lastBuildDate>
      <docs>http://blogs.law.harvard.edu/tech/rss</docs>
      <generator>Jekyll</generator>
      <item>
         <title>Website Update: Sidebars for Everybody!</title>
         <link>https://www.huguesross.net/2021/12/sidebars-for-everybody.html</link>
         <description>About a year ago, I relaunched this site with a completely new layout. Today, we have a less ambitious but still important update!</description>
         <pubDate>30 Dec 2021 00:00:00</pubDate>
         <guid>https://www.huguesross.net/2021/12/sidebars-for-everybody.html</guid>
      </item>
   </channel>
</rss>""", out document));

            var parser = new RssFeedParser () {
                data = document,
            };
            assert (parser.parse_data ());

            WebFeed.Feed result = parser.feed;
            assert (result != null);

            assert (result.generator == "Jekyll");
            assert (result.title == "Hugues Ross");
            assert (result.description == "The gallery site and blog of Hugues Ross, hobbyist and professional AAA Tools Programmer. Here you can find his art gallery, code projects, and discussions thereof.");
            assert (result.site_link == "https://www.huguesross.net/");

            WebFeed.Item[] items = parser.items.to_array ();
            assert (items.length == 1);

            WebFeed.Item item = items[0];
            assert (item.title == "Website Update: Sidebars for Everybody!");
            assert (item.uri == "https://www.huguesross.net/2021/12/sidebars-for-everybody.html");
            assert (item.content == "About a year ago, I relaunched this site with a completely new layout. Today, we have a less ambitious but still important update!");
            assert (item.time_published.equal (new DateTime.utc (2021, 12, 30, 0, 0, 0)));
            assert (item.time_updated.equal (new DateTime.utc (2021, 12, 30, 0, 0, 0)));
        }

        private static void testParseAtomLink () {
            IDocument document;
            assert (XmlDocument.create ("http://example.com",
"""<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
    <channel>
        <title>Hugues Ross</title>
        <link>https://www.huguesross.net/</link>
        <atom:link href="https://www.huguesross.net/feed.xml" rel="self" type="application/rss+xml" xmlns="atom" />
    </channel>
</rss>""", out document));

            var factory = new XmlModuleFactory ();
            factory.register (typeof (AtomXmlModule), AtomXmlModule.NAMESPACE);

            var parser = new RssFeedParser () {
                data = document,
                modules = factory,
            };
            assert (parser.parse_data ());

            WebFeed.Feed result = parser.feed;
            assert (result != null);
            assert (result.feed_uri == "https://www.huguesross.net/feed.xml");
        }

        private static void testParseWrongTimestampFormat () {
            IDocument document;
            assert (XmlDocument.create ("http://example.com",
"""<?xml version="1.0"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
   <channel>
      <title>Hugues Ross</title>
      <link>https://www.huguesross.net/</link>
      <item>
         <title>Test Item</title>
         <pubDate>2022-02-06 11:46:16Z</pubDate>
      </item>
   </channel>
</rss>""", out document));

            var parser = new RssFeedParser () {
                data = document,
            };
            assert (parser.parse_data ());

            WebFeed.Item[] items = parser.items.to_array ();
            assert (items.length == 1);
            WebFeed.Item item = items[0];
            assert (item.time_published.equal (new DateTime.utc (2022, 2, 6, 11, 46, 16)));
        }
    }
}
