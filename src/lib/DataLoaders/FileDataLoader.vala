using Xml;

namespace Singularity {
    /** Data loader for loading from a local file */
    public class FileDataLoader : Object, IDataLoader {
        /** The error message for this request, if any */
        public string? error_message { get; protected set; default = null; }

        /** The request URI */
        public DataLoadRequest request { get; set; }

        /** The contextual data for this update */
        public UpdateContext update_context { get; set; }

        /** Returns the base URI that this request is loading from */
        public string get_base_uri () {
            return UriUtils.get_base (request.uri);
        }

        /**
         * Sends the request asynchronously
         *
         * @return true if the request succeeded; otherwise, false
         */
        public async DataLoadResponse send_async () {
            var file = File.new_for_uri (request.uri);
            try {
                uint8[] contents;
                string? etag;
                if (!yield file.load_contents_async (null, out contents, out etag)) {
                    return new DataLoadResponse.error ("Failed to load file %s".printf (file.get_path ()));
                }

                return new DataLoadResponse () {
                    bytes = new Bytes.take (contents),
                };
            } catch (GLib.Error e) {
                return new DataLoadResponse.error ("Failed to load file %s: %s".printf (file.get_path (), e.message));
            }
        }
    }
}
