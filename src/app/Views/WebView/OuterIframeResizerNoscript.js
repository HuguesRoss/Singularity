function resizeIframe(ifrm) {
    ifrm.height = ifrm.contentWindow.document.documentElement.getBoundingClientRect().height + 'px'
}
var has_resized = false
window.addEventListener('resize', function(event) {
    document.querySelectorAll("iframe.content-frame").forEach (resizeIframe)
});

function postLoadResizeIframe(ifrm) {
    resizeIframe(ifrm)
}
