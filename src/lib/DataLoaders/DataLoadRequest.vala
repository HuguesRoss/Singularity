namespace Singularity {
    public class DataLoadRequest {
        public string uri { get; set; }
        public CacheInfo? cache_info { get; set; }
    }
}
