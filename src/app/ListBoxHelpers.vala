using Gtk;

namespace Singularity {
    public class ListBoxHelpers {
        public static void header_separator (ListBoxRow row, ListBoxRow? brow) {
            if (brow != null && row.get_header () == null) {
                row.set_header (new Separator (Orientation.HORIZONTAL));
            }
        }
    }
}
