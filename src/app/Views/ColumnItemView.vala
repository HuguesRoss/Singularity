using Gtk;
using WebKit;

namespace Singularity {
    /**
    * ItemView that displays items in a 2-column format:
    * One column with an item list, and a second that displys the selected item.
    */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/ColumnItemView.ui")]
    public class ColumnItemView : Widget, IItemView {
        public string title {
            get { return _title; }
            protected set {
                if (_title != value) {
                    _title = value;

                    notify_property ("title");
                }
            }
        }
        private string _title;
        public string empty_description {
            get { return _empty_description; }
            protected set {
                if (_empty_description != value) {
                    _empty_description = value;

                    notify_property ("title");
                }
            }
        }
        private string _empty_description;
        public IconCssBuilder css_icons {
            get { return _css_icons; }
            set {
                if (_css_icons != value) {
                    _css_icons = value;
                    notify_property ("css-icons");
                }
            }
        }
        private IconCssBuilder _css_icons;

        public Widget title_widget { get; set; }
        public bool use_csd { get; set; }
        public Gee.Iterable<IHeaderItem> end_items { get; set; }

        construct {
            item_box.bind_model (_items, (i) => {
                var entry = new ItemListEntry (i as Item);
                m_row_list.add (entry);
                return entry;
            });
            item_box.set_header_func (ListBoxHelpers.header_separator);

            column_scroll.edge_reached.connect ((p) => {
                if (p == PositionType.BOTTOM && !_requesting_items) {
                    request_items.begin ();
                }
            });

            install_action ("read", null, (w) => {
                var view = ((ColumnItemView)w);
                ItemListEntry row = view.item_box.get_selected_row ().get_child () as ItemListEntry;
                if (row != null) {
                    view.item_read_toggle (row.item);
                    view.refresh_view ();
                }
            });
            install_action ("bookmark", null, (w) => {
                var view = ((ColumnItemView)w);
                ItemListEntry row = view.item_box.get_selected_row ().get_child () as ItemListEntry;
                if (row != null) {
                    view.item_star_toggle (row.item);
                    view.refresh_view ();
                }
            });
            _key_controller = new EventControllerKey () {
                propagation_phase = PropagationPhase.CAPTURE,
            };
            _key_controller.key_pressed.connect (on_key_press);
            add_controller (_key_controller);
        }

        ~ColumnItemView () {
            WidgetUtils.cleanup_children (this);
        }

        /**
         * Display the result of a search
         *
         * @param results The result object serving the items to display
         */
        public void view_items (SearchResults results) {
            _requesting_items = false;
            _results = results;
            column_scroll.vadjustment.set_value (0);

            _items.remove_all ();
            item_view.item = null;

            m_row_list.clear ();
            request_items.begin ();
        }

        /**
         * Focus this view
         */
        public void focus_view () {
            item_box.grab_focus ();
        }

        /**
         * Refresh the content of this view
         */
        public void refresh_view () {
            ListBoxRow? row = item_box.get_selected_row ();

            item_view.item = null;
            if (row != null) {
                ItemListEntry? entry = row.get_child () as ItemListEntry;
                if (entry != null) {
                    item_view.item = entry.item;
                }
            }
        }

        /**
         * Mark all visible items as read
         */
        public void read_all () {
            items_viewed (WidgetUtils.listmodel_to_array<Item> (_items));
        }

        private bool on_key_press (uint keyval, uint keycode, Gdk.ModifierType state) {
            if (state == Gdk.ModifierType.NO_MODIFIER_MASK) {
                switch (keyval) {
                    case Gdk.Key.m:
                        activate_action ("read", null);
                        return true;
                    case Gdk.Key.b:
                        activate_action ("bookmark", null);
                        return true;
                }
            }

            return false;
        }

        [GtkCallback]
        private void on_toggle_read () {
            ItemListEntry row = item_box.get_selected_row ().get_child () as ItemListEntry;
            item_read_toggle (row.item);
        }

        [GtkCallback]
        private void on_toggle_star () {
            ItemListEntry row = item_box.get_selected_row ().get_child () as ItemListEntry;
            item_star_toggle (row.item);
        }

        // Called when the user selects an item in the left column
        [GtkCallback]
        void on_item_selected (ListBoxRow? row) {
            if (row == null)
                return;

            ItemListEntry entry = row.get_child () as ItemListEntry;
            if (entry != null) {
                Item item = entry.item;
                if (item.unread) {
                    items_viewed ({item});
                }

                item_view.item = item;
            }
        }

        private async void request_items () {
            var results = _results;
            if (_requesting_items) {
                return;
            }

            _requesting_items = true;
            Gee.Iterable<Item> items = yield get_items (results, (int)_items.n_items);

            // If the results no longer match, the content has changed and we should bail out
            if (_results != results) {
                return;
            }

            foreach (Item item in items) {
                _items.append (item);
            }

            _requesting_items = false;
        }

        [GtkChild]
        private unowned ListBox item_box;
        [GtkChild]
        private unowned ScrolledWindow column_scroll;
        [GtkChild]
        private unowned SingleItemView item_view;

        private Gee.List<ItemListEntry> m_row_list = new Gee.ArrayList<ItemListEntry> ();

        GLib.ListStore _items = new GLib.ListStore (typeof (Item));
        private SearchResults _results;
        private EventControllerKey _key_controller;
        private bool _requesting_items;
    }
}
