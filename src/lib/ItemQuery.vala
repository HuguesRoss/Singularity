namespace Singularity {
    public class ItemQuery {
        public FeedDataEntry feed_node { get; set; }
        public bool? unread_only { get; set; }
        public bool? starred_only { get; set; }
        public bool ascending { get; set; }
    }
}
