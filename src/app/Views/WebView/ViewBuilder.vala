namespace Singularity {
    // Interface for all item view HTML builders
    public interface ViewBuilder : GLib.Object {
        public abstract string build_page_html (Gee.List<Item> items);
        public abstract string build_item_html (Item item, int id);

        // Loads a resource file to a string
        protected string resource_to_string (string resource) throws Error {
            File resource_file = File.new_for_uri ("resource://%s/%s".printf (APP_ID_PATH, resource));
            FileInputStream stream = resource_file.read ();
            DataInputStream data_stream = new DataInputStream (stream);

            StringBuilder builder = new StringBuilder ();
            string? str = data_stream.read_line ();
            while (str != null) {
                builder.append (str + "\n");
                str = data_stream.read_line ();
            }

            data_stream.close ();
            stream.close ();

            return builder.str;
        }
    }
}
