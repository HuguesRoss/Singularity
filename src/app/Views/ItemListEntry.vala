using Gdk;
using Gtk;

namespace Singularity {
    // Widget used for the ColumnView's item list
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/ItemListEntry.ui")]
    public class ItemListEntry : Box {
        public Item item { get; construct; }

        static construct {
            install_action ("copy_link", null, (w) => {
                Gdk.Display.get_default ().get_clipboard ().set_text (((ItemListEntry)w).item.link);
            });
            install_action ("read", null, (w) => {
                var? view = WidgetUtils.find_parent<ColumnItemView> (w);
                if (view != null) {
                    view.item_read_toggle (((ItemListEntry)w).item);
                }
            });
            install_action ("bookmark", null, (w) => {
                var? view = WidgetUtils.find_parent<ColumnItemView> (w);
                if (view != null) {
                    view.item_star_toggle (((ItemListEntry)w).item);
                }
            });
            install_action ("navigate", null, (w) => {
                ((ItemListEntry)w).on_open_link ();
            });

            _menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/ItemMenu.ui");
            add_binding_action (Gdk.Key.M, ModifierType.NO_MODIFIER_MASK, "read", null);
            add_binding_action (Gdk.Key.B, ModifierType.NO_MODIFIER_MASK, "bookmark", null);
        }

        construct {
            _context_menu_gesture = new GestureClick () {
                button = Gdk.BUTTON_SECONDARY,
                exclusive = true,
            };
            _context_menu_gesture.pressed.connect (on_context_menu);
            add_controller (_context_menu_gesture);

            _open_link_gesture = new GestureClick () {
                button = Gdk.BUTTON_MIDDLE,
                exclusive = true,
            };
            _open_link_gesture.pressed.connect (on_open_link);
            add_controller (_open_link_gesture);

            _drag_gesture = new DragSource () {
                actions = DragAction.COPY | DragAction.LINK,
            };
            add_controller (_drag_gesture);
        }

        public ItemListEntry (Item i) {
            Object (item: i);

            _item.notify["starred"].connect (on_status_changed);
            _item.notify["unread"].connect (on_status_changed);

            if (i.owner.icon != null) {
                feed_icon.paintable = i.owner.icon;
            } else {
                feed_icon.icon_name = "application-rss+xml-symbolic";
            }

            if (!StringUtils.null_or_empty (i.title)) {
                title_label.label = i.title.replace ("\n", " ");
            } else {
                title_label.label = "Untitled";
            }
            description_label.label = ItemUtils.gtk_description (i);

            var link_value = Value (typeof (string));
            link_value.set_string (_item.link);
            _drag_gesture.content = new ContentProvider.for_value (link_value);

            on_status_changed ();
        }

        ~ItemListEntry () {
            _item.notify["starred"].disconnect (on_status_changed);
            _item.notify["unread"].disconnect (on_status_changed);
        }

        private void on_open_link () {
            try {
                GLib.Process.spawn_command_line_async (
                    AppSettings.link_command.printf (item.link)
                );
            } catch (Error e) {
                warning ("Error opening external link: %s", e.message);
            }
        }

        private void on_context_menu (int _, double x, double y) {
            if (context_menu == null) {
                context_menu = new PopoverMenu.from_model (_menu_builder.get_object ("menu") as MenuModel) {
                    has_arrow = false,
                };
                context_menu.set_parent (this);
            }
            context_menu.set_pointing_to ({ (int)x, (int)y, 0, 0 });
            context_menu.popup ();
        }

        private void on_status_changed () {
            unread_indicator.reveal_child = item.unread || item.starred;
            var list = new Pango.AttrList ();
            if (item.unread || item.starred) {
                unread_icon.set_from_icon_name (item.starred ? "bookmarks-filled-symbolic" : "shape-circle-filled-symbolic");
                if (item.starred) {
                    unread_icon.remove_css_class ("unread-item");
                    unread_icon.add_css_class ("bookmarked-item");
                } else {
                    unread_icon.remove_css_class ("bookmarked-item");
                    unread_icon.add_css_class ("unread-item");
                }
                unread_icon.tooltip_text = item.starred ? "This item is bookmarked" : "This item is unread";

                list.insert (Pango.attr_weight_new (Pango.Weight.BOLD));
            } else {
                list.insert (Pango.attr_weight_new (Pango.Weight.NORMAL));
            }

            title_label.attributes = list;
        }

        [GtkChild]
        private unowned Image feed_icon;
        [GtkChild]
        private unowned Label title_label;
        [GtkChild]
        private unowned Label description_label;
        [GtkChild]
        private unowned Revealer unread_indicator;
        [GtkChild]
        private unowned Image unread_icon;

        private GestureClick _context_menu_gesture;
        private GestureClick _open_link_gesture;
        private DragSource _drag_gesture;

        private PopoverMenu context_menu;

        private static Builder _menu_builder;
    }
}
