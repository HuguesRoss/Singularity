using WebFeed;

namespace Singularity {
    public class LocateFeedsJob : Job {
        public enum ErrorType {
            NO_ERROR = 0,
            VALIDATION,
            CONNECTION,
            PARSER,
            NO_FEEDS,
        }

        public struct FeedResult {
            WebFeed.Feed feed;
            Gee.Collection<WebFeed.Item?> items;
            bool loaded;
        }

        public ErrorType error_type { get; set; }
        public string error_message { owned get; set; }
        public unowned DataLoaderFactory loader_factory { get; set; }
        public unowned DocumentFactory document_factory { get; set; }
        public unowned FeedParserFactory parser_factory { get; set; }
        public unowned FeedLocatorCollection feed_locators { get; set; }
        public string url { owned get; private set; }
        public Gee.ArrayList<FeedResult?> feeds { get; private set; }

        public LocateFeedsJob (string u) {
            url = u;
            feeds = new Gee.ArrayList<FeedResult?> ();
        }

        protected override async Result run_internal () {
            IDataLoader data_loader = loader_factory.get_for_uri (url);
            DataLoadResponse response = yield data_loader.send_async ();
            if (response.has_error) {
                if (data_loader.error_message == "Invalid URL") {
                    error_type = ErrorType.VALIDATION;
                    error_message = "Singularity doesn't know how to handle this address\u2026\nMake sure you typed it correctly!";
                } else {
                    error_type = ErrorType.CONNECTION;
                    error_message = data_loader.error_message ?? "Something went wrong, but we're not sure what";
                }
                return Result.FAILED;
            }

            IDocument doc = document_factory.get_document (data_loader.request.uri, response.data);
            if (doc == null) {
                error_type = ErrorType.PARSER;
                error_message = "Singularity was able to connect, but doesn't recognize the file it received";
                return Result.FAILED;
            }

            var parser = parser_factory.get_parser (doc);

            if (parser != null) {
                if (!parser.parse_data ()) {
                    error_type = ErrorType.PARSER;
                    error_message = "This %s feed appears to be malformed or corrupted".printf (parser.name);
                    return Result.FAILED;
                }

                parser.feed.feed_uri = url; // Important! Some private RSS feeds report a fake 'link', presumably to reduce incoming requests from large-scale aggregators.
                feeds.add ({ parser.feed, parser.items, true });
            }

            Gee.ArrayList<WebFeed.Feed> possible_feeds;
            if (feed_locators.locate_feeds (doc, out possible_feeds)) {
                foreach (WebFeed.Feed possible_feed in possible_feeds) {
                    // TODO
                    feeds.add ({ possible_feed, Gee.Collection<Item?>.empty (), false });
                }

                return Result.SUCCEEDED;
            } else if (_feeds.is_empty) {
                error_type = ErrorType.PARSER;
                error_message = "This address doesn't match any recognized format";
                return Result.FAILED;
            }

            return Result.SUCCEEDED;
        }
    }
}
