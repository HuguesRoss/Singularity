using Gee;
using Singularity.Db;

namespace Singularity {
    /** DatabaseRequest for subscribing to new feeds */
    // TODO: Add items if provided
    public class SubscribeRequest : IRequest, GLib.Object {
        /** The root FeedDataEntry to insert */
        public FeedDataEntry node { get; construct; }

        /** Use a SQLite transaction for the full request? */
        public bool use_transaction { get { return true; } }

        /** The name of the request */
        public string name { get { return "Subscribe"; } }

        public SubscribeRequest (FeedDataEntry n) {
            Object (node: n);

            _nodes = new Gee.ArrayQueue<FeedDataEntry> ();
            prepare_node_list (n);
        }

        public IRequest.Status step (IDbTarget db) {
            FeedDataEntry node = _nodes.poll ();

            if (node.parent != null) {
                // HACK: Re-set the parent to refresh the parent_id...
                node.parent = node.parent;
            }

            if (node is Feed) {
                var feed = node as Feed;
                int64 id = db.execute_insert ("INSERT OR IGNORE INTO `feeds` (`parent_id`, `type`, `title`, `link`, `site_link`, `description`, `rights`, `generator`, `last_update`,`last_new_content`) VALUES (:parent, :type, :title, :link, :site_link, :description, :rights, :generator, :last_update, :last_new_content)",
                    ":parent", typeof (int), feed.parent_id,
                    ":type", typeof (int), (int)FeedDataEntry.Contents.FEED,
                    ":title", typeof (string), feed.title,
                    ":link", typeof (string), feed.link,
                    ":site_link", typeof (string), feed.site_link,
                    ":description", typeof (string), feed.description,
                    ":rights", typeof (string), feed.rights,
                    ":generator", typeof (string), feed.generator,
                    ":last_update", typeof (int), feed.last_update.to_unix (),
                    ":last_new_content", typeof (int), feed.last_new_content.to_unix ()
                    );
                feed.prepare_for_db ((int)id);
            } else {
                var collection = node as FeedCollection;
                int64 id = db.execute_insert ("INSERT OR IGNORE INTO `feeds` (`parent_id`, `type`, `title`) VALUES (:parent, :type, :title)",
                    ":parent", typeof (int), collection.parent_id,
                    ":type", typeof (int), (int)FeedDataEntry.Contents.COLLECTION,
                    ":title", typeof (string), collection.title
                    );
                collection.prepare_for_db ((int)id);
            }

            if (_nodes.is_empty) {
                return IRequest.Status.COMPLETED;
            }
            return IRequest.Status.CONTINUE;
        }

        private void prepare_node_list (FeedDataEntry n) {
            _nodes.add (n);
            if (n is FeedCollection) {
                foreach (FeedDataEntry n2 in ((FeedCollection)n).nodes) {
                    prepare_node_list (n2);
                }
            }
        }

        private ArrayQueue<FeedDataEntry> _nodes;
    }
}
