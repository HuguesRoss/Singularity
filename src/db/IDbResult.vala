namespace Singularity.Db {
    /** Represents data returned from a database statement */
    public interface IDbResult : Object {
        /** Are there no more rows to read? */
        public abstract bool finished { get; }

        /**
         * Fetch a blob from the given column of the current row
         *
         * @param column The column ID
         *
         * @return A uint8[] containing the blob data
         */
        public abstract uint8[] fetch_blob (int column);

        /**
         * Fetch an int from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public abstract int fetch_int (int column);

        /**
         * Fetch an int64 from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public abstract int64 fetch_int64 (int column);

        /**
         * Fetch a double from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's numerical value
         */
        public abstract double fetch_double (int column);

        /**
         * Fetch a string from the given column of the current row
         *
         * @param column The column ID
         *
         * @return The cell's text value
         */
        public abstract string fetch_string (int column);

        public abstract int find_column (string key);

        /**
         * Fetch a blob from the given named column of the current row
         *
         * @param key The column name
         *
         * @return A uint8[] containing the blob data
         */
        public abstract uint8[] get_blob (string key);

        /**
         * Fetch an int from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public abstract int get_int (string key);

        /**
         * Fetch an int64 from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public abstract int64 get_int64 (string key);

        /**
         * Fetch a double from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's numerical value
         */
        public abstract double get_double (string key);

        /**
         * Fetch a string from the given named column of the current row
         *
         * @param key The column name
         *
         * @return The cell's text value
         */
        public abstract string get_string (string key);

        /**
         * Advance the cursor to the next row.
         * Users should check finished afterwards to ensure there is more data to read
         */
        public abstract void next ();
    }
}
