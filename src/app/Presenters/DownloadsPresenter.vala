namespace Singularity {
    public sealed class DownloadsPresenter : Object {
        public ListModel downloads { get { return _downloads; } }
        private ListStore _downloads = new ListStore (typeof (Download));
        public bool has_downloads { get { return downloads.get_n_items () > 0; }}
        public int pending_count { get; private set; }
        public double download_progress { get; private set; }

        public DownloadsPresenter (WebKit.NetworkSession session) {
            _session = session;
            _session.download_started.connect (on_download_started);
        }

        ~DownloadsPresenter () {
            _session.download_started.disconnect (on_download_started);
        }

        private void on_download_started (WebKit.Download download) {
            var dl = new Download (download);
            if (dl.active) {
                dl.notify["progress"].connect (update_progress);
                dl.notify["active"].connect (update_progress);
            }
            _downloads.append (dl);
            update_progress ();
            notify_property ("has-downloads");
        }

        private void update_progress () {
            double accumulator = 0;
            int items = 0;
            for (uint i = 0; i < _downloads.n_items; ++i) {
                Download d = (Download)_downloads.get_item (i);
                if (d.active) {
                    items++;
                    accumulator += d.progress;
                }
            }

            pending_count = items;

            if (items == 0) {
                download_progress = 0;
            } else {
                download_progress = accumulator / items;
            }
        }

        private WebKit.NetworkSession _session;
    }
}
