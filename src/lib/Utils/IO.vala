namespace Singularity {
// Escpaes common characters for HTML display
public static string strip_htm (string str) {
    return str.replace ("<", "&lt;").replace (">", "&gt;");
}

// Hashes a string for use as a unique id
public static string md5_guid (string str)
    ensures (str != "") {
    uchar[] digest = new uchar[64];
    uchar[] buffer = new uchar[32];
    GCrypt.Hash.hash_buffer (GCrypt.Hash.Algorithm.MD5, digest, str.data);

    GCrypt.MPI mpi;

    GCrypt.Error err;
    size_t scanned;
    err = GCrypt.MPI.scan (out mpi, GCrypt.MPI.Format.USG, digest, 64, out scanned);
    if (err.code () != GCrypt.ErrorCode.NO_ERROR) {
        warning ("Failed to scan MPI: %s", err.to_string ());
        return "";
    }
    err = mpi.aprint (GCrypt.MPI.Format.HEX, out buffer);
    if (err.code () != GCrypt.ErrorCode.NO_ERROR) {
        warning ("Failed to print MPI: %s", err.to_string ());
        return "";
    }

    StringBuilder builder = new StringBuilder ();
    int zero_start = -1;
    for (int i = 0; i < buffer.length; ++i) {
        if ((char)buffer[i] == '0' && zero_start == -1)
            zero_start = i;
        else if ((char)buffer[i] != '0' && buffer[i] != 0) {
            zero_start = -1;
        }

        builder.append_c ((char)buffer[i]);
    }

    string g = builder.str;

    if (zero_start != -1)
        return g.substring (0, zero_start);

    return g;
}

// Scrubs markup out of xml, leaving just the text
public static string xml_to_plain (string xml) {
    StringBuilder builder = new StringBuilder ();
    bool in_tag = false;
    for (int i = 0; i < xml.data.length; ++i) {
        uint8 ch = xml.data[i];

        if (ch == '&') {
            if (!in_tag) {
                builder.append_c ((char)ch);
                bool done = false;
                for (int j = i + 1; j < xml.data.length && !done; ++j) {
                    switch (xml.data[j]) {
                        case '&':
                        case '<':
                        case '>':
                            builder.append ("amp;");
                            done = true;
                        break;
                        case ';':
                            // TODO: Potentially replace based on content
                            done = true;
                        break;
                    }
                }
                if (!done)
                    builder.append ("amp;");
            }
        } else if (ch == '<') {
            in_tag = true;
        } else if (ch == '>') {
            in_tag = false;
        } else {
            if (!in_tag)
                builder.append_c ((char)ch);
        }
    }

    return builder.str.strip ().replace ("&nbsp;", " ");
}
}
