using Singularity.Db;

namespace Singularity {
    // Represents a single item within a larger feed
    public class Item : DataEntryGuid {
        public unowned Feed? owner = null;
        public string title = "";
        public string? link = null;
        public string? icon = null;
        public string? description = null;
        public string? content = null;
        public string weak_guid = "";

        /** Gets the list of people that created this item */
        public Gee.Collection<Person> authors = new Gee.ArrayList<Person> ();

        public Gee.Collection<Tag?> tags;
        public Gee.Collection<Attachment?> attachments = new Gee.ArrayList<Attachment?> ();
        public string? rights = null;
        public DateTime time_published = new DateTime.from_unix_utc (0);
        public DateTime time_updated = new DateTime.from_unix_utc (0);
        public DateTime time_loaded = new DateTime.now_utc ();
        public bool unread {
            get { return _unread; }
            set {
                if (_unread != value) {
                    _unread = value;
                    notify_property ("unread");
                }
            }
        }
        private bool _unread = true;
        public bool starred { get; set; default = false; }

        /**
         * Attachment object holding default values for item attachments
         * NOTE: This is never saved or loaded, and only (sometimes) exists at parse time. **Always null-check!**
         */
        public Attachment? attachment_defaults { get; set; }

        public Item () { }

        public Item.from_webfeed (WebFeed.Item item) {
            title = item.title ?? "";
            link = item.uri;
            icon = item.icon_uri;
            description = item.description;
            content = item.content;
            weak_guid = item.guid ?? "";
            authors.add_all_iterator (item.authors.map<Person> (p => new Person (p.name, p.uri, p.email)));
            // TODO: Tags
            attachments.add_all_iterator (item.attachments.map<Attachment> (a => new Attachment.from_webfeed (a)));
            rights = item.rights;
            time_published = item.time_published;
            time_updated = item.time_updated;
            time_loaded = item.time_loaded;

            if (item.attachment_defaults != null) {
                attachment_defaults = new Attachment.from_webfeed (item.attachment_defaults);
            }
        }

        public Item.from_record (IDbResult r) {
            base.from_record (r);
        }

        /**
         * Get a string to use as the item description
         *
         * @return a non-null string representing the description
         */
        public string get_safe_description () {
            if (!StringUtils.null_or_empty (description)) {
                return description;
            } else if (!StringUtils.null_or_empty (content)) {
                return content;
            } else {
                Attachment a = attachments.first_match (a => !StringUtils.null_or_empty (a.description));
                if (a != null) {
                    return a.description;
                }
            }

            return "";
        }

        /** Creates a query for inserting this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? insert (IDbTarget q) {
            Statement query = new Statement (q, """INSERT INTO items (
                        parent_id,
                        guid,
                        weak_guid,
                        title,
                        link,
                        decription,
                        content,
                        rights,
                        publish_time,
                        update_time,
                        load_time,
                        unread,
                        starred
                        ) VALUES (
                            :parent_id,
                            :guid,
                            :title,
                            :link,
                            :description,
                            :content,
                            :rights,
                            :publish_time,
                            :update_time,
                            :load_time,
                            :unread,
                            :starred
                            )""");
            query[":parent_id"] = owner.id;
            query[":guid"] = guid;
            query[":weak_guid"] = weak_guid;
            query[":title"] = title;
            query[":link"] = link;
            query[":description"] = description;
            query[":content"] = content;
            query[":rights"] = rights;
            query[":publish_time"] = time_published.to_unix ();
            query[":update_time"] = time_updated.to_unix ();
            query[":load_time"] = time_loaded.to_unix ();
            query[":unread"] = unread ? 1 : 0;
            query[":starred"] = starred ? 1 : 0;
            // TODO: Decide how to store tags
            return query;
        }

        /** Creates a query for updating this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? update (IDbTarget q) {
            Statement query = new Statement (q, """UPDATE items SET
                    title = :title,
                    link = :link,
                    description = :description,
                    content = :content,
                    rights = :rights,
                    publish_time = :publish_time,
                    update_time = :update_time,
                    load_time = :load_time WHERE guid = :guid
                    """);
            query[":guid"] = guid;
            query[":title"] = title;
            query[":link"] = link;
            query[":description"] = description;
            query[":content"] = content;
            query[":rights"] = rights;
            query[":publish_time"] = time_published.to_unix ();
            query[":update_time"] = time_updated.to_unix ();
            query[":load_time"] = time_loaded.to_unix ();
            // TODO: Decide how to store tags
            // TODO: Decide how to store attachments
            return query;
        }

        /** Creates a query for removing this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? remove (IDbTarget q) {
            Statement query = new Statement (q, "DELETE FROM items WHERE `guid` = :guid");
            query[":guid"] = guid;
            return query;
        }

        /** Get the list of authors for dislay.
         *
         * If this item has no authors but the feed does, the feed's authors
         * will be provided as a fallback.
         *
         * @return A collection of authors
         */
        public Gee.Collection<Person> get_displayed_authors () {
            if (authors.size == 0 && owner != null) {
                return owner.authors;
            }

            return authors;
        }

        public void prepare_for_db () {
            set_guid (md5_guid (owner.link + weak_guid));
        }

        public string to_string () {
            return "%s: %s [%s]".printf (guid, title, link);
        }

        // Checks to see if two items are the same.
        // TODO: Currently this just compares update times. Should we test all fields?
        public bool equals (Item i2) {
            return time_updated == i2.time_updated;
        }

        /** Populate data from a database record
         *
         * @param r The record to read
         */
        protected override void build_from_record (IDbResult r) {
            weak_guid = r.get_string ("weak_guid");
            title = strip_htm (r.fetch_string (3));
            link = r.get_string ("link");
            description = r.get_string ("description");
            content = r.get_string ("content");
            rights = r.get_string ("rights");
            time_published = new DateTime.from_unix_utc (r.get_int ("publish_time"));
            time_updated = new DateTime.from_unix_utc (r.get_int ("update_time"));
            time_loaded = new DateTime.from_unix_utc (r.get_int ("load_time"));
            _unread = r.get_int ("unread") == 1;
            starred = r.get_int ("starred") == 1;
            icon = r.get_string ("icon");
            // TODO: Decide how to retrieve owner
            // TODO: Decide how to store tags
        }
    }
}
