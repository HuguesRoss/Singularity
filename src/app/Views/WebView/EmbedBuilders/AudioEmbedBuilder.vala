namespace Singularity {
    /** Generates an embed for audio attachments */
    public class AudioEmbedBuilder : Object, IEmbedBuilder {
        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         *
         * @return true if the embed could be made, false if this attachment isn't supported
         */
        public bool build_attachment_embed (Attachment attachment, Item item, out string embed) {
            if (StringUtils.null_or_empty (attachment.mimetype) || (!attachment.mimetype.has_prefix ("audio") && attachment.medium != "audio")) {
                embed = null;
                return false;
            }

            embed = """<audio class="singularity-attachment-embed" controls src="%s" preload="metadata" style="display: block;"></audio>""".printf (UriUtils.make_absolute (attachment.url, item.owner.base_uri));
            return true;
        }
    }
}
