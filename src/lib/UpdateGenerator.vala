using WebFeed;

namespace Singularity {
    /** This class handles a single feed update, by requesting the data and running the appropriate parser. */
    public class UpdateGenerator {
        protected Feed to_update;

        public UpdateGenerator (Feed f, IDataLoader loader, DocumentFactory document_factory, FeedParserFactory parser_factory, UpdateContext update_context) {
            to_update = f;
            _loader = loader;
            _loader.update_context = update_context;
            _document_factory = document_factory;
            _parser_factory = parser_factory;
            _update_context = update_context;
        }

        public static bool ensure_item_guid (WebFeed.Item item) {
            // Validate Item and establish GUID
            if (StringUtils.null_or_empty (item.guid)) {
                if (!StringUtils.null_or_empty (item.uri)) {
                    item.guid = item.uri;
                } else if (!StringUtils.null_or_empty (item.title)) {
                    item.guid = item.title;
                } else if (!StringUtils.null_or_empty (item.content)) {
                    item.guid = item.content;
                } else {
                    return false;
                }
            }

            return true;
        }

        /**
         * Runs the actual feed update.
         * This returns a package containing either the resulting update data, or an error message
         */
        public async UpdatePackage do_update () {
            DataLoadResponse response = _loader.send ();
            if (response.has_error) {
                return new UpdatePackage.failure (to_update, response.error_message);
            } else if (response.data == null) {
                // No errors and no data? Nothing to do!
                return new UpdatePackage.ignore (to_update);
            }

            // if (response.cache_info != null) {
            //     message ("Got cache info for %s:\nLast Modif: %s\nEtag: %s\n", to_update.to_string (), response.cache_info.last_modified ?? "none", response.cache_info.etag ?? "none");
            // }

            IDocument doc = _document_factory.get_document (_loader.request.uri, response.data);

            IFeedParser parser = null;
            if (doc == null) {
                return new UpdatePackage.failure (to_update, "Couldn't determine document content type");
            }

            parser = _parser_factory.get_parser (doc);

            if (parser == null) {
                return new UpdatePackage.failure (to_update, "Couldn't determine feed type");
            }

            if (!parser.parse_data ())
                return new UpdatePackage.failure (to_update, "Failed to parse %s feed data".printf (parser.name));

            // Attempt to load the feed icon
            if (parser.feed.icon_uri != null) {
                parser.feed.icon_uri = UriUtils.make_absolute (parser.feed.icon_uri, _loader.get_base_uri ());
            }

            to_update.update_contents (parser.feed);

            // Attempt to load the feed icon
            CacheInfo? icon_cache_info = null;
            if (to_update.icon_uri != null && _update_context.icons != null) {
                var icon_loader = new HttpDataLoader () {
                    update_context = _update_context,
                    request = new DataLoadRequest () {
                        uri = parser.feed.icon_uri,
                        cache_info = yield _update_context.get_cache_info_for_uri (parser.feed.icon_uri),
                    },
                };
                var icon_response = yield icon_loader.send_async ();
                if (icon_response.cache_info != null) {
                    icon_cache_info = icon_response.cache_info;
                }

                if (icon_response.has_error) {
                    // TODO: Attach to UpdatePackage as a warning
                    warning (icon_response.error_message);
                } else if (icon_response.data != null) {
                    try {
                        to_update.icon = Gdk.Texture.from_bytes (icon_response.bytes);
                    } catch (Error e) {
                        // TODO: Attach to UpdatePackage as a warning
                        warning (e.message);
                    }
                }
            }

            var items = new Gee.ArrayList<Singularity.Item> ();
            foreach (WebFeed.Item item in parser.items) {
                if (!ensure_item_guid (item)) {
                    warning ("Could not establish GUID for item in %s (%s), as it has no guid, title, link, or content", parser.feed.title, parser.feed.feed_uri);
                    continue;
                }

                items.add (new Singularity.Item.from_webfeed (item));
            }

            return new UpdatePackage.success (to_update, items) {
                feed_cache_info = response.cache_info,
                icon_cache_info = icon_cache_info,
            };
        }

        private IDataLoader _loader;
        private FeedParserFactory _parser_factory;
        private DocumentFactory _document_factory;
        private UpdateContext _update_context;
    }
}
