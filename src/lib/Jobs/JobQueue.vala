namespace Singularity {
    public class JobQueue : Object {
        public int n_jobs { get { return _pending_jobs + _active_threads; } }
        public int n_pending { get { return _pending_jobs; } }
        public int n_active { get { return _active_threads; } }
        public signal void post_job (Job job);

        public JobQueue (int thread_count = 1, ulong frequency) {
            _jobs = new AsyncQueue<Job> ();
            _frequency = frequency;
            _active_jobs = new Job[thread_count];
            _worker_threads = new Thread<void*>[thread_count];
            for (int i = 0; i < thread_count; ++i) {
                _worker_threads[i] = new Thread<void*> (null, this.process);
            }
        }

        public bool cancel_all () {
            bool cancelled_any = false;

            _jobs.@lock ();
            Job j = _jobs.try_pop_unlocked ();
            cancelled_any = j != null;
            while (j != null) {
                j.cancel ();
                post_job (j);
                j = _jobs.try_pop_unlocked ();
            }

            lock (_lock) {
                _pending_jobs = 0;
                for (int i = 0; i < _active_jobs.length; ++i) {
                    if (_active_jobs[i] != null) {
                        cancelled_any = true;
                        _active_jobs[i].cancel ();
                        _active_threads--;
                        post_job (_active_jobs[i]);
                        _active_jobs[i] = null;
                    }
                }
            }

            _jobs.@unlock ();

            return cancelled_any;
        }

        public void request (Job job) {
            assert (job.state == Job.State.FRESH);

            job.state = Job.State.WAITING;
            _jobs.push (job);
            lock (_lock) {
                _pending_jobs++;
            }
        }

        public async Job.Result request_and_wait (owned Job job) {
            request (job);
            return yield job.wait ();
        }

        private void* process () {
            int i = -1;
            lock (_lock) {
                i = _thread_count;
                _thread_count++;
            }

            var loop = new MainLoop ();

            while (true) {
                Job job = _jobs.pop ();
                lock (_lock) {
                    _pending_jobs--;
                    _active_jobs[i] = job;
                    _active_threads++;
                }

                job.run.begin (() => {
                    loop.quit ();
                });
                loop.run ();

                lock (_lock) {
                    _active_jobs[i] = null;
                    _active_threads--;
                    post_job (job);
                }

                if (_frequency > 0) {
                    Thread.usleep (_frequency);
                }
            }
        }

        private Job[] _active_jobs;
        private AsyncQueue<Job> _jobs;
        private int _pending_jobs = 0;
        private int _active_threads = 0;
        private int _thread_count = 0;
        private Object _lock = new Object (); // _pending_jobs, _active_jobs, _active_threads, _thread_count
        private Thread<void*>[] _worker_threads;
        private ulong _frequency;
    }
}
