namespace Singularity.Db {
    /** IRequest with basic code for multi-stage transactions */
    public abstract class SimpleRequest : IRequest, GLib.Object {

        /** Enable logging of SQL and other DB actions */
        public const bool LOG_DB = false;

        /** Whether or not this request should be treated as a single DB transation */
        public override bool use_transaction { get { return _transaction; } }
        private bool _transaction = false;

        /** The name of the request */
        public override string name { get { return _name ?? get_type ().name (); } }
        protected string _name;

        /** The index of the current step of the request */
        protected int request_index { get; private set; default = 0; }

        protected SimpleRequest (bool transaction = false) {
            _transaction = transaction;
        }

        /**
         * Perform one statement of the query
         *
         * @param target The target of the query
         *
         * @return Status object indicating the current status of this query
         */
        public virtual Status step (IDbTarget target) {
            Statement statement = build_query (target);
            if (LOG_DB) {
                message ("%s step %d: %s", name, request_index, statement.sql);
            }
            IDbResult res = statement.execute ();
            if (LOG_DB) {
                message ("%s processing %d", name, request_index);
            }
            Status status = process_result (res);
            request_index++;
            return status;
        }

        /**
         * Build the next Statement to execute on the target
         *
         * @param target The target to query
         *
         * @return A Statement to execute
         */
        protected abstract Statement build_query (IDbTarget target);

        /**
         * Process the result of the last Statement, and indicate if
         * execution should continue.
         *
         * @param res The result of the last Statement
         *
         * @return The status of the request
         */
        protected virtual Status process_result (IDbResult res) {
            return Status.DEFAULT;
        }
    }
}
