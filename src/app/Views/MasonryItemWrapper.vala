using Gtk;

namespace Singularity {
    public class MasonryItemWrapper : Widget {
        public Object item { get; construct; }
        public Widget child { get; construct; }
        public bool selectable { get; set; default = true; }
        public bool activatable { get; set; default = true; }

        public signal void activate_signal ();

        construct {
            set_css_name ("child");
            layout_manager = new BinLayout ();
            _click_gesture = new GestureClick () {
                button = Gdk.BUTTON_PRIMARY,
                exclusive = true,
            };
            _click_gesture.pressed.connect (on_click);
            _click_gesture.released.connect (on_release);
            add_controller (_click_gesture);

            set_activate_signal_from_name ("activate_signal");
            activate_signal.connect (on_release);
        }

        public MasonryItemWrapper (uint position, Object item, Widget child) {
            Object (item: item, child: child);

            //focusable = true;
            _position = position;
            child.set_parent (this);
        }

        ~MasonryItemWrapper () {
            WidgetUtils.cleanup_children (this);
        }

        private void on_click () {
            if (selectable) {
                var view = parent as MasonryView;
                if (view != null) {
                    view.model.select_item (_position, true);
                }
            }
        }

        private void on_release () {
            if (activatable) {
                var view = parent as MasonryView;
                if (view != null) {
                    view.activate (_position);
                }
            }
        }

        private GestureClick _click_gesture;
        private uint _position;
    }
}
