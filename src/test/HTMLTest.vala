using Xml;
using Singularity;

namespace Singularity.Tests {
    public class HTMLTests : UnitTest {
        public string name { get { return "HTML"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("extract_image", testExtractImage),
            });
        }

        private static void testExtractImage () {
            string[] data = {
                """<div class='src'><a href='asdasdasd'>src&eq;"sdfsdfsdf"<img id="test" src=""></a><br><img/><img id='test2' src='https://example.com/test.png'></div>""",
                """<img src="get-image.php?src=example"></img>""",
            };
            string[] images = {
                "https://example.com/test.png",
                "get-image.php?src=example",
            };
            for (int i = 0; i < data.length; ++i) {
                string? image = StringUtils.extract_image (data[i]);
                message ("BEFORE '%s' == '%s'", image, images[i]);
                message ("AFTER  '%s' == '%s'", image, images[i]);
                assert (image == images[i]);
            }
        }
    }
}
