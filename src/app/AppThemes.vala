namespace Singularity {
    public enum AppThemes {
        SYSTEM = 0,
        LIGHT,
        DARK;

        /**
         * Parse a string into an AppTheme
         *
         * @param str The tring to parse
         */
        public static AppThemes parse (string str) {
            switch (str.strip ()) {
                case "system":
                    return AppThemes.SYSTEM;
                case "light":
                    return AppThemes.LIGHT;
                case "dark":
                    return AppThemes.DARK;
                default:
                    return AppThemes.SYSTEM;
            }
        }

        /**
         * Convert this AppTheme into a computer-parseable string
         *
         * @return The parseable string representation of this AppTheme
         */
        public string serialize () {
            switch (this) {
                case AppThemes.SYSTEM:
                    return "system";
                case AppThemes.LIGHT:
                    return "light";
                case AppThemes.DARK:
                    return "dark";
            }

            return "system";
        }

        public void apply (Adw.StyleManager style_manager) {
            switch (this) {
                case AppThemes.SYSTEM:
                    style_manager.color_scheme = Adw.ColorScheme.PREFER_LIGHT;
                    break;
                case AppThemes.LIGHT:
                    style_manager.color_scheme = Adw.ColorScheme.FORCE_LIGHT;
                    break;
                case AppThemes.DARK:
                    style_manager.color_scheme = Adw.ColorScheme.FORCE_DARK;
                    break;
            }
        }
    }

    public enum AppScrollbars {
        SYSTEM = 0,
        SUBTLE,
        THICK;

        /**
         * Parse a string into an AppScrollbar
         *
         * @param str The string to parse
         */
        public static AppScrollbars parse (string str) {
            switch (str.strip ()) {
                case "system":
                    return AppScrollbars.SYSTEM;
                case "subtle":
                    return AppScrollbars.SUBTLE;
                case "thick":
                    return AppScrollbars.THICK;
                default:
                    return AppScrollbars.SYSTEM;
            }
        }

        /**
         * Convert this AppScrollbar into a computer-parseable string
         *
         * @return The parseable string representation of this AppScrollbar
         */
        public string serialize () {
            switch (this) {
                case AppScrollbars.SYSTEM:
                    return "system";
                case AppScrollbars.SUBTLE:
                    return "subtle";
                case AppScrollbars.THICK:
                    return "thick";
            }

            return "system";
        }

        public void apply (Gtk.Settings settings) {
            switch (this) {
                case AppScrollbars.SYSTEM:
                    settings.reset_property ("gtk-overlay-scrolling");
                    break;
                case AppScrollbars.SUBTLE:
                    settings.set ("gtk-overlay-scrolling", true);
                    break;
                case AppScrollbars.THICK:
                    settings.set ("gtk-overlay-scrolling", false);
                    break;
            }
        }
    }
}
