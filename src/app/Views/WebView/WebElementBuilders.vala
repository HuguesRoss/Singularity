namespace Singularity {
    public class WebElementBuilders {
        /** Gets the embed builder factory used for item attachments */
        public static EmbedBuilderFactory embed_factory {
            get {
                if (_factory == null) {
                    _factory = new EmbedBuilderFactory ();
                }
                return _factory;
            }
        }
        private static EmbedBuilderFactory _factory;

        /**
         * Build the HTML to display a single attachment 
         *
         * @param attachment The attachment to display
         *
         * @return A string contianing the attachment HTML
         */
        public static string build_attachment (Attachment attachment, Item item) {
            var builder = new StringBuilder ("<div class='attachment'>");
            string name = attachment.name;
            string icon = "text-x-generic-symbolic";

            if (StringUtils.null_or_empty (name) || name.down () == item.title.down ()) {
                name = "";
            }

            if (!StringUtils.null_or_empty (attachment.mimetype)) {
                if (name == "") {
                    name = "Attached " + MimeTypeInfo.get_display_name (attachment.mimetype);
                }
                icon = MimeTypeInfo.get_icon (attachment.mimetype);
            } else if (!StringUtils.null_or_empty (attachment.medium)) {
                if (name == "") {
                    name = "Attached " + get_medium_display_name (attachment.medium);
                }
                icon = get_medium_icon (attachment.medium);
            } else if (name == "") {
                name = "Attached File";
            }

            builder.append_printf ("<div class='title'><div class='icon' style='background-image: var(--%s)'></div><span>%s</span></div>", icon, name);

            if (attachment.authors.size > 0) {
                if (StringUtils.null_or_empty (attachment.name)) {
                    builder.append ("Posted");
                }
                int i = 0;
                foreach (Person author in attachment.authors) {
                    string? author_name = author.name ?? author.email;
                    if (author_name != null) {
                        if (i == 0) {
                            builder.append (" by ");
                        } else {
                            builder.append (", ");
                        }

                        if (author.uri != null) {
                            builder.append_printf ("<a href='%s'>%s</a>", UriUtils.make_absolute (author.uri, item.owner.base_uri), author_name);
                        } else {
                            builder.append (author_name);
                        }
                    }
                }
            }

            string embed;
            bool has_embed = false;
            bool can_download = true;
            if (embed_factory.build_attachment_embed (attachment, item, out embed, out can_download)) {
                has_embed = true;
                builder.append (embed);
            }

            if (attachment.views > 0 || attachment.ratings > 0 || attachment.avg_rating > 0 || attachment.favorites > 0) {
                builder.append ("<p>");
                if (attachment.views > 0) {
                    builder.append_printf ("%lld Views", attachment.views);
                }

                if (attachment.ratings > 0 || attachment.avg_rating > 0) {
                    if (attachment.views > 0) {
                        builder.append (" • ");
                    }

                    // Round the value
                    int full_stars = (int)((attachment.avg_rating / attachment.max_rating * 10) + 0.5);
                    // Clamp from 0 - 10
                    full_stars = full_stars > 10 ? 10 : full_stars < 0 ? 0 : full_stars;

                    builder.append ("Rated ");

                    // Add the stars
                    int i = 0;
                    for (; i < full_stars - 1; i += 2) {
                        builder.append ("\u2605");
                    }
                    if (i < full_stars) {
                        builder.append ("\u2BEA");
                        i += 2;
                    }
                    for (; i < 10; i += 2) {
                        builder.append ("\u2606");
                    }

                    if (attachment.ratings > 0) {
                        builder.append_printf (" • %lld rating%s", attachment.ratings, attachment.ratings > 1 ? "s" : "");
                    }
                } else if (attachment.favorites > 0) {
                    if (attachment.views > 0) {
                        builder.append (" • ");
                    }

                    builder.append_printf ("\u2605 %lld", attachment.favorites);
                }
                builder.append ("</p>");
            }

            if (!StringUtils.null_or_empty (attachment.description)) {
                builder.append_printf ("<p>%s</p>", attachment.description);
            }

            if (!has_embed || can_download) {
                builder.append_printf ("<div class='button-box download-buttons'><a href='%s'><div class='download-button'>Download</div></a>", UriUtils.make_absolute (attachment.url, item.owner.base_uri));
                if (attachment.size > 0) {
                    string size = StringUtils.to_legible_filesize ((ulong)attachment.size);
                    if (!StringUtils.null_or_empty (size)) {
                        builder.append_printf ("<span class='filesize'>%s</span>", size);
                    }
                }
                builder.append ("</div>");
            }

            builder.append ("</div>");
            return builder.str;
        }

        public static string escape_srcdoc (string? content) {
            if (content == null) {
                return "";
            }

            return content.replace ("&", "&amp;")
                          .replace ("\"", "&quot;");
        }

        /**
         * Build the header HTML for an item
         *
         * @param item The item to render for
         * @param has_buttons Whether or not to include read/star buttons
         *
         * @return The header HTML
         */
        public static string build_post_header (Item item, bool has_buttons = true) {
            var head_builder = new StringBuilder ("<header><section class='title'>");
            head_builder.append_printf ("<a href='%s'>%s</a>",
                UriUtils.make_absolute (item.link, item.owner.base_uri),
                item.title == "" ? "Untitled Post" : item.title);
            if (has_buttons) {
                head_builder.append ("""<div class='button-box'>
                                            <div class='read-button'><div class='icon'></div></div>
                                            <div class='star'><div class='icon'></div></div>
                                        </div>""");
            }
            head_builder.append ("</section>");

            string? posted = WebElementBuilders.build_posted (item);
            if (posted != null) {
                head_builder.append (posted);
            }
            head_builder.append ("</header>");

            return head_builder.str;
        }

        public static string build_post_content (Item item, int id, string content_header, bool is_high_contrast) {
            StringBuilder content_builder = new StringBuilder ();
            content_builder.append_printf ("<html %s><head>", is_high_contrast ? "class=\"highcontrast\"" : "");
            content_builder.append (content_header);
            content_builder.append ("</head>");
            content_builder.append_printf ("<body style=\"overflow-y: hidden;overflow-x: scroll;\">%s</body></html>", item.content);

            return content_builder.str;
        }

        public static string? get_post_icon (Item item) {
            string? icon = item.icon;
            if (icon == null && !StringUtils.null_or_empty (item.description)) {
                icon = StringUtils.extract_image (item.description);
            }
            if (icon == null && !StringUtils.null_or_empty (item.content)) {
                icon = StringUtils.extract_image (item.content);
            }
            if (icon == null) {
                foreach (Attachment a in item.attachments) {
                    if (!StringUtils.null_or_empty (a.mimetype) && a.mimetype.has_prefix ("image/") || a.medium == "image") {
                        icon = a.url;
                        break;
                    } else if (!StringUtils.null_or_empty (a.icon)) {
                        icon = a.icon;
                        break;
                    }
                }
            }

            return icon;
        }

        public static string? build_post_icon (Item item) {
            string? icon = get_post_icon (item);

            if (icon != null) {
                return "<img src=\"%s\"></img>".printf (UriUtils.make_absolute (icon, item.owner.base_uri));
            }

            return null;
        }

        /**
         * Build the "Posted by" section of an item's header
         *
         * @param item The item to render for
         *
         * @return A string containing the section HTML, or null if not applicable
         */
        public static string? build_posted (Item item) {
            string? name = null;
            foreach (Person author in item.get_displayed_authors ()) {
                string? author_name = author.name ?? author.email;
                if (author_name != null) {
                    if (name == null) {
                        name = "by ";
                    } else {
                        name = name + ", ";
                    }

                    if (author.uri != null) {
                        name = name + "<a href='%s'>%s</a>".printf (UriUtils.make_absolute (author.uri, item.owner.base_uri), author_name);
                    } else {
                        name = name + author_name;
                    }
                }
            }

            string? date = null;
            if (item.time_published.compare (new DateTime.from_unix_utc (0)) != 0) {
                date = item.time_published.format ("%A, %B %e %Y");
                date = "on <time class='date' datetime='%s'>%s</time> ".printf (date, date);
            }

            if (name == null && date == null) {
                return null;
            } else if (name != null && date != null) {
                return "<span class='posted'>Posted %s %s</span>".printf (name, date);
            } else {
                return "<span class='posted'>Posted %s</span>".printf (name ?? date);
            }
        }

        public static string get_theme () {
            string resource_path;
            switch (AppSettings.theme_id) {
                case AppThemes.LIGHT:
                    resource_path = "Views/WebView/colors-light.css";
                    break;
                case AppThemes.DARK:
                    resource_path = "Views/WebView/colors-dark.css";
                    break;
                default:
                    var style_manager = Adw.StyleManager.get_default ();
                    resource_path = style_manager.dark ? "Views/WebView/colors-dark.css" : "Views/WebView/colors-light.css";
                    break;
            }

            return resource_path;
        }

        public static string get_medium_display_name (string medium) {
            switch (medium) {
                case "image":
                    return "Image";
                case "audio":
                    return "Audio";
                case "video":
                    return "Video";
                default:
                    return "File";
            }
        }

        public static string get_medium_icon (string medium) {
            switch (medium) {
                case "image":
                    return "image-x-generic-symbolic";
                case "audio":
                    return "audio-x-generic-symbolic";
                case "video":
                    return "video-x-generic-symbolic";
                default:
                    return "text-x-generic-symbolic";
            }
        }
    }
}
