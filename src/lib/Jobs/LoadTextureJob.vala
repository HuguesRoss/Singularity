using Gdk;

namespace Singularity {
    public interface ILoadTextureJob : Job {
        public abstract string url { owned get; set; }
        public abstract Texture texture { get; protected set; }
    }

    public class DownloadTextureJob : Job, ILoadTextureJob {
        public string url { owned get; set; }
        public Soup.Session session { get; set; }
        public Texture texture { get; protected set; }

        protected override async Result run_internal () {
            try {
                string scheme = UriUtils.get_scheme (url);
                if (scheme == "file") {
                    texture = (Texture.from_file (File.new_for_uri (url)));
                } else if (scheme.has_prefix ("http")) { /** Since we call has_prefix, this also matches https */
                    var message = new Soup.Message ("GET", url);
                    texture = Texture.for_pixbuf (
                        yield new Pixbuf.from_stream_async (
                            yield session.send_async (message, Soup.MessagePriority.NORMAL, null)
                        )
                    );
                }
            } catch (Error e) {
                warning ("Failed to download texture from %s: %s", url, e.message);
                return Result.FAILED;
            }

            return Result.SUCCEEDED;
        }
    }

    public class LoadLocalTextureJob : Job, ILoadTextureJob {
        public string url { owned get; set; }
        public Texture texture { get; protected set; }

        public LoadLocalTextureJob.with_result (Texture tex) {
            texture = tex;
            state = State.COMPLETED;
            result = Result.SUCCEEDED;
        }

        protected override async Result run_internal () {
            try {
                texture = (Texture.from_file (File.new_for_uri (url)));
            } catch (Error e) {
                warning ("Failed to load texture file from %s: %s", url, e.message);
                return Result.FAILED;
            }

            return Result.SUCCEEDED;
        }
    }
}
