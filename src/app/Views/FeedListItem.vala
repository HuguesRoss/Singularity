using Gdk;
using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/FeedListItem.ui")]
    public class FeedListItem : Widget {
        public new string icon_name {
            get { return _icon_name; }
            set {
                if (_icon_name != value) {
                    _icon_name = value;
                    if (_paintable == null) {
                        icon.icon_name = _icon_name;
                    }
                }
            }
        }
        private string _icon_name;
        public new Paintable paintable {
            get { return _paintable; }
            set {
                if (_paintable != value) {
                    _paintable = value;

                    if (_paintable == null) {
                        icon.icon_name = _icon_name;
                    } else {
                        icon.icon_name = null;
                    }
                    icon.paintable = _paintable;
                }
            }
        }
        private Paintable _paintable;

        public TreeListRow list_row {
            get { return _list_row; }
            set {
                if (_list_row != value) {
                    if (node != null) {
                        node.notify["has-error"].disconnect (refresh_error);
                        node.notify["icon"].disconnect (refresh_icon);
                    }
                    _list_row = value;
                    notify_property ("list-row");
                    notify_property ("node");
                    if (node != null) {
                        node.notify["has-error"].connect (refresh_error);
                        node.notify["icon"].connect (refresh_icon);
                        paintable = node.icon;
                        icon_name = node.icon_name;
                    }
                    refresh_error ();
                }
            }
        }
        private TreeListRow _list_row;

        public IFeedListNode? node { owned get { return list_row == null ? null : list_row.item as IFeedListNode; } }

        public bool can_edit_title { get { return node.can_rename; } }

        public bool is_editing_title {
            get { return _is_editing_title; }
            set {
                if (_is_editing_title != value) {
                    _is_editing_title = value && can_edit_title;

                    title_stack.visible_child = _is_editing_title ? (Widget)title_entry : (Widget)title_label;
                    if (_is_editing_title) {
                        title_entry.text = node.title;
                        // FIXME: Really nasty hack. For some reason, the focus leaves and re-enters *twice* when grabbing focus.
                        _leave_count = 2;
                        title_entry.grab_focus ();
                    }

                    notify_property ("is-editing-title");
                }
            }
        }
        private bool _is_editing_title;
        private int _leave_count = 0;

        construct {
            _focus_controller = new EventControllerFocus ();
            _focus_controller.leave.connect (() => {
                if (is_editing_title) {
                    _leave_count--;
                    if (_leave_count <= 0) {
                        is_editing_title = false;
                    }
                }
            });
            title_entry.add_controller (_focus_controller);
        }

        ~FeedListItem () {
            WidgetUtils.cleanup_children (this);
        }

        private void refresh_error () {
            if (node != null && node.has_error) {
                title_label.add_css_class ("error");
            } else {
                title_label.remove_css_class ("error");
            }
        }

        private void refresh_icon () {
            if (node != null) {
                paintable = node.icon;
                icon_name = node.icon_name;
            } else {
                paintable = null;
                icon_name = "application-rss+xml-symbolic";
            }
        }

        [GtkCallback]
        private void save_title () {
            if (title_entry.text != "") {
                node.rename (title_entry.text);
            }
            is_editing_title = false;
        }

        [GtkChild]
        private unowned Image icon;

        [GtkChild]
        private unowned Label title_label;

        [GtkChild]
        private unowned Text title_entry;

        [GtkChild]
        private unowned Stack title_stack;

        private EventControllerFocus _focus_controller;
    }
}
