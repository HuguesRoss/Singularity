using WebKit;

namespace Singularity {
    public class WebPolicyHandler {
        public static bool policy_decision (PolicyDecision decision, PolicyDecisionType type) {
            if (type == PolicyDecisionType.NAVIGATION_ACTION || type == PolicyDecisionType.NEW_WINDOW_ACTION) {
                NavigationPolicyDecision nav_dec = (NavigationPolicyDecision) decision;
                if (nav_dec.get_navigation_action ().get_navigation_type () == NavigationType.LINK_CLICKED) {
                    try {
                        GLib.Process.spawn_command_line_async (
                            AppSettings.link_command.printf (nav_dec.get_navigation_action ().get_request ().uri));
                        nav_dec.ignore ();
                    } catch (Error e) {
                        warning ("Error opening external link: %s", e.message);
                    }
                }
                return true;
            }
            return false;
        }
    }
}
