namespace Singularity {
    public class DataLoadResponse {
        public Bytes? bytes { get; set; }
        public string? data { owned get {
            if (bytes == null) {
                return null;
            }

            return (string)bytes.get_data ();
        } }
        public CacheInfo? cache_info { get; set; }

        /** The error message for the request, if any errors occurred */
        public string? error_message { get; private set; }

        /** Whether or not the request ended with an error */
        public bool has_error { get { return error_message != null; } }

        public DataLoadResponse.error (string message) {
            error_message = message;
        }
    }
}
