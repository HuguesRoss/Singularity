using Gtk;
using WebKit;

namespace Singularity {
    public sealed class SingleItemView : Widget {
        public Item item {
            get { return _item; }
            set {
                if (_item != value) {
                    if (_item != null) {
                        _item.notify["starred"].disconnect (on_starred_changed);
                        _item.notify["unread"].disconnect (on_unread_changed);
                    }

                    _item = value;

                    refresh ();

                    if (_item != null) {
                        _item.notify["starred"].connect (on_starred_changed);
                        _item.notify["unread"].connect (on_unread_changed);
                    }
                }
            }
        }
        private Item _item;
        public IconCssBuilder css_icons {
            get { return _css_icons; }
            set {
                if (_css_icons != value) {
                    _css_icons = value;
                    _builder = new ColumnViewBuilder (_css_icons);

                    if (item != null) {
                        refresh ();
                    }
                    notify_property ("css-icons");
                }
            }
        }
        private IconCssBuilder _css_icons;

        public signal void toggle_read ();
        public signal void toggle_star ();

        construct {
            layout_manager = new BinLayout ();

            var content_manager = new UserContentManager ();
            try {
                _link_script = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/ColumnViewLink.js".printf (APP_ID_PATH));
                _resizer_script = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/OuterIframeResizer.js".printf (APP_ID_PATH));
                _resizer_script_nojs = UserScriptBuilder.load_from_resource ("resource://%s/Views/WebView/OuterIframeResizerNoscript.js".printf (APP_ID_PATH));
            } catch (Error e) {
                error ("Can't read JS resources: %s", e.message);
            }

            content_manager.script_message_received.connect (message_received);
            content_manager.register_script_message_handler ("test", null);

            _web_view = (WebView) Object.new (typeof (WebView),
                hexpand: true,
                vexpand: true,
                user_content_manager: content_manager,
                settings: new WebKit.Settings () {
                    // TODO: For now. We may want to disable this for release builds
                    enable_developer_extras = true,
                    enable_page_cache = true,
                    enable_smooth_scrolling = true,
                    enable_write_console_messages_to_stdout = false,
                }
            );
            _web_view.decide_policy.connect (WebPolicyHandler.policy_decision);
            _web_view.set_parent (this);
        }

        ~SingleItemView () {
            WidgetUtils.cleanup_children (this);
        }

        public void refresh () {
            if (_builder == null) {
                return;
            }

            _web_view.user_content_manager.remove_all_scripts ();
            _web_view.user_content_manager.add_script (_link_script);
            _web_view.user_content_manager.add_script (AppSettings.enable_javascript ? _resizer_script : _resizer_script_nojs);


            var list = new Gee.ArrayList<Item> ();
            if (item != null) {
                list.add (item);
            }
            _web_view.load_html (_builder.build_page_html (list), null);
        }

        private void message_received (JSC.Value @value) {
            if (!@value.is_string ())
                return;

            string command = @value.to_string ();
            if (command.has_prefix ("s:")) {
                int id;
                if (command.scanf ("s:%d", out id) == 1) {
                    toggle_star ();
                }
            } else if (command.has_prefix ("r:")) {
                int id;
                if (command.scanf ("r:%d", out id) == 1) {
                    toggle_read ();
                }
            } else if (command.has_prefix ("d:")) {
                if (command.length > 2) {
                    _web_view.download_uri (command.substring (2));
                }
            }
        }

        private void on_unread_changed () {
            _web_view.evaluate_javascript.begin (SET_JS_TEMPLATE.printf ("read", item.unread ? "false" : "true"), -1, null, null, null);
        }

        private void on_starred_changed () {
            _web_view.evaluate_javascript.begin (SET_JS_TEMPLATE.printf ("starred", item.starred ? "true" : "false"), -1, null, null, null);
        }

        private ColumnViewBuilder _builder;
        private WebKit.WebView _web_view;
        private UserScript _link_script;
        private UserScript _resizer_script;
        private UserScript _resizer_script_nojs;

        private const string SET_JS_TEMPLATE = "document.getElementsByClassName('singularity-article')[0].dataset.%s = %s;";
    }
}
