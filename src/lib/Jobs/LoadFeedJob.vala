using WebFeed;

namespace Singularity {
    public class LoadFeedJob : Job {
        public string error_message { owned get; set; }
        public unowned DataLoaderFactory loader_factory { get; set; }
        public unowned DocumentFactory document_factory { get; set; }
        public unowned FeedParserFactory parser_factory { get; set; }
        public string url { owned get; private set; }
        public Feed feed { get; set; }
        public Gee.ArrayList<Item> items { get { return _items; }}
        private Gee.ArrayList<Item> _items = new Gee.ArrayList<Item> ();

        public LoadFeedJob (string u) {
            url = u;
        }

        protected override async Result run_internal () {
            IDataLoader data_loader = loader_factory.get_for_uri (url);
            DataLoadResponse response = yield data_loader.send_async ();
            if (response.has_error) {
                error_message = response.error_message;
                return Result.FAILED;
            }

            IDocument doc = document_factory.get_document (data_loader.request.uri, response.data);
            if (doc == null) {
                error_message = "Couldn't determine document content type";
                return Result.FAILED;
            }

            var parser = parser_factory.get_parser (doc);

            if (parser == null) {
                error_message = "Couldn't determine feed type";
                return Result.FAILED;
            }

            if (!parser.parse_data ()) {
                error_message = "This %s feed appears to be malformed or corrupted".printf (parser.name);
                return Result.FAILED;
            }

            if (parser.feed.icon_uri != null) {
                parser.feed.icon_uri = UriUtils.make_absolute (parser.feed.icon_uri, data_loader.get_base_uri ());
            }

            feed.update_contents (parser.feed);

            foreach (WebFeed.Item item in parser.items) {
                if (UpdateGenerator.ensure_item_guid (item)) {
                    _items.add (new Item.from_webfeed (item));
                } else {
                    warning (
                        "Could not establish GUID for item in %s (%s), as it has no guid, title, link, or content",
                        parser.feed.title, parser.feed.feed_uri
                    );
                }
            }

            return Result.SUCCEEDED;
        }
    }
}
