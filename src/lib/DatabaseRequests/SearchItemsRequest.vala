using Singularity.Db;
using Gee;

namespace Singularity {
    public class SearchItemsRequest : SimpleRequest {
        public ItemQuery query { get; construct set; }
        public Gee.ArrayList<string> result { get; construct set; }
        public uint limit { get; set; default = 1000; }
        public uint offset { get; set; default = 0; }

        construct {
            result = new Gee.ArrayList<string> ();
        }

        public SearchItemsRequest (ItemQuery q, ItemCache c) {
            Object (query: q);
        }

        public override Statement build_query (IDbTarget db) {
            // -1 is a special case--that's the root node, so no need to filter
            bool has_node = query.feed_node != null && query.feed_node.id != -1;

            // First run? Contrust the filter for reuse
            var filter_builder = new StringBuilder ();

            bool has_filters = false;
            if (has_node) {
                filter_builder.append_printf (" %s parent_id IN (SELECT id FROM feed_node)", has_filters ? "AND" : "WHERE");
                has_filters = true;
            }

            // TODO: Custom filtering
            if (query.unread_only == true) {
                filter_builder.append_printf (" %s (unread = 1)", has_filters ? "AND" : "WHERE");
                has_filters = true;
            }

            if (query.starred_only == true) {
                filter_builder.append_printf (" %s (starred = 1)", has_filters ? "AND" : "WHERE");
                has_filters = true;
            }

            // TODO: Custom ordering
            filter_builder.append_printf (" ORDER BY (unread | starred) DESC, publish_time %s", query.ascending ? "ASC" : "DESC");
            if (limit > 0) {

                filter_builder.append_printf (" LIMIT %lu", limit);
                if (offset > 0) {
                    filter_builder.append_printf (" OFFSET %lu", offset);
                }
            }
            var builder = new StringBuilder ();
            if (has_node) {
                // Recursively build a list of :parent_id's child feed ids
                builder.append_printf ("WITH RECURSIVE feed_node(id) AS (VALUES (:id) UNION ALL SELECT feeds.id FROM feeds,feed_node WHERE feeds.parent_id=feed_node.id) ");
            }

            builder.append_printf ("SELECT guid FROM items %s", filter_builder.str);

            var q = new Statement (db, builder.str);

            if (has_node) {
                q[":id"] = query.feed_node.id;
            }
            return q;
        }

        public override IRequest.Status process_result (IDbResult res) {
            for (; !res.finished; res.next ()) {
                result.add (res.fetch_string (0));
            }
            if (_to_create.size == 0) {
                return IRequest.Status.COMPLETED;
            }
            return IRequest.Status.COMPLETED;
        }

        private ArrayList<string> _to_create = new ArrayList<string> ();
    }
}
