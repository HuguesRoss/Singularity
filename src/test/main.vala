using Singularity;

public static void main (string[] args) {
    Test.init (ref args);

    Tests.TestSuite data_suite = new Tests.TestSuite ("data");
    data_suite.add (new Tests.PersonTest ());

    Tests.TestSuite serializer_suite = new Tests.TestSuite ("serializer");
    serializer_suite.add (new Tests.AtomTests ());
    serializer_suite.add (new Tests.HTMLTests ());
    serializer_suite.add (new Tests.RssTests ());
    serializer_suite.add (new Tests.OpmlTests ());
    serializer_suite.add (new Tests.StringTests ());
    serializer_suite.add (new Tests.UriTests ());

    Test.run ();
}
