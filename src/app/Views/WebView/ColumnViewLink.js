var prep_done = false;
var items;
setTimeout(prepare, 2000);

function toggleStar(button) {
    var parent = button.parentNode.parentNode.parentNode.parentNode;
    parent.dataset.starred = (parent.dataset.starred == 'false' ? 'true' : 'false');
    webkit.messageHandlers.test.postMessage("s:" + parent.dataset.id);
}

function toggleRead(button) {
    var parent = button.parentNode.parentNode.parentNode.parentNode;
    if(parent.dataset.read_set == 'true' || parent.dataset.read == 'true') {
        parent.dataset.read = (parent.dataset.read == 'false' ? 'true' : 'false');
        webkit.messageHandlers.test.postMessage("r:" + parent.dataset.id);
    }
    parent.dataset.read_set = 'true';
}

function downloadLink(event) {
    event.preventDefault();
    webkit.messageHandlers.test.postMessage("d:" + this.href);
}

function prepareItems(starting_id) {
    items = document.getElementsByClassName('singularity-article');

    for(var j = starting_id; j < items.length; ++j){
        var i = items[j];
        var readbutton = i.children[0].children[0].children[1].children[0];
        var starbutton = i.children[0].children[0].children[1].children[1];
        i.dataset.read_set = 'false';
        readbutton.onclick = function() {toggleRead(this);}
        starbutton.onclick = function() {toggleStar(this);}

        var downloads = i.querySelectorAll('.download-button')
        for (let k = 0; k < downloads.length; ++k) {
            downloads[k].parentNode.addEventListener('click', downloadLink);
        }
    }
}

function prepare() {
    if(prep_done)
        return;
    prep_done = true;

    prepareItems(0);
}
prepareItems(0);
