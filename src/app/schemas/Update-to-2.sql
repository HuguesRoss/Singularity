CREATE TABLE "feed_authors" (
    feed_id     INTEGER NOT NULL,
    author_id   TEXT NOT NULL,
    PRIMARY KEY (feed_id, author_id),
    FOREIGN KEY (feed_id) REFERENCES feeds (id)
    FOREIGN KEY (author_id) REFERENCES people (guid)
);

CREATE TABLE "item_authors" (
    item_guid   TEXT NOT NULL,
    author_id   TEXT NOT NULL,
    PRIMARY KEY (item_guid, author_id),
    FOREIGN KEY (item_guid) REFERENCES items (guid)
    FOREIGN KEY (author_id) REFERENCES people (guid)
);

INSERT INTO item_authors (item_guid, author_id)
SELECT item_guid, guid FROM people;

ALTER TABLE people DROP COLUMN feed_id;
ALTER TABLE people DROP COLUMN item_guid;
