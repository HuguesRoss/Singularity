using Singularity.Db;

namespace Singularity {
    /** A DatabaseRequest for unsubscribing from a feed */
    public class UnsubscribeRequest : SimpleRequest {
        public Feed feed { get; private set; }
        public UnsubscribeRequest (Feed f) {
            base (true);
            feed = f;
            _name = "Unsubscribe from %s".printf (f.title);
        }

        private string[] queries = {
            // Delete all item_authors where the item GUID resolves to one of the items under the feed
            "DELETE FROM item_authors WHERE item_guid in (SELECT item_guid FROM items INNER JOIN item_authors ON items.guid = item_authors.item_guid INNER JOIN people ON people.guid = item_authors.author_id WHERE parent_id = :id)",
            "DELETE FROM items WHERE parent_id = :id",

            "DELETE FROM feed_authors WHERE feed_id = :id",
            "DELETE FROM icons WHERE id = :id",
            "DELETE FROM feeds WHERE id = :id",
        };

        public override Statement build_query (IDbTarget db) {
            var q = new Statement (db, queries[request_index]);
            q[":id"] = feed.id;
            return q;
        }

        public override IRequest.Status process_result (IDbResult res) {
            return request_index < queries.length - 1 ? IRequest.Status.CONTINUE : IRequest.Status.COMPLETED;
        }
    }
}
