using Gtk;
using Gee;

namespace Singularity {
    public class WidgetUtils {
        public static void cleanup_children (Widget w) {
            foreach (var child in get_children (w)) {
                child.unparent ();
            }
        }
        public static Collection<Widget> get_children (Widget w) {
            var children = new ArrayList<Widget> ();
            for (Widget child = w.get_first_child (); child != null; child = child.get_next_sibling ()) {
                children.add (child);
            }
            return children;
        }
        public static T? find_parent<T> (Widget w) {
            for (Widget child = w; child != null; child = child.parent) {
                if (child is T) {
                    return child;
                }
            }
            return null;
        }
        public static TreeListRow? treelistrow_at (ListView list, double x, double y) {
            Widget? child = list.pick (x, y, PickFlags.DEFAULT);
            if (child != null) {
                // There is a child of internal class GtkListItemWidget wrapping the content, and it is picked if we click the margings. So, if that happens we take the child.
                if (child.parent == list) {
                    child = child.get_first_child ();
                }

                var expander = WidgetUtils.find_parent<TreeExpander> (child);
                if (expander != null) {
                    return expander.list_row;
                }
            }

            return null;
        }
        public static bool pop_temp_navigation_pages (Adw.NavigationView view) {
            // Check if the top can pop. If it doesn't, we popped nothing.
            bool popped = view.pop ();
            if (!popped) {
                return false;
            }

            // ...If it does, continue popping and return true at the end
            while (view.pop ()); // Repeats until the stack cannot pop
            return true;
        }
        public static T[] listmodel_to_array<T> (GLib.ListModel model) {
            assert (model.get_item_type () == typeof (T));

            uint n_items = model.get_n_items ();
            T[] array = new T[n_items];
            for (uint i = 0; i < n_items; ++i) {
                array[i] = model.get_item (i);
            }

            return array;
        }
    }
}
