namespace Singularity {
    public sealed class UpdateQueueWrapper : Object {
        public bool is_updating {
            get { return _is_updating; }
            private set {
                if (_is_updating != value) {
                    _is_updating = value;
                    notify_property ("is_updating");
                }
            }
        }
        private bool _is_updating;
        public float update_progress {
            get { return _update_progress; }
            private set {
                if (_update_progress != value) {
                    _update_progress = value;
                    notify_property ("update_progress");
                }
            }
        }
    private float _update_progress = 1;

        public signal void update_failed (UpdatePackage package);
        public signal void update_ready (UpdatePackage package);

        public UpdateQueueWrapper (UpdateQueue queue) {
            _update_queue = queue;
            _update_queue.update_processed.connect (on_update_processed);
        }

        ~UpdateQueueWrapper () {
            _update_queue.update_processed.disconnect (on_update_processed);
            _update_queue = null;
        }

        public void request_update (Feed feed) {
            _update_queue.request_update (feed);
            queue_progress_update ();
        }

        public void cancel_update (Feed feed) {
            _update_queue.cancel_update (feed);
            queue_progress_update ();
        }

        // TODO: Use a params-array once they stabilize
        public void request_updates (Gee.Iterable<Feed> feeds) {
            foreach (Feed feed in feeds) {
                _update_queue.request_update (feed);
            }
            queue_progress_update ();
        }

        private void on_update_processed (UpdatePackage package) {
            if (package.is_error) {
                warning ("Can't update feed %s: %s", package.feed.to_string (), package.message);
                update_failed (package);
            } else if (package.usable) {
                update_ready (package);
            }

            queue_progress_update ();
        }

        private void queue_progress_update () {
            lock (_progress_queued) {
                if (_progress_queued) {
                    return;
                }

                _progress_queued = true;
            }
            Idle.add_once (() => {
                lock (_progress_queued) {
                    _progress_queued = false;

                    int remaining = _update_queue.length;

                    if (!is_updating) {
                        _update_count = remaining;
                    }

                    if (_update_count == 0) {
                        update_progress = 1;
                        is_updating = false;
                    } else {
                        update_progress = (float)(_update_count - remaining) / (float)_update_count;
                        is_updating = remaining > 0;
                    }
                }
            });
        }

        private bool _progress_queued;
        private int _update_count;
        private UpdateQueue _update_queue;
    }
}
