using WebFeed;

namespace Singularity {
    /**
     * XML module parser for Youtube RSS feeds
     */
    public class YoutubeXmlModule : Object, IXmlModule {
        /** The XML namespace */
        public const string NAMESPACE = "http://www.youtube.com/xml/schemas/2015";

        /**
         * Read an XML node describing a feed
         *
         * @param feed The associated feed
         * @param node The XML node
         * @param document_uri The base URI of the XML document
         */
        public void read_feed_node (WebFeed.Feed feed, Xml.Node* node, string document_uri) {
        }

        /**
         * Read an XML node describing an item
         *
         * @param item The associated item
         * @param node The XML node
         * @param document_uri The base URI of the XML document
         */
        public void read_item_node (WebFeed.Item item, Xml.Node* node, string document_uri) {
            switch (node->name) {
                case "videoId":
                    item.attachment_defaults = item.attachment_defaults ?? new WebFeed.Attachment ();
                    item.attachment_defaults.embed_uri = Websites.Youtube.embed_uri (node->get_content ());
                    break;
                // case channelId:
            }
        }
    }
}
