namespace Singularity {
    public class ItemUtils {
        public static string gtk_description (Item item) {
            string desc = item.get_safe_description ();
            if (StringUtils.null_or_empty (desc)) {
                return "";
            }

            desc = xml_to_plain (desc);
            int line_index = desc.index_of ("\n");
            if (line_index == -1) {
                return desc;
            }

            return desc.substring (0, line_index);
        }
    }
}
