/*
    Singularity - A web newsfeed aggregator
    Copyright (C) 2022  Hugues Ross <hugues.ross@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using Gee;

/**
 * Helper class for getting the URI to an icon file
 */
public class IconUriBuilder : GLib.Object {
    /**
     * Get the uri representing the given icon file
     *
     * @param icon_file The file to get a URI to
     *
     * @return A CSS-compatible URI
     */
    public static async string get_icon_uri_async (File icon_file) {
        string path = icon_file.get_uri ();
        _encoded_icons = _encoded_icons ?? new HashMap<string, string> ();
        if (_encoded_icons.has_key (path)) {
            return _encoded_icons[path];
        }

        try {
            FileInfo info = yield icon_file.query_info_async (
                FileAttribute.STANDARD_CONTENT_TYPE,
                FileQueryInfoFlags.NONE);
            string? etag;
            Bytes data = yield icon_file.load_bytes_async (null, out etag);

            string url = "url(\"data:%s;base64,%s\")".printf (
                info.get_attribute_string (FileAttribute.STANDARD_CONTENT_TYPE),
                Base64.encode (Bytes.unref_to_data (data)));

            _encoded_icons[path] = url;
            return url;

        } catch (Error e) {
            error ("Failed to read icon at %s: %s", path, e.message);
        }
    }

    /**
     * get the URI representing an icon in memory
     *
     * @param icon The icon data to encode
     *
     * @return A CSS-compatible URI
     */
    public static string get_icon_pixbuf_uri (Gdk.Pixbuf icon) {
        try {
            uint8[] data;
            icon.save_to_buffer (out data, "png");
            string url = "url(\"data:image/png;base64,%s\")".printf (
                Base64.encode (data));

            return url;
        } catch (Error e) {
            error ("Failed to convert icon from memory: %s", e.message);
        }
    }

    private static HashMap<string, string> _encoded_icons;
}
