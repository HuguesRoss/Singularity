namespace Singularity {
    /** Generates an embed for image attachments and generic attachments with icons */
    public class ImageEmbedBuilder : Object, IEmbedBuilder {
        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         *
         * @return true if the embed could be made, false if this attachment isn't supported
         */
        public bool build_attachment_embed (Attachment attachment, Item item, out string embed) {
            string? image_url = null;
            if ((!StringUtils.null_or_empty (attachment.mimetype) && attachment.mimetype.has_prefix ("image")) || attachment.medium == "image") {
                image_url = attachment.url;
            } else if (!StringUtils.null_or_empty (attachment.icon)) {
                image_url = attachment.icon;
            }

            if (image_url == null) {
                embed = null;
                return false;
            }

            embed = """<img class="singularity-attachment-embed" src="%s" style="display: block"/>""".printf (UriUtils.make_absolute (image_url, item.owner.base_uri));
            return true;
        }
    }
}
