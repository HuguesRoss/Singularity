using Adw;
using Gdk;
using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/FeedPropertiesWindow.ui")]
    public class FeedPropertiesWindow : Adw.Dialog {
        public Feed feed { get; construct set; }
        public string feed_title { get; private set; }
        public string link { get; private set; }
        public Paintable icon { get; private set; }
        public string description { get; private set; }
        public bool has_website { get; private set; }
        public string copyright { get; private set; }
        public bool has_copyright { get; private set; }
        public string authors { get; private set; }
        public bool has_authors { get; private set; }
        public string tags { get; private set; }
        public bool has_tags { get; private set; }
        public string last_updated { get; private set; default = "Never"; }

        construct {
            var group = new SimpleActionGroup ();

            var copy_action = new SimpleAction ("copy-url", null);
            copy_action.activate.connect (() => {
                get_clipboard ().set_text (link);
            });
            group.add_action (copy_action);

            var visit_website_action = new SimpleAction ("visit-website", null);
            visit_website_action.activate.connect (visit_website);
            group.add_action (visit_website_action);

            insert_action_group ("properties", group);

            // HACK: Selectable GTK Labels automatically select everything when focused,
            //       which is completely nonsensical but nonetheless the current behavior.
            //       Thus, we force something else to take focus first
            focus_widget = website_row;
        }

        public FeedPropertiesWindow (Feed feed) {
            Object (feed: feed);
            feed_title = feed.title;
            title = "Properties - %s".printf (feed_title);
            link = feed.link;
            icon = feed.icon;
            description = feed.description;
            has_website = !StringUtils.null_or_empty (feed.site_link);
            copyright = feed.rights;
            has_copyright = !StringUtils.null_or_empty (feed.rights);
            authors = feed.authors.size == 0
                    ? null
                    : feed.authors.fold<string> ((a, s) => s != "" ? (s + ", " + a.to_string ()) : (a.to_string ()), "");
            has_authors = feed.authors.size > 0;
            has_tags = false;
            if (feed.last_new_content.to_unix () != 0) {
                last_updated = DateTimeUtils.to_date_string (feed.last_new_content.to_local ());
            }
        }

        private void visit_website () {
            try {
                GLib.Process.spawn_command_line_async (AppSettings.link_command.printf (feed.site_link));
            } catch (Error e) {
                warning ("Error opening external link: %s", e.message);
            }
        }

        [GtkChild]
        private unowned Widget website_row;
    }
}
