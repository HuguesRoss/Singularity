namespace Singularity.Db {
    /** Error domain for internal database processing errors */
    internal errordomain DbError {
        SCRIPT_LOAD_FAILED,
        SCRIPT_EXECUTE_FAILED,
    }
}
