using Gee;

namespace Singularity {
    /** Factory class that builds IDataLoaders from a request URI */
    public class DataLoaderFactory {
        public DataLoaderFactory (Type @default) {
            _default_loader = @default;
        }

        /**
         * Create a data loader for a given a URI
         *
         * @param uri The request URI
         *
         * @return The new data loader, or null if there are no loaders that can load the given URI
         * 
         */
        public IDataLoader? get_for_uri (string uri) {
            return get_for_request (new DataLoadRequest () {
                uri = uri,
            });
        }

        public IDataLoader? get_for_request (DataLoadRequest request) {
            string scheme = UriUtils.get_scheme (request.uri);
            Type loader_type = _default_loader;
            if (_loaders.has_key (scheme)) {
                loader_type = _loaders[scheme];
            }
            return (IDataLoader)Object.new (loader_type, "request", request);
        }

        /**
         * Register a data loader type to be used for a particular URI scheme
         *
         * @param scheme The URI scheme to use (ie. http, ftp, file, etc.)
         * @param loader The loader type
         */
        public void register_for_scheme (string scheme, Type loader) {
            _loaders[scheme] = loader;
        }

        private HashMap<string, Type> _loaders = new HashMap<string, Type> ();
        private Type? _default_loader;
    }
}
