namespace Singularity {
    public class DateTimeUtils {
        public static string to_date_string (DateTime date, bool include_time = true) {
            if (include_time) {
                return "%d/%d/%04d %02d:%02d".printf (
                    date.get_month (),
                    date.get_day_of_month (),
                    date.get_year (),
                    date.get_hour (),
                    date.get_minute ());
            }

            return "%d/%d/%04d".printf (
                date.get_month (),
                date.get_day_of_month (),
                date.get_year ());
        }
    }
}
