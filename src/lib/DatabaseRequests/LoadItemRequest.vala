using Singularity.Db;
using Gee;

namespace Singularity {
    public class LoadItemRequest : SimpleRequest {
        public string guid { owned get; construct set; }
        public ItemCache item_cache { get; construct set; }
        public Item item { get; private set; }

        public LoadItemRequest (string g, ItemCache c) {
            Object (guid: g, item_cache: c);
        }

        public override Statement build_query (IDbTarget db) {
            string str = "";

            switch (request_index) {
                case Stage.LOAD_CONTENT:
                    str = "SELECT * FROM items WHERE items.guid == :guid";
                    break;
                case Stage.LOAD_ATTACHMENTS:
                    str = "SELECT * FROM enclosures WHERE enclosures.item_guid == :guid";
                    break;
                case Stage.LOAD_ITEM_PEOPLE:
                    str = "SELECT item_authors.item_guid,people.* FROM item_authors INNER JOIN people ON people.guid = item_authors.author_id WHERE item_authors.item_guid == :guid";
                    break;
                case Stage.LOAD_ATTACHMENT_PEOPLE:
                    str = "SELECT enclosure_guid,people.* FROM enclosures INNER JOIN enclosure_authors ON enclosures.guid = enclosure_authors.enclosure_guid INNER JOIN people ON people.guid = enclosure_authors.author_id WHERE enclosures.item_guid == :guid";
                    break;
            }

            var q = new Statement (db, str);

            q[":guid"] = guid;
            return q;
        }

        public override IRequest.Status process_result (IDbResult res) {
            switch (request_index) {
                case Stage.LOAD_CONTENT:
                    string guid = res.fetch_string (0);
                    int parent_id = res.get_int ("parent_id");
                    item = new Item.from_record (res) {
                        owner = item_cache.get_feed (parent_id),
                    };
                    item_cache.insert (item);
                    break;
                case Stage.LOAD_ATTACHMENTS:
                    for (; !res.finished; res.next ()) {
                        var a = new Attachment.from_record (res);
                        item.attachments.add (a);
                        _new_attachments[a.guid] = a;
                    }
                    break;
                case Stage.LOAD_ITEM_PEOPLE:
                    for (; !res.finished; res.next ()) {
                        item.authors.add (new Person.from_record (res));
                    }
                    break;
                case Stage.LOAD_ATTACHMENT_PEOPLE:
                    for (; !res.finished; res.next ()) {
                        var attachment = _new_attachments[res.get_string ("enclosure_guid")];
                        if (attachment != null) {
                            attachment.authors.add (new Person.from_record (res));
                        }
                    }
                    break;
            }
            return request_index < Stage.LAST ? IRequest.Status.CONTINUE :
                IRequest.Status.COMPLETED;
        }

        private HashMap<string, Attachment> _new_attachments = new HashMap<string, Attachment> ();

        enum Stage {
            LOAD_CONTENT,
            LOAD_ATTACHMENTS,
            LOAD_ITEM_PEOPLE,
            LOAD_ATTACHMENT_PEOPLE,
            LAST = LOAD_ATTACHMENT_PEOPLE
        }
    }
}
