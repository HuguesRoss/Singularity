using Gtk;

namespace Singularity {
    /**
     * Widget that displays feed update errors
     */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/Popovers/AttachmentsPopover.ui")]
    public class AttachmentsPopover : Popover {
        public string current_page { get {
            if (presenter == null) {
                return "empty";
            }

            return presenter.has_downloads ? "list" : "empty";
        } }
        public DownloadsPresenter presenter {
            get { return _presenter; }
            set {
                if (_presenter != value) {
                    if (_presenter != null) {
                        _presenter.notify["has-downloads"].disconnect (on_has_downloads_changed);
                    }

                    _presenter = value;

                    if (_presenter != null) {
                        _presenter.notify["has-downloads"].connect (on_has_downloads_changed);
                    }

                    notify_property ("current-page");
                }
            }
        }
        private DownloadsPresenter _presenter;

        ~AttachmentsPopover () {
            WidgetUtils.cleanup_children (this);
            presenter = null;
        }

        private void on_has_downloads_changed () {
            notify_property ("current-page");
        }
    }
}
