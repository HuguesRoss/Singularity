function resizeIframe(ifrm) {
    var height = ifrm.contentWindow.postMessage("FrameHeight", "*");   
}
var has_resized = false
window.addEventListener('resize', function(event) {
    document.querySelectorAll("iframe.content-frame").forEach (resizeIframe)
});
window.addEventListener('message', function(event) {
    if (event.data.hasOwnProperty("FrameHeight")) {
        document.querySelector(`[data-id="${event.data.id}"] iframe`).style.height = `${event.data.FrameHeight}px`;
    }
});
function postLoadResizeIframe(ifrm) {
    resizeIframe(ifrm)
}
