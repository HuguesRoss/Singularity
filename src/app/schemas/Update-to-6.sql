CREATE TABLE "cached_urls" (
    url TEXT PRIMARY KEY,
    last_good_result DATE,
    last_modified TEXT,
    etag TEXT
);
