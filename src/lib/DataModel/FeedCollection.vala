using Singularity.Db;

namespace Singularity {
    // Represents a collection of feeds. FeedCollections act a lot like folders,
    // and can hold both feeds and other collections.
    public class FeedCollection : FeedDataEntry {
        public Icon? icon { get; protected set; }
        public override string icon_name { get { return _custom_icon ?? "folder-symbolic"; } }
        public string custom_icon {
            get { return _custom_icon; }
            set {
                if (_custom_icon != value) {
                    _custom_icon = value;
                    notify_property ("custom_icon");
                    notify_property ("icon_name");
                }
            }
        }
        private string _custom_icon;
        public Gee.List<FeedDataEntry> nodes { get; protected set; default = new Gee.ArrayList<FeedDataEntry> (); }

        public enum DBColumn {
            ID = 0,
            TITLE,
            COUNT
        }

        public signal void nodes_changed ();

        public FeedCollection (string new_title) {
            title = new_title;
        }

        public FeedCollection.from_record (IDbResult r) {
            base.from_record (r);
        }

        public FeedCollection.root () { }

        // Adds a node as a child, and updates its parent
        public void add_node (FeedDataEntry c) {
            nodes.add (c);
            c.parent = this;
            nodes_changed ();
        }

        // Removes a child node, and updates its parent
        public void remove_node (FeedDataEntry c)
            requires (nodes.contains (c)) {
            nodes.remove (c);
            c.parent = null;
            nodes_changed ();
        }

        /** Creates a query for inserting this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? insert (IDbTarget q) {
            Statement query = new Statement (q, """INSERT INTO feeds (id, parent_id, type, title)
                VALUES (:id, :parent_id, :type, :title)""");
            query[":id"] = id;
            query[":parent_id"] = parent_id;
            query[":type"] = FeedDataEntry.Contents.COLLECTION;
            query[":title"] = title;
            // TODO: Decide how to store icons
            return query;
        }

        /** Creates a query for updating this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? update (IDbTarget q) {
            Statement query = new Statement (q, """UPDATE collections SET
                    title = :title,
                    parent_id = :parent_id
                WHERE id = :id""");
            query[":id"] = id;
            query[":parent_id"] = parent_id;
            query[":title"] = title;
            // TODO: Decide how to store icons
            return query;
        }

        /** Creates a query for removing this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? remove (IDbTarget q) {
            Statement query = new Statement (q, "DELETE FROM collections WHERE `id` = :id");
            query[":id"] = id;
            return query;
        }

        /** Populate data from a database record
         *
         * @param r The record to read
         */
        protected override void build_from_record (IDbResult r) {
            set_id (r.fetch_int (0));

            parent_id = r.get_int ("parent_id");
            title = r.get_string ("title");
            // TODO: Decide how to store icons
        }

        public void prepare_for_db (int new_id) {
            set_id (new_id);
        }

        // Returns all feeds contained in this collection
        public override Gee.List<Feed> get_feeds () {
            Gee.List<Feed> feeds = new Gee.ArrayList<Feed> ();
            foreach (FeedDataEntry node in nodes) {
                feeds.add_all (node.get_feeds ());
            }

            return feeds;
        }
    }
}
