namespace Singularity.MimeTypeInfo {
    public static string get_display_name (string mime_type) {
        if (mime_type.has_prefix ("audio/")) {
            return "Audio";
        } else if (mime_type.has_prefix ("image/")) {
            return "Image";
        } else if (mime_type.has_prefix ("video/")) {
            return "Video";
        } else if (mime_type.has_prefix ("font/")) {
            return "Font";
        } else {
            switch (mime_type) {
                case "application/msword":
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                case "application/x-abiword":
                case "application/vnd.oasis.opendocument.text":
                case "application/pdf":
                case "application/rtf":
                    return "Document";

                case "application/zip":
                case "application/x-zip-compressed":
                case "application/gzip":
                case "application/x-gzip":
                case "application/x-bzip":
                case "application/x-bzip2":
                case "application/vnd.rar":
                case "application/x-tar":
                case "application/x-7z-compressed":
                    return "Archive";

                case "application/octet-stream":
                    return "Executable";

                case "text/calendar":
                    return "Calendar";

                case "text/csv":
                case "application/vnd.oasis.opendocument.spreadsheet":
                case "application/vnd.ms-excel":
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "Spreadsheet";

                case "application/vnd.oasis.opendocument.presentation":
                case "application/vnd.ms-powerpoint":
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    return "Presentation";

                default:
                    return "File";
            }
        }
    }

    public static string get_icon (string mime_type) {
        if (mime_type.has_prefix ("audio/")) {
            return "audio-x-generic-symbolic";
        } else if (mime_type.has_prefix ("image/")) {
            return "image-x-generic-symbolic";
        } else if (mime_type.has_prefix ("video/")) {
            return "video-x-generic-symbolic";
        } else if (mime_type.has_prefix ("font/")) {
            return "font-x-generic-symbolic";
        } else {
            switch (mime_type) {
                case "application/msword":
                case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
                case "application/x-abiword":
                case "application/vnd.oasis.opendocument.text":
                case "application/pdf":
                case "application/rtf":
                    return "x-office-document-symbolic";

                case "application/zip":
                case "application/x-zip-compressed":
                case "application/gzip":
                case "application/x-gzip":
                case "application/x-bzip":
                case "application/x-bzip2":
                case "application/vnd.rar":
                case "application/x-tar":
                case "application/x-7z-compressed":
                    return "package-x-generic-symbolic";

                case "application/octet-stream":
                    return "application-x-executable-symbolic";

                case "text/calendar":
                    return "x-office-calendar-symbolic";

                case "text/csv":
                case "application/vnd.oasis.opendocument.spreadsheet":
                case "application/vnd.ms-excel":
                case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                    return "x-office-spreadsheet-symbolic";

                case "application/vnd.oasis.opendocument.presentation":
                case "application/vnd.ms-powerpoint":
                case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
                    return "x-office-presentation-symbolic";

                default:
                    return "text-x-generic-symbolic";
            }
        }
    }
}
