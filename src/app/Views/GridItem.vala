using Gdk;
using Gtk;

namespace Singularity {
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/GridItem.ui")]
    class GridItem : Widget {
        public TextureCache icons { get; private set; }
        public Item item {
            get { return _item; }
            set {
                if (_item != value) {
                    if (_item != null) {
                        // TODO
                        icon = null;
                        _item.notify["starred"].disconnect (on_status_changed);
                        _item.notify["unread"].disconnect (on_status_changed);
                    }

                    _item = value;
                    if (_item != null) {
                        _item.notify["starred"].connect (on_status_changed);
                        _item.notify["unread"].connect (on_status_changed);
                        on_status_changed ();
                        if (_item.owner.icon != null) {
                            feed_icon.paintable = _item.owner.icon;
                        } else {
                            feed_icon.icon_name = "application-rss+xml-symbolic";
                        }
                        description = ItemUtils.gtk_description (_item);
                        var link_value = Value (typeof (string));
                        link_value.set_string (_item.link);
                        _drag_gesture.content = new ContentProvider.for_value (link_value);
                    } else {
                        feed_icon.icon_name = "application-rss+xml-symbolic";
                        description = "";
                    }
                    notify_property ("title");
                }
            }
        }
        private Item _item;
        public string title {
            get {
                if (item == null) {
                    return "";
                }

                return item.title ?? "";
            }
        }
        public bool show_badge {
            get {
                if (item == null) {
                    return false;
                }

                return item.unread || item.starred;
            }
        }
        public bool show_icon { get { return icon != null; } }
        public string description { owned get; set; }
        public Paintable? icon { get; set; }
        public int content_height { get; private set; default = 160; }

        static construct {
            install_action ("copy_link", null, (w) => {
                Gdk.Display.get_default ().get_clipboard ().set_text (((GridItem)w).item.link);
            });
            install_action ("read", null, (w) => {
                var? view = WidgetUtils.find_parent<GridItemView> (w);
                if (view != null) {
                    view.item_read_toggle (((GridItem)w).item);
                }
            });
            install_action ("bookmark", null, (w) => {
                var? view = WidgetUtils.find_parent<GridItemView> (w);
                if (view != null) {
                    view.item_star_toggle (((GridItem)w).item);
                }
            });
            install_action ("navigate", null, (w) => {
                ((GridItem)w).on_open_link ();
            });

            _menu_builder = new Builder.from_resource (APP_ID_PATH + "/Views/ItemMenu.ui");
            add_binding_action (Gdk.Key.M, ModifierType.NO_MODIFIER_MASK, "read", null);
            add_binding_action (Gdk.Key.B, ModifierType.NO_MODIFIER_MASK, "bookmark", null);
        }
        construct {
            _context_menu_gesture = new GestureClick () {
                button = Gdk.BUTTON_SECONDARY,
                exclusive = true,
            };
            _context_menu_gesture.pressed.connect (on_context_menu);
            add_controller (_context_menu_gesture);

            _open_link_gesture = new GestureClick () {
                button = Gdk.BUTTON_MIDDLE,
                exclusive = true,
            };
            _open_link_gesture.pressed.connect (on_open_link);
            add_controller (_open_link_gesture);

            _drag_gesture = new DragSource () {
                actions = DragAction.COPY | DragAction.LINK,
            };
            add_controller (_drag_gesture);

            map.connect (on_map);
        }

        ~GridItem () {
            WidgetUtils.cleanup_children (this);
            item = null;
        }

        private void on_map () {
            var view = WidgetUtils.find_parent<GridItemView> (this);
            if (view != null) {
                icons = view.icons;
            }

            if (item != null) {
                string? icon_uri = WebElementBuilders.get_post_icon (item);
                if (icon_uri != null) {
                    icons.get_for_uri.begin (icon_uri, (obj, res) => {
                        icon = icons.get_for_uri.end (res);
                        notify_property ("show-icon");
                    });
                }
            }
        }

        private void on_open_link () {
            try {
                GLib.Process.spawn_command_line_async (
                    AppSettings.link_command.printf (item.link)
                );
                var? view = WidgetUtils.find_parent<GridItemView> (this);
                if (view != null) {
                    view.items_viewed ({ item });
                }
            } catch (Error e) {
                warning ("Error opening external link: %s", e.message);
            }
        }

        private void on_context_menu (int _, double x, double y) {
            if (context_menu == null) {
                context_menu = new PopoverMenu.from_model (_menu_builder.get_object ("menu") as MenuModel) {
                    has_arrow = false,
                };
                context_menu.set_parent (this);
            }
            context_menu.set_pointing_to ({ (int)x, (int)y, 0, 0 });
            context_menu.popup ();
        }

        private void on_status_changed () {
            if (item != null && show_badge) {
                unread_icon.set_from_icon_name (item.starred ? "bookmarks-filled-symbolic" : "shape-circle-filled-symbolic");
                if (item.starred) {
                    unread_icon.remove_css_class ("unread-item");
                    unread_icon.add_css_class ("bookmarked-item");
                } else {
                    unread_icon.remove_css_class ("bookmarked-item");
                    unread_icon.add_css_class ("unread-item");
                }
                unread_icon.tooltip_text = item.starred ? "This item is bookmarked" : "This item is unread";
            }
            notify_property ("show-badge");
        }

        private GestureClick _context_menu_gesture;
        private GestureClick _open_link_gesture;
        private DragSource _drag_gesture;

        private PopoverMenu context_menu;

        private static Builder _menu_builder;

        [GtkChild]
        private unowned Image feed_icon;

        [GtkChild]
        private unowned Image unread_icon;
    }
}
