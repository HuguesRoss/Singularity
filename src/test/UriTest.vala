using Singularity;

namespace Singularity.Tests {
    public class UriTests : UnitTest {
        public string name { get { return "URI"; } }

        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("base_uri", testBaseUri),
            });
        }

        private static void testBaseUri () {
            // TODO: Add more test cases
            string[] uris = {
                "http://example.com",
                "https://exampleforumsite.com/viewtopic.php?t=94365&amp;p=253683#p253683",
                "https://exampleforumsite.com/topics/viewtopic.php?t=94365&amp;p=253683#p253683",
            };
            string[] base_uris = {
                "http://example.com/",
                "https://exampleforumsite.com/",
                "https://exampleforumsite.com/",
            };
            string[] base_uris_with_path = {
                "http://example.com/",
                "https://exampleforumsite.com/",
                "https://exampleforumsite.com/topics/",
            };

            for (int i = 0; i < uris.length; ++i) {
                string base_uri = UriUtils.get_base (uris[i]);
                string base_uri_with_path = UriUtils.get_base (uris[i], true);
                message ("BEFORE \"%s\"", uris[i]);
                message ("AFTER \"%s\" == \"%s\"", base_uri, base_uris[i]);
                message ("AFTER (BASE) \"%s\" vs \"%s\"", base_uri_with_path, base_uris_with_path[i]);
                assert (base_uri == base_uris[i]);
                assert (base_uri_with_path == base_uris_with_path[i]);
            }
        }
    }
}
