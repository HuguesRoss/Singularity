namespace Singularity {
    // Represents a node within a CollectionTreeStore, and wraps FeedDataEntry
    // with information about parents and helper functions to help prevent extra
    // type-checking code elsewhere
    public class CollectionNode : Object, IFeedListNode {
        public bool can_filter { get { return true; } }
        public bool can_rename { get { return data is FeedCollection && id != -1; } }
        public FeedDataEntry data { get; construct set; }
        public int id { get { return data.id; } }
        public string description {
            get {
                if (data is Feed) {
                    return ((Feed)data).description;
                } else {
                    return "";
                }
            }
        }
        public string title { get { return data.title; } }
        public string type_name { get { return (data is FeedCollection) ? "Collection" : "Feed"; } }
        public string sort_key { get { return data.sort_key; } }
        public string icon_name { get { return data.icon != null ? null : data.icon_name; } }
        public Gdk.Paintable? icon { get { return data.icon; } }

        private int _unread_count;
        public int unread_count { get { return _unread_count; } }

        private bool _has_error;
        public bool has_error { get { return _has_error; } }
        public bool has_unread { get { return _unread_count > 0; } }

        public CollectionNode (FeedDataEntry entry) {
            Object (data: entry);
            refresh_unread ();
            data.notify.connect (on_notify);
        }

        ~CollectionNode () {
            clear_children ();
        }

        public ItemQuery query () {
            return new ItemQuery () { feed_node = data, };
        }

        public void set_failed () {
            _has_error = true;
            notify_property ("has-error");
        }

        public void add_child (IFeedListNode child) {
            if (_children == null) {
                return;
            }

            add_child_internal (child);

            refresh_unread ();
        }
        public void remove_child (IFeedListNode child) {
            if (_children == null) {
                return;
            }

            remove_child_internal (child);

            refresh_unread ();
        }
        // Returns a list containing all of the node's children.
        // If this node wraps a Feed, an empty list is returned.
        public Gee.List<IFeedListNode> get_children () {
            if (_children == null) {
                _children = new Gee.ArrayList<IFeedListNode> ();
                if (data is FeedCollection) {
                    foreach (var node in ((FeedCollection)data).nodes) {
                        add_child_internal (new CollectionNode (node));
                    }
                }
            }

            return _children;
        }

        public override void refresh_unread () {
            int original_count = _unread_count;

            if (data is Feed) {
                _unread_count = ((Feed)data).unread_count;
            } else {
                _unread_count = get_children ().fold<int> ((n, c) => c + n.unread_count, 0);
            }

            if (original_count != _unread_count) {
                notify_property ("unread-count");
                notify_property ("has-unread");
            }
        }

        private void clear_children () {
            if (_children != null) {
                foreach (IFeedListNode node in _children) {
                    node.notify["unread-count"].disconnect (on_child_unread_changed);
                }
                _children = null;
            }
        }

        private void add_child_internal (IFeedListNode child) {
            child.notify["unread-count"].connect (on_child_unread_changed);
            _children.add (child);
        }

        private void remove_child_internal (IFeedListNode child) {
            child.notify["unread-count"].disconnect (on_child_unread_changed);
            _children.remove (child);
        }

        private void on_child_unread_changed (ParamSpec p) {
            refresh_unread ();
        }

        private void on_notify (ParamSpec p) {
            switch (p.name) {
                case "id":
                case "title":
                case "description":
                case "sort-key":
                    notify_property (p.name);
                    break;
                case "icon":
                    notify_property ("icon");
                    break;
                case "unread-count":
                    lock (_refresh_queued) {
                        if (_refresh_queued) {
                            return;
                        }

                        _refresh_queued = true;
                    }
                    Idle.add_once (() => {
                        lock (_refresh_queued) {
                            _refresh_queued = false;
                        }
                        refresh_unread ();
                    });
                    break;
            }
        }

        private Gee.ArrayList<IFeedListNode> _children;
        private bool _refresh_queued;
    }
}
