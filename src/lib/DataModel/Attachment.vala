using Singularity.Db;

namespace Singularity {
    /** Represents an attached file on an item (sometimes refered to as an enclosure) */
    public class Attachment : DataEntryGuid {
        public string name { get; set; }
        public string url { get; set; }
        public string? description { get; set; }
        public string? icon { get; set; }
        public string? hash { get; set; }
        public string? hash_algo { get; set; }
        public string? embed_uri { get; set; }
        public string? player_uri { get; set; }
        public string? rights { get; set; }
        public string? medium { get; set; }
        public int64 views { get; set; default = -1; }
        public int64 favorites { get; set; default = -1; }
        public int64 ratings { get; set; default = -1; }
        public double avg_rating { get; set; default = -1; }
        public double min_rating { get; set; default = -1; }
        public double max_rating { get; set; default = -1; }

        /** Gets the list of people that created this attachment */
        public Gee.Collection<Person> authors = new Gee.ArrayList<Person> ();

        /** The size in bytes */
        public int64 size { get; set; }

        /** The MIME type of the attached file */
        public string mimetype { get; set; }

        public Attachment.from_webfeed (WebFeed.Attachment attachment) {
            name = attachment.filename;
            url = attachment.file_uri;
            description = attachment.description;
            icon = attachment.icon_uri;
            if (attachment.hash != null) {
                hash = attachment.hash.value;
                if (attachment.hash.algorithm != WebFeed.HashAlgorithm.UNKNOWN) {
                    hash_algo = WebFeed.HashAlgorithm.to_string (attachment.hash.algorithm);
                }
            }
            embed_uri = attachment.embed_uri;
            player_uri = attachment.player_uri;
            rights = attachment.rights;
            medium = attachment.medium;
            views = attachment.views;
            favorites = attachment.favorites;
            ratings = attachment.ratings;
            avg_rating = attachment.avg_rating;
            min_rating = attachment.min_rating;
            max_rating = attachment.max_rating;
            authors.add_all_iterator (attachment.authors.map<Person> (p => new Person (p.name, p.uri, p.email)));
            size = attachment.size;
            mimetype = attachment.mimetype;
        }

        public Attachment.from_record (IDbResult r) {
            base.from_record (r);
        }

        // Prepares and hashes this object's guid
        public void prepare_for_db (Item owner) {
            set_guid (md5_guid (owner.guid + url));
        }

        /** Populate data from a database record
         * @param r The record to read
         */
        protected override void build_from_record (IDbResult r) {
            set_guid (r.get_string ("guid"));
            name = r.get_string ("name");
            url = r.get_string ("uri");
            description = r.get_string ("description");
            icon = r.get_string ("icon");
            hash = r.get_string ("hash");
            hash_algo = r.get_string ("hash_algo");
            embed_uri = r.get_string ("embed_uri");
            player_uri = r.get_string ("player_uri");
            rights = r.get_string ("rights");
            medium = r.get_string ("medium");
            size = r.get_int64 ("length");
            mimetype = r.get_string ("mimetype");
            views = r.get_int64 ("views");
            favorites = r.get_int64 ("favorites");
            ratings = r.get_int64 ("ratings");
            avg_rating = r.get_double ("avg_rating");
            min_rating = r.get_double ("min_rating");
            max_rating = r.get_double ("max_rating");
        }
    }
}
