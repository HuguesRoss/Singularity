namespace Singularity.Tests {
    public class PersonTest : UnitTest {
        public string name { get { return "Person"; } }
        public TestCase[] test_cases;
        public void add_tests (string path) {
            add_cases (path, {
                TestCase ("name", testPersonName),
                TestCase ("url", testPersonURL),
                TestCase ("email", testPersonEmail),
                TestCase ("is_valid_name", testPersonIsValidName),
                TestCase ("is_valid_email", testPersonIsValidEmail),
                TestCase ("is_invalid_url", testPersonIsInvalidURL),
                TestCase ("is_invalid_empty_name", testPersonIsInvalidEmptyName),
                TestCase ("is_invalid_empty_email", testPersonIsInvalidEmptyEmail),
                TestCase ("is_invalid_null", testPersonIsInvalidNull),
            });
        }

        private static void testPersonName () {
            Person person = new Person ("Alice");
            assert ("Alice" == person.name);
        }
        private static void testPersonURL () {
            Person person = new Person ("Bob", "http://example.com");
            assert ("http://example.com" == person.uri);
        }
        private static void testPersonEmail () {
            Person person = new Person ("Chris", "http://example.com", "chris@example.com");
            assert ("chris@example.com" == person.email);
        }
        private static void testPersonIsValidName () {
            Person person = new Person ("Alice");
            assert (person.is_valid);
        }
        private static void testPersonIsValidEmail () {
            Person person = new Person (null, null, "bob@example.com");
            assert (person.is_valid);
        }
        private static void testPersonIsInvalidURL () {
            Person person = new Person (null, "http://example.com", null);
            assert (!person.is_valid);
        }
        private static void testPersonIsInvalidEmptyName () {
            Person person = new Person ("");
            assert (!person.is_valid);
        }
        private static void testPersonIsInvalidEmptyEmail () {
            Person person = new Person (null, null, "");
            assert (!person.is_valid);
        }
        private static void testPersonIsInvalidNull () {
            Person person = new Person (null);
            assert (!person.is_valid);
        }
    }
}
