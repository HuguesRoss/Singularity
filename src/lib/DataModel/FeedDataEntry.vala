using Singularity.Db;

namespace Singularity {
    // DataEntry holding common data for both the Feed and Collection classes
    public abstract class FeedDataEntry : DataEntry {
        // Represents the a FeedDataEntry's type in the database.
        // TODO: Refactor out if possible, find a more elegant way to describe
        //       the Feed/FeedCollection type difference
        public enum Contents {
            FEED,
            COLLECTION
        }


        public string title {
            get { return _title; }
            set {
                if (_title != value) {
                    _title = value;
                    notify_property ("title");
                    notify_property ("sort_key");
                }
            }
        }
        private string _title;

        public string sort_key {
            get { return _sort_key ?? title; }
            set {
                if (_sort_key != value) {
                    _sort_key = value;
                    notify_property ("sort_key");
                }
            }
        }
        private string _sort_key;
        public Gdk.Paintable? icon {
            get { return _icon; }
            set {
                if (_icon != value) {
                    _icon = value;
                    notify_property ("icon");
                }
            }
        }
        private Gdk.Paintable? _icon = null;
        public int parent_id { get; protected set; default = -1; }
        public abstract string icon_name { get; }

        public virtual string to_string () {
            return "%d (%d): %s".printf (id, parent_id, title);
        }

        private FeedCollection? _parent = null;
        public FeedCollection? parent {
            get { return _parent; }
            set {
                _parent = value;

                if (value == null) {
                    parent_id = -1;
                } else {
                    parent_id = value.id;
                }

                notify_property ("parent");
            }
        }

        public abstract Gee.List<Feed> get_feeds ();
    }
}
