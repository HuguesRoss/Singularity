using Gtk;
using Gee;

namespace Singularity {
    /**
     * Widget for displaying a single feed's update error message
     */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/ErrorLabel.ui")]
    public class ErrorLabel : Widget {
        public ErrorLabel (Feed feed, string message) {
            title_label.label = feed.title;
            error_label.label = message;

            if (feed.icon != null) {
                feed_icon.paintable = feed.icon;
            } else {
                feed_icon.icon_name = "application-rss+xml-symbolic";
            }
        }

        ~ErrorLabel () {
            WidgetUtils.cleanup_children (this);
        }

        [GtkCallback]
        public void on_copy () {
            var clipboard = get_clipboard ();
            clipboard.set_text ("%s: %s".printf (title_label.label, error_label.label));
        }

        [GtkChild]
        private unowned Label error_label;

        [GtkChild]
        private unowned Image feed_icon;

        [GtkChild]
        private unowned Label title_label;
    }
}
