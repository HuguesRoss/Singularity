namespace Singularity {
    /** Generates an embed for video attachments */
    public class VideoEmbedBuilder : Object, IEmbedBuilder {
        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         *
         * @return true if the embed could be made, false if this attachment isn't supported
         */
        public bool build_attachment_embed (Attachment attachment, Item item, out string embed) {
            if (StringUtils.null_or_empty (attachment.mimetype) || (!attachment.mimetype.has_prefix ("video") && attachment.medium != "video")) {
                embed = null;
                return false;
            }

            embed = """<video class="singularity-attachment-embed" controls src="%s" preload="metadata" style="display: block;"></video>""".printf (UriUtils.make_absolute (attachment.url, item.owner.base_uri));
            return true;
        }
    }
}
