namespace Singularity {
    public class CacheInfo {
        public DateTime last_good_result { get; set; }
        public string url { get; set; }
        public string? last_modified { get; set; }
        public string? etag { get; set; }
        public bool is_empty {
            get { return last_modified == null && etag == null; }
        }

        public void apply_to_message (Soup.Message message) {
            // TODO: timeout if last_good_result is very old?

            if (last_modified != null) {
                message.request_headers.append ("If-Modified-Since", last_modified);
            } else if (etag != null) {
                message.request_headers.append ("If-None-Match", etag);
            }
        }

        public static CacheInfo? try_build_from_response (Soup.Message message) {
            string? last_modified = message.response_headers.get_one ("Last-Modified");
            string? etag = message.response_headers.get_one ("ETag");
            if (last_modified != null || etag != null) {
                return new CacheInfo () {
                    url = message.uri.to_string (),
                    last_good_result = new DateTime.now_utc (),
                    last_modified = last_modified,
                    etag = etag,
                };
            }

            return null;
        }
    }
}
