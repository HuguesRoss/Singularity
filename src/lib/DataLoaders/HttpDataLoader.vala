namespace Singularity {
    const string USER_AGENT = "Singularity RSS Reader/0.6-dev [https://www.huguesross.net/code/singularity]";

    /** Data loader for loading feeds via HTTP */
    public class HttpDataLoader : Object, IDataLoader {
        /** The error message for this request, if any */
        public string? error_message { get; protected set; }

        /** The contextual data for this update */
        public UpdateContext update_context { get; set; }

        /** The request URI */
        public DataLoadRequest request { get; set; }

        /** Returns the base URI that this request is loading from */
        public string get_base_uri () {
            return UriUtils.get_base (request.uri);
        }

        /**
         * Sends the request asynchronously
         *
         * @return true if the request succeeded; otherwise, false
         */
        public async DataLoadResponse send_async () {
            var session = new Soup.Session ();
            session.user_agent = USER_AGENT;
            if (update_context != null && update_context.cookies != null) {
                session.add_feature (update_context.cookies);
            }

            var message = new Soup.Message ("GET", request.uri);

            if (message == null) {
                return new DataLoadResponse.error ("Invalid URL");
            }


            if (request.cache_info != null) {
                request.cache_info.apply_to_message (message);
            }

            try {
                Bytes bytes = yield session.send_and_read_async (message, 0, null);

                if (message.status_code == 304) { // Not Modified, no need to check again
                    // TODO
                    return new DataLoadResponse ();
                } if (message.status_code >= 400) {
                    return new DataLoadResponse.error (get_status_error (message.status_code));
                }

                if (bytes.length == 0) {
                    return new DataLoadResponse.error ("Server returned no data");
                }

                return new DataLoadResponse () {
                    bytes = new Bytes (bytes.get_data ()),
                    cache_info = CacheInfo.try_build_from_response (message),
                };
            } catch (GLib.Error e) {
                return new DataLoadResponse.error (e.message);
            }
        }

        /**
         * Get a textual error from the given HTTP status code
         *
         * @param status_code The status code
         * @return A string containing an error message
         */
        private string get_status_error (uint status_code) {
            string description = "";
            switch (status_code) {
                case Soup.Status.NOT_FOUND:
                    description = "Not found";
                    break;
                case Soup.Status.FORBIDDEN:
                    description = "Forbidden";
                    break;
                case Soup.Status.UNAUTHORIZED:
                    description = "Unauthorized";
                    break;
                case 429:
                    description = "Too many requests";
                    break;
                default:
                    return "Server returned error code %u".printf (status_code);
            }

            return "Server returned error code %u: %s".printf (status_code, description);
        }
    }
}
