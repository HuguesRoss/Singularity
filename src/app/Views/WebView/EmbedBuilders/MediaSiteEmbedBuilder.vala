namespace Singularity {
    /** Generates an embed for attachments with an external video player */
    public class MediaSiteEmbedBuilder : Object, IEmbedBuilder {
        public override bool supports_download { get { return false; } }

        /**
         * Build an HTML embed for a post attachment
         * 
         * @param attachment The attachment
         * @param item The associated item
         * @param embed Contains the embed, if valid
         *
         * @return true if the embed could be made, false if this attachment isn't supported
         */
        public bool build_attachment_embed (Attachment attachment, Item item, out string embed) {
            string? embed_uri = attachment.embed_uri;
            string? player_uri = attachment.player_uri ?? attachment.embed_uri ?? attachment.url;
            if (embed_uri == null) {
                string id = null;
                if (player_uri != null && Websites.Youtube.matches_video_link (player_uri, out id)) {
                    embed_uri = Websites.Youtube.embed_uri (id);
                }

                if (embed_uri == null) {
                    embed = null;
                    return false;
                }
            }

            embed = """<iframe class="singularity-attachment-embed singularity-frame" allowfullscreen="" height="360" src="%s" sandbox="allow-same-origin allow-scripts allow-popups" width="640"></iframe><p><a target="_blank" rel="noopener noreferrer" href="%s">Watch in a web browser</a></p>""".printf (embed_uri, player_uri);
            return true;
        }
    }
}
