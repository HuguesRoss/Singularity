/*
    Singularity - A web newsfeed aggregator
    Copyright (C) 2022  Hugues Ross <hugues.ross@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace Singularity {
    /**
     * Various string-related utility functions
     */
    public class StringUtils {
        /**
         * Check if a string is null or empty (ie. equal to "")
         *
         * @param str The string to check
         *
         * @return true if the input was null or empty
         */
        public static bool null_or_empty (string? str) {
            return str == null || str == "";
        }

        /**
         * Convert a number of bytes into a human-readable string format
         *
         * @param size The size in bytes
         *
         * @return A string in a format similar to "13.5Gb"
         */
        public static string to_legible_filesize (ulong size) {
            string[] filesize_suffixes = {
                "b",
                "Kb",
                "Mb",
                "Gb",
                "Tb",
                "Pb",
            };
            int i = 0;
            ulong remainder = 0;
            for (; i < filesize_suffixes.length - 1; i++) {
                if (size / 1024 == 0) {
                    break;
                }
                remainder = size % 1024;
                size = size / 1024;
            }

            remainder = remainder / 102;
            if (remainder % 10 >= 5) {
                remainder++;
            }

            if (remainder > 9) {
                size++;
                remainder = 0;
            }

            if (remainder > 0) {
                return "%lu.%1lu%s".printf (size, remainder, filesize_suffixes[i]);
            } else {
                return "%lu%s".printf (size, filesize_suffixes[i]);
            }
        }

        /**
         * Try to find and <img> tag and get its URL from the src attribute
         *
         * @param content The content to scan
         *
         * @return An image URL, or null if none can be found
         */
        public static string? extract_image (string content) {
            StringBuilder builder = new StringBuilder ();
            bool in_tag = false;
            bool in_img = false;
            bool in_src = false;
            bool in_str = false;
            int tag_start = -1;
            for (int i = 0; i < content.data.length; ++i) {
                uint8 ch = content.data[i];

                if (in_src && (ch == '\"' || ch == '\'')) {
                    in_str = !in_str;
                    if (builder.str != "")
                        return builder.str;
                } else if (in_str) {
                    builder.append_c ((char)ch);
                } else if (in_img && content.substring (i - 3, 3) == "src") {
                    in_src = true;
                } else if (in_tag && i - tag_start == 4 && content.substring (tag_start + 1, 3) == "img") {
                    in_img = true;
                } else if (ch == '<') {
                    in_tag = true;
                    tag_start = i;
                } else if (ch == '>') {
                    in_tag = false;
                    in_img = false;
                    in_src = false;
                    in_str = false;
                }
            }

            return null;
        }
    }
}
