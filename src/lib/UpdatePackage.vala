namespace Singularity {
    /**
     * Represents the result of a feed update created by an UpdateGenerator
     * This is just a container, and has no logic of its' own
     */
    public class UpdatePackage : Object {
        /** Whether or not this package is the result of an error */
        public bool is_error { get; construct; }

        public Feed feed { get; construct; }
        public Gee.Collection<Item?>? new_items { get; construct; }
        public string? message { get; construct; }
        public CacheInfo? feed_cache_info { get; set; }
        public CacheInfo? icon_cache_info { get; set; }
        public bool usable { get; construct; }

        public UpdatePackage.success (Feed f, Gee.Collection<Item?> i_new) {
            Object (
                is_error: false,
                feed: f,
                new_items: i_new,
                message: null,
                usable: true);
        }

        public UpdatePackage.ignore (Feed f) {
            Object (
                is_error: false,
                feed: f,
                new_items: null,
                message: null,
                usable: false);
        }

        public UpdatePackage.failure (Feed f, string m) {
            Object (
                is_error: true,
                feed: f,
                new_items: null,
                message: m,
                usable: false);
        }
    }
}
