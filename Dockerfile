FROM alpine:3.15

RUN apk add --no-cache meson gcc vala vala-lint git \
  gettext txt2man appstream-glib desktop-file-utils \
  gdk-pixbuf-dev gtk+3.0-dev keybinder3-dev libc-dev \
  libgcrypt-dev libgee-dev libsoup-dev libxml2-dev sqlite-dev \
  webkit2gtk-dev
