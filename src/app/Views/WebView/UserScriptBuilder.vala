using WebKit;

namespace Singularity.UserScriptBuilder {
    string load_source_from_resource (string uri) throws Error {
        File userscript_resource = File.new_for_uri (uri);
        FileInputStream stream = userscript_resource.read ();
        DataInputStream data_stream = new DataInputStream (stream);

        // Load the linking JS and inject it
        StringBuilder builder = new StringBuilder ();
        string? str = data_stream.read_line ();
        do {
            builder.append (str + "\n");
            str = data_stream.read_line ();
        } while (str != null);

        return builder.str;
    }

    UserScript load_from_resource (string uri) throws Error {
        return new UserScript (
            load_source_from_resource (uri),
            UserContentInjectedFrames.TOP_FRAME,
            UserScriptInjectionTime.START, null, null);
    }
}
