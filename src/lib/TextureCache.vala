using Gdk;
using Gee;

namespace Singularity {
    public class TextureCache : Object {
        public Soup.CookieJarDB cookies {
            get { return _cookies; }
            set {
                if (_cookies != value) {
                    if (_cookies != null) {
                        _session.remove_feature (_cookies);
                    }

                    _cookies = value;

                    if (_cookies != null) {
                        _session.add_feature (_cookies);
                    }
                }
            }
        }
        private Soup.CookieJarDB _cookies;

        public async Texture? get_for_uri (string uri) {
            if (StringUtils.null_or_empty (uri)) {
                return null;
            }

            string canon = UriUtils.make_canon (uri);
            ILoadTextureJob job = _thumbs[canon];
            if (job == null) {
                string scheme = UriUtils.get_scheme (uri);
                if (scheme == "file") {
                    job = new DownloadTextureJob () {
                        url = canon,
                        session = _session,
                    };
                } else {
                    job = new LoadLocalTextureJob () {
                        url = canon,
                    };
                }

                _thumbs[canon] = job;
                _jobs.request (job);
            }

            if ((yield job.wait ()) == Job.Result.SUCCEEDED) {
                return job.texture;
            }

            return null;
        }

        public async bool save_to_uri (string from_uri, string to_uri) {
            string from_canon = UriUtils.make_canon (from_uri);
            string to_canon = UriUtils.make_canon (to_uri);
            string scheme = UriUtils.get_scheme (to_canon);
            if (from_canon == to_canon || scheme.has_prefix ("http")) {
                return false;
            }

            Texture? tex = yield get_for_uri (from_canon);
            if (tex == null) {
                return false;
            }

            _thumbs[to_canon] = new LoadLocalTextureJob.with_result (tex);

            var file = File.new_for_uri (to_canon);
            // TODO: Not ideal for non-PNG extensions... we should add something to re-format or update the extension on save later
            tex.save_to_png (file.get_path ());

            return true;
        }

        /**
         * Forcibly remove a texture from the cache, forcing it to be redownloaded next request
         *
         * @param uri The texture uri
         */
        public void invalidate (string uri) {
            if (StringUtils.null_or_empty (uri)) {
                return;
            }

            string canon = UriUtils.make_canon (uri);
            _thumbs.unset (canon);
        }

        private HashMap<string, ILoadTextureJob> _thumbs = new HashMap<string, ILoadTextureJob> ();
        private static Soup.Session _session = new Soup.Session ();
        private JobQueue _jobs = new JobQueue (8, 500000 /* 0.5 seconds */);
    }
}
