using Gee;
using Singularity.Db;

namespace Singularity {
    // Represents an RSS/Atom/RDF feed
    public class Feed : FeedDataEntry {
        /** Gets the list of people that created this feed */
        public Collection<Person> authors = new Gee.ArrayList<Person> ();

        public string? description { get; set; }

        /** The base uri to this feed's link */
        public string base_uri {
            get { return _base_uri ?? (_base_uri = UriUtils.get_base (link)); }
        }

        /** The link to this feed's source document */
        public string link {
            get { return _link; }
            set {
                if (_link != value) {
                    _link = value;
                    _base_uri = null;
                }
            }
        }
        public string? site_link { get; set; }
        public string? rights { get; set; }
        public Collection<Tag?> tags { get; set; }
        public string? generator { get; set; }

        /** The URI to this feed's icon, if one exists */
        public string? icon_uri { get; set; }
        public override string icon_name { get { return "application-rss+xml-symbolic"; } }
        public DateTime last_update { get; set; default = new DateTime.from_unix_utc (0); }
        public DateTime last_new_content { get; set; default = new DateTime.from_unix_utc (0); }
        public int unread_count {
            get { return _unread_count; }
            internal set {
                if (_unread_count != value) {
                    _unread_count = value;
                    notify_property ("unread-count");
                }
            }
        }
        private int _unread_count;

        /**
         * Attachment object holding default values for item attachments
         * NOTE: This is never saved or loaded, and only (sometimes) exists at parse time. **Always null-check!**
         */
        public Attachment? attachment_defaults { get; set; }

        public Feed () {}

        public Feed.from_record (IDbResult r) {
            base.from_record (r);
        }

        /**
         * Sets contents based on a FeedProvider's contents
         *
         * @param feed The feed to copy from
         */
        public void update_contents (WebFeed.Feed feed) {
            title = feed.title == "" ? "Untitled Feed" : feed.title;

            if (feed.description != null)
                description = feed.description;

            authors.clear ();
            authors.add_all_iterator (feed.authors.map<Person> (p => new Person (p.name, p.uri, p.email)));

            if (feed.site_link != null)
                site_link = feed.site_link;

            if (feed.rights != null)
                rights = feed.rights;

            // TODO: Update tags

            if (feed.generator != null)
                generator = feed.generator;

            if (icon_uri != feed.icon_uri) {
                icon_uri = feed.icon_uri;
                icon = null;
            }

            last_update = feed.last_update;
        }

        /** Creates a query for inserting this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? insert (IDbTarget q) {
            Statement query = new Statement (q, """INSERT INTO feeds
                    (id, parent_id, type, title, link, site_link, description, rights, generator, last_update)
                    VALUES (
                        :id,
                        :parent_id,
                        :type,
                        :title,
                        :link,
                        :site_link,
                        :description,
                        :rights,
                        :generator,
                        :last_update,
                        :last_new_content
                        )""");
            query[":id"] = id;
            query[":parent_id"] = parent_id;
            query[":type"] = (int)FeedDataEntry.Contents.FEED;
            query[":title"] = title;
            query[":link"] = link;
            query[":site_link"] = site_link;
            query[":description"] = description;
            query[":rights"] = rights;
            query[":generator"] = generator;
            query[":last_update"] = last_update.to_unix ();
            query[":last_new_content"] = last_new_content.to_unix ();
            // TODO: Decide how to store icons
            // TODO: Decide how to store tags
            return query;
        }

        /** Creates a query for updating this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? update (IDbTarget q) {
            // TODO: Build the query to only have what's needed
            Statement query = new Statement (q, """UPDATE feeds SET
                    title = :title,
                    link = :link,
                    site_link = :site_link,
                    description = :description,
                    rights = :rights,
                    generator = :generator,
                    last_update = :last_update,
                    last_new_content = :last_new_content
                    WHERE id = :id""");
            query[":id"] = id;
            query[":title"] = title;
            query[":link"] = link;
            query[":site_link"] = site_link;
            query[":description"] = description;
            query[":rights"] = rights;
            query[":generator"] = generator;
            query[":last_update"] = last_update.to_unix ();
            query[":last_new_content"] = last_new_content.to_unix ();
            // TODO: Decide how to store icons
            // TODO: Decide how to store tags
            return query;
        }

        /** Creates a query for removing this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public override Statement? remove (IDbTarget q) {
            Statement query = new Statement (q, "DELETE FROM feeds WHERE `id` = :id");
            query[":id"] = id;
            return query;
        }

        public override string to_string () {
            return "%d (%d): %s [%s | %s]".printf (id, parent_id, title, link, site_link);
        }

        // Sets ID value in preparation for database access
        public void prepare_for_db (int new_id) {
            set_id (new_id);
        }

        // Return a collection containing this feed
        public override Gee.List<Feed> get_feeds () {
            Gee.List<Feed> feeds = new Gee.ArrayList<Feed> ();
            feeds.add (this);
            return feeds;
        }

        /** Populate data from a database record
         *
         * @param r The record to read
         */
        protected override void build_from_record (IDbResult r) {
            // FIXME: This is currently necessary due to a left outer join. See if this can be removed somehow.
            set_id (r.fetch_int (0));

            parent_id = r.get_int ("parent_id");
            title = r.get_string ("title");
            link = r.get_string ("link");
            site_link = r.get_string ("site_link");
            description = r.get_string ("description");
            rights = r.get_string ("rights");
            generator = r.get_string ("generator");
            last_update = new DateTime.from_unix_utc (r.get_int ("last_update"));
            last_new_content = new DateTime.from_unix_utc (r.get_int ("last_new_content"));
            unread_count = r.get_int ("unread_count");
            // TODO: Decide how to store tags
        }

        private string? _base_uri;
        private string? _link;
    }
}
