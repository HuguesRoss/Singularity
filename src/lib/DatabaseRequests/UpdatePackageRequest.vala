using Gee;
using Singularity.Db;

namespace Singularity {
    /** DatabaseRequest for saving the contents of a successful UpdatePackage to the database */
    public class UpdatePackageRequest : IRequest, GLib.Object {
        /** Use a SQLite transaction for the full request? */
        public bool use_transaction { get { return true; } }

        /** The name of the request */
        public string name { get { return "Save feed updates"; } }

        /** The package to save */
        public UpdatePackage package { get; construct set; }

        public bool use_owner_id { get; set; default = true; }

        public UpdatePackageRequest (UpdatePackage pak) {
            Object (package: pak);

            foreach (Item i in package.new_items) {
                i.owner = package.feed;
                i.prepare_for_db ();
                _items.add (i);
                foreach (Attachment a in i.attachments) {
                    a.prepare_for_db (i);

                    foreach (Person author in a.authors) {
                        author.prepare_for_db ();
                        _people.add (author);
                    }
                }
                foreach (Person author in package.feed.authors) {
                    author.prepare_for_db ();
                    _people.add (author);
                }
                foreach (Person author in i.authors) {
                    author.prepare_for_db ();
                    _people.add (author);
                }
            }
        }

        public Status step (IDbTarget db) {
            // If needed, retrieve the feed id
            if (!use_owner_id) {
                var q = new Statement (db, "SELECT `id` FROM `feeds` WHERE link = :link");
                q[":link"] = package.feed.link;
                var res = q.execute ();

                // Can't find the feed anymore? Assume it has been deleted
                // while the request was pending
                if (res.finished) {
                    return Status.COMPLETED;
                }

                package.feed.prepare_for_db (res.get_int ("id"));
            }

            // Update the feed's data
            with (var q = new Statement (db, "UPDATE feeds SET (title, link, site_link, description, rights, generator, last_update) = (:title, :link, :site_link, :description, :rights, :generator, :last_update) WHERE id = :id")) {
                q[":title"] = package.feed.title;
                q[":link"] = package.feed.link;
                q[":site_link"] = package.feed.site_link;
                q[":description"] = package.feed.description;
                q[":rights"] = package.feed.rights;
                q[":generator"] = package.feed.generator;
                q[":last_update"] = package.feed.last_update.to_unix ();
                q[":id"] = package.feed.id;
                q.execute ();
            }

            with (var q = new Statement (db, "INSERT OR IGNORE INTO people (guid, url, name, email) VALUES (:guid, :url, :name, :email)")) {
                foreach (Person person in _people) {
                    q[":guid"] = person.guid;
                    q[":url"] = person.uri;
                    q[":name"] = person.name;
                    q[":email"] = person.email;
                    q.execute ();
                    q.reset ();
                }
            }

            if (!package.feed.authors.is_empty) {
                var q_delete = new Statement (db, "DELETE FROM feed_authors WHERE feed_id = :feed_id");
                q_delete[":feed_id"] = package.feed.id;
                q_delete.execute ();

                with (var q = new Statement (db, "INSERT OR IGNORE INTO feed_authors (feed_id, author_id) VALUES (:feed_id, :author_id)")) {
                    foreach (Person author in package.feed.authors) {
                        q[":feed_id"] = package.feed.id;
                        q[":author_id"] = author.guid;
                        q.execute ();
                        q.reset ();
                    }
                }
            }

            if (package.feed.icon is Gdk.Texture) {
                Statement q = new Statement (db, "REPLACE INTO icons (id, width, height, alpha, bits, rowstride, data) VALUES (:id, :width, :height, :alpha, :bits, :rowstride, :data)");
                q[":id"] = package.feed.id;
                q[":width"] = 0;
                q[":height"] = 0;
                q[":alpha"] = 0;
                q[":bits"] = 0;
                q[":rowstride"] = 0;
                Bytes last_icon_bytes = ((Gdk.Texture)package.feed.icon).save_to_png_bytes ();
                q.set_blob (":data", last_icon_bytes.get_data ());
                q.execute ();
            }

            var q_count = new Statement (db, "SELECT COUNT (*) FROM items");
            int item_count = q_count.execute ().fetch_int (0);
            q_count.reset ();

            bool any_items_inserted = false;
            with (var q = new Statement (db, "INSERT INTO items (guid, parent_id, weak_guid, title, link, description, content, rights, publish_time, update_time, load_time, unread, starred, icon) VALUES (:guid, :parent_id, :weak_guid, :title, :link, :description, :content, :rights, :publish_time, :update_time, :load_time, :unread, :starred, :icon) ON CONFLICT (guid) DO UPDATE SET title=excluded.title, link=excluded.link, description=excluded.description, content=excluded.content, rights=excluded.rights, update_time=excluded.update_time, icon=excluded.icon RETURNING unread, starred")) {
                foreach (Item item in _items) {
                    q[":guid"] = item.guid;
                    q[":parent_id"] = package.feed.id;
                    q[":weak_guid"] = item.weak_guid;
                    q[":title"] = item.title;
                    q[":link"] = item.link;
                    q[":description"] = item.description;
                    q[":content"] = item.content;
                    q[":rights"] = item.rights;
                    q[":publish_time"] = item.time_published.to_unix ();
                    q[":update_time"] = item.time_published.to_unix ();
                    q[":load_time"] = item.time_published.to_unix ();
                    q[":unread"] = item.unread ? 1 : 0;
                    q[":starred"] = item.starred ? 1 : 0;
                    q[":icon"] = item.icon;

                    var res = q.execute ();

                    // TODO: Can we tell if the insert succeeded?
                    item.unread = res.get_int ("unread") == 1;
                    item.starred = res.get_int ("starred") == 1;

                    q.reset ();
                }

                any_items_inserted = q_count.execute ().fetch_int (0) > item_count;
            }

            if (any_items_inserted) {
                var q = new Statement (db, "UPDATE feeds SET (last_new_content) = (:last_new_content) WHERE id = :id");
                q[":id"] = package.feed.id;
                q[":last_new_content"] = package.feed.last_new_content.to_unix ();
                q.execute ();
            }

            with (var q_delete = new Statement (db, "DELETE FROM enclosures WHERE item_guid = :item_guid"))
            with (var q_delete_authors = new Statement (db, "DELETE FROM enclosure_authors WHERE item_guid = :item_guid"))
            with (var q_insert = new Statement (db, "INSERT OR IGNORE INTO enclosures (feed_id, item_guid, guid, uri, name, description, icon, hash, hash_algo, embed_uri, player_uri, rights, medium, views, favorites, ratings, avg_rating, min_rating, max_rating, length, mimetype) VALUES (:feed_id, :item_guid, :guid, :uri, :name, :description, :icon, :hash, :hash_algo, :embed_uri, :player_uri, :rights, :medium, :views, :favorites, :ratings, :avg_rating, :min_rating, :max_rating, :length, :mimetype)"))
            with (var q_insert_author = new Statement (db, "INSERT OR IGNORE INTO enclosure_authors (enclosure_guid, item_guid, author_id) VALUES (:enclosure_guid, :item_guid, :author_id)")) {
                foreach (Item item in _items) {
                    q_delete[":item_guid"] = item.guid;
                    q_delete.execute ();
                    q_delete.reset ();

                    q_delete_authors[":item_guid"] = item.guid;
                    q_delete_authors.execute ();
                    q_delete_authors.reset ();

                    foreach (Attachment a in item.attachments) {
                        q_insert[":feed_id"] = package.feed.id;
                        q_insert[":item_guid"] = item.guid;
                        q_insert[":guid"] = a.guid;
                        q_insert[":uri"] = a.url;
                        q_insert[":name"] = a.name;
                        q_insert[":description"] = a.description;
                        q_insert[":icon"] = a.icon;
                        q_insert[":hash"] = a.hash;
                        q_insert[":hash_algo"] = a.hash_algo;
                        q_insert[":embed_uri"] = a.embed_uri;
                        q_insert[":player_uri"] = a.player_uri;
                        q_insert[":rights"] = a.rights;
                        q_insert[":medium"] = a.medium;
                        q_insert[":views"] = a.views;
                        q_insert[":favorites"] = a.favorites;
                        q_insert[":ratings"] = a.ratings;
                        q_insert[":avg_rating"] = a.avg_rating;
                        q_insert[":min_rating"] = a.min_rating;
                        q_insert[":max_rating"] = a.max_rating;
                        q_insert[":length"] = a.size;
                        q_insert[":mimetype"] = a.mimetype;
                        q_insert.execute ();
                        q_insert.reset ();

                        foreach (Person author in a.authors) {
                            q_insert_author[":enclosure_guid"] = a.guid;
                            q_insert_author[":item_guid"] = item.guid;
                            q_insert_author[":author_id"] = author.guid;
                            q_insert_author.execute ();
                            q_insert_author.reset ();
                        }
                    }
                }
            }

            with (var q_delete = new Statement (db, "DELETE FROM item_authors WHERE item_guid = :item_guid"))
            with (var q_insert = new Statement (db, "INSERT OR IGNORE INTO item_authors (item_guid, author_id) VALUES (:item_guid, :author_id)")) {
                foreach (Item item in _items) {
                    q_delete[":item_guid"] = item.guid;
                    q_delete.execute ();
                    q_delete.reset ();

                    foreach (Person author in item.authors) {
                        q_insert[":item_guid"] = item.guid;
                        q_insert[":author_id"] = author.guid;
                    }
                }
            }

            with (var q_update = new Statement (db, "REPLACE INTO cached_urls (url, last_good_result, last_modified, etag) VALUES (:url, :last_good_result, :last_modified, :etag)"))
            with (var q_delete = new Statement (db, "DELETE FROM cached_urls WHERE url = :url")) {
                if (package.feed_cache_info == null) {
                    q_delete[":url"] = package.feed.link;
                    q_delete.execute ();
                    q_delete.reset ();
                } else {
                    q_update[":url"] = package.feed_cache_info.url;
                    q_update[":last_good_result"] = package.feed_cache_info.last_good_result.to_unix ();
                    q_update[":last_modified"] = package.feed_cache_info.last_modified;
                    q_update[":etag"] = package.feed_cache_info.etag;
                    q_update.execute ();
                }

                if (package.feed.icon_uri != null) {
                    if (package.icon_cache_info == null) {
                        q_delete[":url"] = package.feed.icon_uri;
                        q_delete.execute ();
                        q_delete.reset ();
                    } else {
                        q_update[":url"] = package.icon_cache_info.url;
                        q_update[":last_good_result"] = package.icon_cache_info.last_good_result.to_unix ();
                        q_update[":last_modified"] = package.icon_cache_info.last_modified;
                        q_update[":etag"] = package.icon_cache_info.etag;
                        q_update.execute ();
                    }
                }
            }

            return Status.COMPLETED;
        }

        private ArrayList<Item> _items = new ArrayList<Item> ();
        private ArrayList<Person> _people = new ArrayList<Person> ();
    }
}
