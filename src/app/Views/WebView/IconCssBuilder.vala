using Gdk;
using Gee;

namespace Singularity {
    public class IconCssBuilder {
        public async void add_themed_icon (string icon_name) {
            try {
                string icon_file = IconLocator.get_icon (icon_name, 16);
                Pixbuf buf = new Pixbuf.from_file (icon_file);
                _icons[icon_name] = IconUriBuilder.get_icon_pixbuf_uri (buf);
            } catch (Error e) {
                warning ("Failed to load icon %s: %s", icon_name, e.message);
                //_icons[icon_name] = "";
            }
            _cached_css = null;
        }

        public async void add_icon_for_file (string key, File icon_file) {
            _icons[key] = yield IconUriBuilder.get_icon_uri_async (icon_file);
            _cached_css = null;
        }

        public string get_css () {
            if (_cached_css == null) {
                generate_css ();
            }

            return _cached_css;
        }

        private void generate_css () {
            var builder = new StringBuilder ("html{");
            foreach (Map.Entry<string,string> icon in _icons) {
                builder.append_printf ("--%s:%s;", icon.key, icon.value);
            }
            builder.append ("}");
            _cached_css = builder.str;
        }

        private string? _cached_css = null;
        private HashMap<string,string> _icons = new HashMap<string,string> ();
    }
}
