using Gee;

namespace Singularity.Db {
    /** Represents a prepared SQL statement */
    public class Statement : Object, IDbExecutable {
        /** The SQL code to execute */
        public string sql { get; construct set; }

        public Statement (IDbTarget target, string sql) {
            Object (sql: sql);

            _target = target;
            int res = target.db.prepare_v2 (sql, sql.length, out _statement);
            if (res != Sqlite.OK) {
                error ("Failed to prepare statement %s: %s", sql, target.db.errmsg ());
            }
        }

        ~Statement () {
            _statement.reset ();
        }

        /**
         * Execute this object's database code and get the result
         *
         * @return The result from the database
         */
        public IDbResult execute () {
            int res = _statement.step ();
            if (res != Sqlite.OK && res != Sqlite.ROW && res != Sqlite.DONE) {
                error ("Failed to execute statement %s: %s", sql, _statement.db_handle ().errmsg ());
            }
            return new StatementDbResult (_statement, res);
        }

        /**
         * Execute an INSERT statement and return the last insert rowid
         *
         * @return the last inserted rowid
         */
        public int64 execute_insert () {
            while (_statement.step () == Sqlite.ROW);

            return _target.db.last_insert_rowid ();
        }

        public void reset () {
            _statement.reset ();
        }

        /**
         * Bind a value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public new void @set (string name, Value? value) {
            if (value == null) {
                set_string (name, null);
            } else {
                Type type = value.type ();
                if (type == typeof (string)) {
                    set_string (name, value.get_string ());
                } else if (type == typeof (int)) {
                    set_int (name, value.get_int ());
                } else if (type == typeof (int64)) {
                    set_int64 (name, value.get_int64 ());
                } else if (type == typeof (double)) {
                    set_double (name, value.get_double ());
                } else {
                    error ("Failed to set %s %s: Unsupported type", value?.type_name () ?? "null", name);
                }
            }
        }

        /**
         * Bind a blob value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public void set_blob (string key, uint8[] value) {
            int param = find_param (key);
            ensure_param (param, "Failed to set blob %s".printf (key));
            _statement.bind_blob (param, value, value.length);
        }

        /**
         * Bind an int value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public void set_int (string key, int value) {
            int param = find_param (key);
            ensure_param (param, "Failed to set int %s on %s".printf (key, sql));
            _statement.bind_int (param, value);
        }

        /**
         * Bind an int64 value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public void set_int64 (string key, int64 value) {
            int param = find_param (key);
            ensure_param (param, "Failed to set int64 %s on %s".printf (key, sql));
            _statement.bind_int64 (param, value);
        }

        /**
         * Bind an double value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public void set_double (string key, double value) {
            int param = find_param (key);
            ensure_param (param, "Failed to set double %s on %s".printf (key, sql));
            _statement.bind_double (param, value);
        }

        /**
         * Bind a string value to the given key
         *
         * @param key The key to bind to
         * @param value The value to bind
         */
        public void set_string (string key, string? value) {
            int param = find_param (key);
            ensure_param (param, "Failed to set string %s on %s".printf (key, sql));

            if (value != null) {
                _statement.bind_text (param, value, value.length);
            } else {
                _statement.bind_null (param);
            }
        }

        private int find_param (string key) {
            return _statement.bind_parameter_index (key);
        }

        private void ensure_param (int param, string message) {
            if (param <= 0 || param > _statement.bind_parameter_count ()) {
                var parameters = new StringBuilder ();
                for (int i = 1; i <= _statement.bind_parameter_count (); i++) {
                    parameters.append_printf ("\n  [%d] %s", i, _statement.bind_parameter_name (i));
                }

                error ("%s: Parameter %d not found. Available parameters:%s", message, param, parameters.str);
            }
        }

        private Sqlite.Statement _statement;
        private IDbTarget _target;
    }
}
