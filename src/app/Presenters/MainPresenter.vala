using Gee;
using Singularity.Db;

namespace Singularity {
    /** Presenter for the MainWindow view */
    public class MainPresenter : Object {
        public MainPresenter (Database db) {
            _database = db;
        }

        /**
         * Marks all provided items as read
         *
         * @param items The items to update
         */
        public async void view_items (owned Item[] items) {
            // Mark items as read
            yield _database.execute_request_async (new ItemViewRequest (items));
            foreach (Item i in items) {
                i.unread = false;
            }

            // Update all feeds
            var feeds = new HashSet<Feed> ();
            foreach (Item i in items) {
                if (i.owner != null && feeds.add (i.owner)) {
                    yield _database.execute_request_async (new GetUnreadCountRequest (i.owner));
                }
            }
        }

        private Database _database;
    }
}
