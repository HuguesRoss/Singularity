using Singularity.Db;

namespace Singularity {
    public abstract class SimpleItem : Object {
        public int id { get; construct; }
        public string name { get; construct; }

        protected SimpleItem (int n_id, string n_name) { Object (id:n_id, name:n_name); }
        protected SimpleItem.from_query (string type_name, IDbResult q) {
            int n_id;
            string n_name;
            n_name = q.get_string ("name");
            n_id = q.get_int ("id");
            Object (id:n_id, name:n_name);
        }

        public Statement save_query (IDbTarget db) {
            StringBuilder builder = new StringBuilder ("INSERT INTO ");
            builder.append_printf ("%s (id, name) VALUES (%d, '%s')", get_table (), id, name.replace ("\'", "\'\'"));
            var q = new Statement (db, builder.str);
            return q;
        }

        public abstract string get_table ();
    }
}
