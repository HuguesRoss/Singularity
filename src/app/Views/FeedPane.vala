using Gdk;
using Gtk;

namespace Singularity {
    // The contents of the left pane, which holds the treeview for browsing feeds
    [GtkTemplate (ui="/net/huguesross/Singularity/Views/FeedPane.ui")]
    public class FeedPane : Widget {
        public IFeedListNode? selected_node { get { return _selected; } }
        protected Object? selected {
            get { return _selected; }
            set {
                var row = value as TreeListRow;
                if (row == null) {
                    _selected = null;
                } else {
                    _selected = row.item as IFeedListNode;
                }
                notify_property ("selected_node");
            }
        }
        private IFeedListNode? _selected;
        private FeedDataEntry? _menu_selected;

        public TreeListModel feeds_model { get; private set; }

        public Sorter item_sorter { get; construct set; }

        public TreeFilter item_filter { get; construct set; }

        public bool is_filtering { get; set; }

        construct {
            item_sorter = new StringSorter (new PropertyExpression (typeof (IFeedListNode), null, "sort-key")) {
                ignore_case = true,
            };
            _string_filter = new StringFilter (new PropertyExpression (typeof (IFeedListNode), null, "title")) {
                ignore_case = true,
            };
            item_filter = new TreeFilter (_string_filter);

            add_actions ();

            context_menu_gesture = new GestureClick () {
                button = Gdk.BUTTON_SECONDARY,
                exclusive = true,
            };
            context_menu_gesture.released.connect (on_context_menu);
           tree_view.add_controller (context_menu_gesture);

            _drag_source = new DragSource () {
                actions = DragAction.MOVE,
            };
            _drag_source.prepare.connect (prepare_drag);
            tree_view.add_controller (_drag_source);
            _drop_target = new DropTarget (typeof (CollectionNode), DragAction.MOVE);
            _drop_target.drop.connect (drop);
            tree_view.add_controller (_drop_target);
        }

        ~FeedPane () {
            WidgetUtils.cleanup_children (this);
        }

        public void init (MainView owner_view, SingularityApp app) {
            owner = owner_view;

            feed_data = app.get_feed_store ();
            feeds_model = feed_data.tree_model;
        }

        /** Expand the base category (All Feeds) */
        public void expand_base () {
            feeds_model.get_row (0).expanded = true;
        }

        /**
        * Jumps to the previous feed in the list
        *
        * @param next_unread Jumps to the previous feed with unread items
        */
        public void prev_feed (bool next_unread) {
            uint cursor = selection.selected;
            int unread_count = 0;
            do {
                if (cursor == 0) {
                    cursor = selection.n_items;
                }
                cursor--;
                TreeListRow? row = selection.get_item (cursor) as TreeListRow;
                if (row != null && row.item is IFeedListNode) {
                    unread_count = ((IFeedListNode)row.item).unread_count;
                }
            } while (next_unread && (unread_count == 0 && cursor != selection.selected));
            selection.selected = cursor;
        }

        /**
        * Jumps to the next feed in the list
        *
        * @param next_unread Jumps to the next feed with unread items
        */
        public void next_feed (bool next_unread) {
            uint cursor = selection.selected;
            int unread_count = 0;
            do {
                cursor++;
                if (cursor >= selection.n_items) {
                    cursor = 0;
                }
                TreeListRow? row = selection.get_item (cursor) as TreeListRow;
                if (row != null && row.item is IFeedListNode) {
                    unread_count = ((IFeedListNode)row.item).unread_count;
                }
            } while (next_unread && (unread_count == 0 && cursor != selection.selected));
            selection.selected = cursor;
        }

        public void select_bookmarks () {
            uint cursor = 0;
            TreeListRow? node = selection.get_item (cursor) as TreeListRow;
            while (node != null) {
                if (node.item is BookmarksNode) {
                    selection.selected = cursor;
                    return;
                }
                cursor++;
                node = selection.get_item (cursor) as TreeListRow;
            }
        }

        private void add_actions () {
            SimpleActionGroup feed_menu_group = new SimpleActionGroup ();
            SimpleActionGroup collection_menu_group = new SimpleActionGroup ();

            SimpleAction act_properties = new SimpleAction ("properties", null);
            act_properties.activate.connect (show_properties);
            act_properties.set_enabled (true);
            feed_menu_group.add_action (act_properties);

            SimpleAction act_update = new SimpleAction ("update", null);
            act_update.activate.connect (() => {
                if (_menu_selected is Feed)
                    owner.updates.request_update ((Feed)_menu_selected);
            });
            act_update.set_enabled (true);
            feed_menu_group.add_action (act_update);

            SimpleAction act_unsubscribe = new SimpleAction ("unsubscribe", null);
            act_unsubscribe.activate.connect (() => {
                if (_menu_selected is Feed)
                    owner.unsub_requested ((Feed)_menu_selected);
            });
            act_unsubscribe.set_enabled (true);
            feed_menu_group.add_action (act_unsubscribe);

            SimpleAction act_collection_new = new SimpleAction ("new", null);
            act_collection_new.activate.connect (() => {
                if (_menu_selected is Feed) {
                    owner.new_collection_requested (_menu_selected.parent);
                } else {
                    owner.new_collection_requested ((FeedCollection)_menu_selected);
                }
            });
            act_collection_new.set_enabled (true);
            collection_menu_group.add_action (act_collection_new);

            SimpleAction act_collection_rename = new SimpleAction ("rename", null);
            act_collection_rename.activate.connect (() => {
                if (_menu_selected is FeedCollection) {
                    IFeedListNode node = feed_data.get_node (_menu_selected);
                    foreach (var child in WidgetUtils.get_children (tree_view)) {
                        var list_item = child.get_first_child () as FeedListItem;
                        if (list_item != null && list_item.node == node && list_item.can_edit_title) {
                            list_item.is_editing_title = true;
                        }
                    }
                }
            });
            act_collection_rename.set_enabled (true);
            collection_menu_group.add_action (act_collection_rename);

            SimpleAction act_collection_delete = new SimpleAction ("delete", null);
            act_collection_delete.activate.connect (() => {
                if (_menu_selected is FeedCollection) {
                    owner.delete_collection_requested ((FeedCollection)_menu_selected);
                }
            });
            act_collection_delete.set_enabled (true);
            collection_menu_group.add_action (act_collection_delete);

            this.insert_action_group ("feed", feed_menu_group);
            this.insert_action_group ("collection", collection_menu_group);
        }

        private void show_properties () {
            if (_menu_selected is Feed) {
                new FeedPropertiesWindow ((Feed)_menu_selected).present (this);
            }
        }

        private void on_context_menu (int _, double x, double y) {
            _menu_selected = null;

            Popover menu = base_menu;
            Rectangle rect = { (int)x, (int)y, 1, 1 };
            TreeListRow? row = WidgetUtils.treelistrow_at (tree_view, x, y);
            if (row != null) {
                var collection = row.item as CollectionNode;
                if (collection != null) {
                    _menu_selected = collection.data;

                    if (_menu_selected is Feed) {
                        menu = feed_menu;
                    } else if (_menu_selected is FeedCollection && _menu_selected.id != -1) {
                        menu = collection_menu;
                    }
                }
            }

            menu.pointing_to = rect;
            menu.popup ();
        }

        private ContentProvider? prepare_drag (double x, double y) {
            TreeListRow? row = WidgetUtils.treelistrow_at (tree_view, x, y);

            if (row == null || !(row.item is CollectionNode)) {
                return null;
            }

            var node = (CollectionNode)row.item;
            var @value = Value (typeof (CollectionNode));
            @value.set_object (node);
            return new ContentProvider.for_value (@value);
        }

        private bool drop (Value @value, double x, double y) {
            CollectionNode node = @value.get_object () as CollectionNode;
            if (node == null) {
                return false;
            }

            TreeListRow? row = WidgetUtils.treelistrow_at (tree_view, x, y);
            CollectionNode? dest = null;
            int dest_id = -1;
            if (row != null && row.item is CollectionNode) {
                dest = (CollectionNode)row.item;
                dest_id = dest.data.id;
            }

            // Same parent? Can't move
            if (node.data.parent_id == dest_id) {
                return false;
            }

            feed_data.move_node (node, dest);

            return true;
        }

        [GtkCallback]
        private void on_search_changed (SearchEntry entry) {
            bool has_text = entry.text != "";
            feed_data.foreach (row => {
                row.expanded = has_text;
            });
            if (!has_text) {
                expand_base ();
            }

            _string_filter.search = entry.text;
        }

        [GtkCallback]
        private void on_stop_search (SearchEntry entry) {
            feed_data.foreach (row => {
                row.expanded = false;
            });
            expand_base ();
            is_filtering = false;
        }

        private FeedListTreeStore feed_data;
        private unowned MainView owner;

        [GtkChild]
        private unowned PopoverMenu base_menu;
        [GtkChild]
        private unowned PopoverMenu feed_menu;
        [GtkChild]
        private unowned PopoverMenu collection_menu;
        [GtkChild]
        private unowned ListView tree_view;
        [GtkChild]
        private unowned SingleSelection selection;

        private GestureClick context_menu_gesture;

        private DragSource _drag_source;
        private DropTarget _drop_target;
        private StringFilter _string_filter;
    }
}
