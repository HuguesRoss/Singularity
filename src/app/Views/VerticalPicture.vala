using Gtk;
using Gdk;

namespace Singularity {
    public class VerticalPicture : Widget {
        public int max_ratio {
            get { return _max_ratio; }
            set {
                if (_max_ratio != value) {
                    _max_ratio = value;
                    notify_property ("max-ratio");
                    queue_resize ();
                }
            }
        }
        private int _max_ratio = 5;
        public Paintable paintable {
            get { return _paintable; }
            set {
                if (_paintable != value) {
                    _paintable = value;
                    _picture.paintable = _paintable;
                    notify_property ("paintable");
                    queue_resize ();
                }
            }
        }
        private Paintable _paintable;

        construct {
            _picture = new Picture () {
                can_shrink = true,
                content_fit = ContentFit.COVER,
                halign = Align.FILL,
                valign = Align.FILL,
            };
            _picture.set_parent (this);
        }

        ~VerticalPicture () {
            _picture.unparent ();
        }

        public override SizeRequestMode get_request_mode () {
            return SizeRequestMode.HEIGHT_FOR_WIDTH;
        }

        public override void measure (Orientation o, int for_size, out int minimum, out int natural, out int minimum_baseline, out int natural_baseline) {
            if (paintable == null) {
                minimum = 0;
                natural = 0;
                minimum_baseline = -1;
                natural_baseline = -1;
                return;
            }

            int w = paintable.get_intrinsic_width ();
            int h = paintable.get_intrinsic_height ();
            float ratio = float.min ((float)h / (float)w, max_ratio);

            minimum = (int)(ratio * for_size);
            natural = (int)(ratio * for_size);
            minimum_baseline = -1;
            natural_baseline = -1;
        }

        public override void size_allocate (int width, int height, int baseline) {
            Allocation a = {
                0,
                0,
                width,
                height,
            };
            _picture.allocate_size (a, -1);
        }

        private Picture _picture;
    }
}
