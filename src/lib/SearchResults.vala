namespace Singularity {
    public class SearchResults : Object {
        public ItemQuery query { get; construct set; }
        public ItemCache cache { get; construct set; }
        public int loaded { get { return _results_items.size; } }
        public int total { get { return _results.size; } }

        public SearchResults (ItemQuery query, ItemCache cache, Gee.ArrayList<string> result_ids) {
            Object (query: query, cache: cache);
            _results = result_ids;
        }

        public async Gee.Collection<Item> get_items (int limit, int cursor) {
            if (cursor >= total || limit <= 0) {
                return Gee.Collection.empty<Item> ();
            }

            int adjusted_cursor = int.min (cursor, loaded);
            int end = int.min (cursor + limit, total);
            for (int i = adjusted_cursor; i < end; ++i) {
                string guid = _results[i];
                Item item;
                if (!cache.try_get (guid, out item)) {
                    item = yield cache.load_item (guid);
                }
                _results_items.add (item);
            }
            return _results_items.slice (cursor, end) ?? new Gee.ArrayList<Item> ();
        }

        private Gee.ArrayList<string> _results;
        private Gee.ArrayList<Item> _results_items = new Gee.ArrayList<Item> ();
    }
}
