using Gtk;

namespace Singularity {
    /**
    * ItemView that displays items in a grid.
    * Clicking any grid square causes the full item to pop up over the view.
    */
    [GtkTemplate (ui = "/net/huguesross/Singularity/Views/GridItemView.ui")]
    public class GridItemView : Widget, IItemView {
        public string title {
            get { return _title; }
            protected set {
                if (_title != value) {
                    _title = value;

                    notify_property ("title");
                }
            }
        }
        private string _title;
        public string empty_description {
            get { return _empty_description; }
            protected set {
                if (_empty_description != value) {
                    _empty_description = value;

                    notify_property ("title");
                }
            }
        }
        private string _empty_description;
        public bool has_items { get { return _results != null && _results.total > 0; } }
        public TextureCache icons { get; set; }
        public IconCssBuilder css_icons {
            get { return _css_icons; }
            set {
                if (_css_icons != value) {
                    _css_icons = value;
                    notify_property ("css-icons");
                }
            }
        }
        private IconCssBuilder _css_icons;

        public SingleSelection selection_model { get; construct; }

        public Widget title_widget { get; set; }
        public bool use_csd { get; set; }
        public Gee.Iterable<IHeaderItem> end_items { get; set; }
        public Gee.Iterable<IHeaderItem> page_end_items { get; set; }

        public bool use_masonry_layout {
            get { return _view_widget == items_mason; }
            set {
                view_widget = value ? (Widget)items_mason : (Widget)items_grid;
            }
        }

        private Scrollable view_scroll {
            get { return (Scrollable)_view_widget; }
        }

        private Widget view_widget {
            get {
                return _view_widget;
            }
            set {
                if (_view_widget != value) {
                    if (_view_widget != null) {
                        view_scroll.vadjustment.notify.disconnect (on_scroll);
                        _view_widget.unparent ();
                    }

                    _view_widget = value;

                    if (_view_widget != null) {
                        scroll.child = _view_widget;
                        view_scroll.vadjustment.notify.connect (on_scroll);
                    }
                }
            }
        }
        private Widget _view_widget = null;

        construct {
            selection_model = new SingleSelection (_items);
            install_action ("read", null, (w) => {
                var view = ((GridItemView)w);
                var item = view.selection_model.selected_item as Item;
                if (item != null) {
                    view.item_read_toggle (item);
                }
            });
            install_action ("bookmark", null, (w) => {
                var view = ((GridItemView)w);
                var item = view.selection_model.selected_item as Item;
                if (item != null) {
                    view.item_star_toggle (item);
                }
            });
            _key_controller = new EventControllerKey () {
                propagation_phase = PropagationPhase.CAPTURE,
            };
            _key_controller.key_pressed.connect (on_key_press);
            add_controller (_key_controller);

            view_widget = items_grid;
        }

        ~GridItemView () {
            view_widget = null;

            WidgetUtils.cleanup_children (this);
        }

        /**
         * Display a list of items
         *
         * @param items The items to display
         */
        public void view_items (SearchResults results) {
            _requesting_items = false;
            _results = results;
            notify_property ("has-items");
            view_scroll.vadjustment.set_value (0);

            page_cursor = 0;
            read_cursor = 0;

            _items.remove_all ();

            request_items.begin ();
        }

        /**
         * Focus this view
         */
        public void focus_view () {
            view_widget.grab_focus ();
        }

        /**
         * Refresh the content of this view
         */
        public void refresh_view () {
            wrapper.refresh_view ();
        }

        /**
         * Mark all visible items as read
         */
        public void read_all () {
            items_viewed (WidgetUtils.listmodel_to_array<Item> (_items));
        }

        private bool on_key_press (uint keyval, uint keycode, Gdk.ModifierType state) {
            if (state == Gdk.ModifierType.NO_MODIFIER_MASK) {
                switch (keyval) {
                    case Gdk.Key.m:
                        activate_action ("read", null);
                        return true;
                    case Gdk.Key.b:
                        activate_action ("bookmark", null);
                        return true;
                }
            }

            return false;
        }

        [GtkCallback]
        private void on_scroll_end (PositionType p) {
            if (p == PositionType.BOTTOM) {
                request_items.begin ();
            }
        }

        private void read_in_viewport_grid (float top, float bottom, Gee.ArrayList<Item> items) {
            uint i = 0;
            Graphene.Rect bounds = {};
            for (Widget child = view_widget.get_first_child (); child != null; child = child.get_next_sibling ()) {
                if (i >= read_cursor && child.compute_bounds (view_widget, out bounds)) {
                    float child_bottom = bounds.get_bottom_left ().y;
                    if (child_bottom >= top) {
                        if (child_bottom <= bottom) {
                            read_cursor = i + 1;
                            var item = selection_model.get_item (i) as Item;
                            if (item != null) {
                                items.add (item);
                            }
                        } else {
                            break;
                        }
                    }
                }
                i++;
            }
        }

        private void read_in_viewport_masonry (float top, float bottom, Gee.ArrayList<Item> items) {
            Graphene.Rect bounds = {};
            for (uint i = 0; i < items_mason.n_columns; ++i) {
                foreach (Widget child in items_mason.get_column_widgets (i)) {
                    if (child.compute_bounds (this, out bounds)) {
                        float child_bottom = bounds.get_bottom_left ().y;
                        if (child_bottom >= top) {
                            if (child_bottom <= bottom) {
                                var item = ((MasonryItemWrapper)child).item as Item;
                                if (item != null && item.unread) {
                                    items.add (item);
                                }
                            } else {
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void on_scroll () {
            if (_read_on_scroll_blocked) {
                return;
            }

            Graphene.Rect scroll_bounds = {};
            if (!scroll.compute_bounds (this, out scroll_bounds)) {
                return;
            }
            float top = scroll_bounds.get_top_left ().y;
            float bottom = scroll_bounds.get_bottom_left ().y;


            Adjustment adj = view_scroll.vadjustment;
            var items = new Gee.ArrayList<Item> ();
            if (use_masonry_layout) {
                read_in_viewport_masonry (top, bottom, items);
            } else {
                read_in_viewport_grid (top, bottom, items);
            }

            if (!items.is_empty) {
                items_viewed (items.to_array ());
            }

            if (_view_widget == items_mason && adj.value + adj.page_increment > items_mason.min_column_height) {
                request_items.begin ();
            }
        }

        [GtkCallback]
        private void activated (uint index) {
            var item = selection_model.get_item (index) as Item;
            if (item == null) {
                return;
            }

            var stack = WidgetUtils.find_parent<Adw.NavigationView> (this);
            if (stack == null) {
                return;
            }
            wrapper.item = item;
            stack.push (new Adw.NavigationPage (wrapper, item.title));
            items_viewed ({ item });
        }

        [GtkCallback]
        private void on_toggle_read () {
            if (wrapper.item == null) {
                return;
            }
            item_read_toggle (wrapper.item);
        }

        [GtkCallback]
        private void on_toggle_star () {
            if (wrapper.item == null) {
                return;
            }

            item_star_toggle (wrapper.item);
        }

        [GtkCallback]
        private Widget create_masonry_item (Object item) {
            return new MasonryItem () {
                item = item as Item,
            };
        }

        private async void request_items () {
            var results = _results;
            if (_requesting_items) {
                return;
            }

            _requesting_items = true;
            Gee.Iterable<Item> items = yield get_items (results, (int)_items.n_items);

            // If the results no longer match, the content has changed and we should bail out
            if (_results != results) {
                return;
            }

            _read_on_scroll_blocked = true;
            bool has_items = false;
            foreach (Item item in items) {
                _items.append (item);
                has_items = true;
            }
            if (has_items) {
                Timeout.add_seconds (1, () => {
                    _read_on_scroll_blocked = false;
                    on_scroll ();
                    return false;
                });
            } else {
                _read_on_scroll_blocked = false;
            }
            _requesting_items = false;
        }

        [GtkChild]
        private unowned ScrolledWindow scroll;

        [GtkChild]
        private unowned GridView items_grid;

        [GtkChild]
        private unowned MasonryView items_mason;

        [GtkChild]
        private unowned SingleItemWrapper wrapper;

        private int page_cursor = 0;
        private GLib.ListStore _items = new GLib.ListStore (typeof (Item));
        private SearchResults _results;
        private bool _requesting_items = true;
        private bool _read_on_scroll_blocked = false;
        private EventControllerKey _key_controller;
        private uint read_cursor = 0;
    }
}
