ALTER TABLE "enclosures" ADD COLUMN "description" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "icon" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "hash" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "hash_algo" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "embed_uri" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "player_uri" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "rights" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "medium" VARCHAR;
ALTER TABLE "enclosures" ADD COLUMN "views" INTEGER;
ALTER TABLE "enclosures" ADD COLUMN "favorites" INTEGER;
ALTER TABLE "enclosures" ADD COLUMN "ratings" INTEGER;
ALTER TABLE "enclosures" ADD COLUMN "avg_rating" NUMBER;
ALTER TABLE "enclosures" ADD COLUMN "min_rating" NUMBER;
ALTER TABLE "enclosures" ADD COLUMN "max_rating" NUMBER;

CREATE TABLE "enclosure_authors" (
    enclosure_guid TEXT NOT NULL,
    author_id   TEXT NOT NULL,
    PRIMARY KEY (enclosure_guid, author_id),
    FOREIGN KEY (enclosure_guid) REFERENCES enclosures (guid)
    FOREIGN KEY (author_id) REFERENCES people (guid)
);
