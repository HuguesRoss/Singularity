namespace Singularity {
    /** Represents a request for loading data from a URI */
    public interface IDataLoader : Object {
        /** Whether or not this request generated an error */
        public bool error_exists { get { return error_message != null; } }

        /** The error message for this request, if any */
        public abstract string? error_message { get; protected set; }

        /** The request object */
        public abstract DataLoadRequest request { get; set; }

        /** Container providing additional contextual data from the application */
        public abstract UpdateContext update_context { get; set; }

        /** Returns the base URI that this request is loading from */
        public abstract string get_base_uri ();

        /**
         * Sends the request synchronously
         *
         * @return true if the request succeeded; otherwise, false
         */
        public DataLoadResponse send () {
            var loop = new MainLoop ();

            DataLoadResponse result = null;
            send_async.begin ((obj, ret) => {
                result = send_async.end (ret);
                loop.quit ();
            });

            loop.run ();
            return result;
        }

        /**
         * Sends the request asynchronously
         *
         * @return true if the request succeeded; otherwise, false
         */
        public abstract async DataLoadResponse send_async ();
    }
}
