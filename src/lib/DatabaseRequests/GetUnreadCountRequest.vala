using Singularity.Db;

namespace Singularity {
    /** DatabaseRequest for querying a feed's unread item count */
    public class GetUnreadCountRequest : SimpleRequest {
        /** The feed to query */
        public Feed feed { get; construct; }

        /** Holds the unread count after execution */
        public int unread_count { get; private set; }

        public GetUnreadCountRequest (Feed f) {
            Object (feed: f);

            _name = "Get Unread for %s".printf (f.title);
        }

        public override Statement build_query (IDbTarget db) {
            var q = new Statement (db, _sql);
            q[":id"] = feed.id;

            return q;
        }

        public override IRequest.Status process_result (IDbResult res) {
            feed.unread_count = res.get_int ("unread_count");
            return IRequest.Status.COMPLETED;
        }

        private string _sql = "SELECT sum (items.unread) AS `unread_count` FROM `items` WHERE `parent_id` = :id";
    }
}
