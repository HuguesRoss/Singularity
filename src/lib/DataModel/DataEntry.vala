using Singularity.Db;

namespace Singularity {
    public abstract class DataEntry : Object {
        public int id { get { return _id; } }
        public bool is_inserted { get { return id != -1; } }

        protected DataEntry () {
            _id = -1;
        }

        protected DataEntry.from_record (IDbResult r) {
            _id = r.get_int ("id");
            build_from_record (r);
        }

        /** Creates a query for inserting this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public virtual Statement? insert (IDbTarget q) {
            return null;
        }

        /** Creates a query for updating this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public virtual Statement? update (IDbTarget q) {
            return null;
        }

        /** Creates a query for removing this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public virtual Statement? remove (IDbTarget q) {
            return null;
        }

        /** Populate data from a database record
         *
         * @param r The record to read
         */
        protected abstract void build_from_record (IDbResult r);

        protected void set_id (int new_id) {
            _id = new_id;
        }

        private int _id = -1;
    }

    public abstract class DataEntryGuid : Object {
        public string guid { get { return _guid; } }
        public bool is_inserted { get { return guid != null; } }

        protected DataEntryGuid () {
        }

        protected DataEntryGuid.from_record (IDbResult r) {
            _guid = r.get_string ("guid");
            build_from_record (r);
        }

        /** Creates a query for inserting this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public virtual Statement? insert (IDbTarget q) {
            return null;
        }

        /** Creates a query for updating this entry
         *
         * @param q The IDbTarget to run this query against
         */
        public virtual Statement? update (IDbTarget q) {
            return null;
        }

        /** Creates a query for removing this entry
         *
         * @param q The ueryable to run this query against
         */
        public virtual Statement? remove (IDbTarget q) {
            return null;
        }

        /** Populate data from a database record
         *
         * @param r The record to read
         */
        protected abstract void build_from_record (IDbResult r);

        protected void set_guid (string new_guid) {
            _guid = new_guid;
        }

        private string _guid = null;
    }
}
