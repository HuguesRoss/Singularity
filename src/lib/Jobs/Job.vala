namespace Singularity {
    public abstract class Job : Object {
        public enum State {
            FRESH = 0,
            WAITING,
            RUNNING,
            COMPLETED,
            CANCELLED,
        }

        public enum Result {
            SUCCEEDED,
            FAILED,
            CANCELLED,
        }

        delegate void ResultHandler (Result res);

        public State state {
            get {
                lock (_lock) {
                    return _state;
                }
            }
            internal set {
                bool changed = false;
                lock (_lock) {
                    if (_state != value) {
                        _state = value;
                        changed = true;
                    }
                }

                if (changed) {
                    notify_property ("state");
                }
            }
        }
        private State _state;

        public Result result {
            get {
                lock (_lock) {
                    return _result;
                }
            }
            protected set {
                bool changed = false;
                lock (_lock) {
                    if (_result != value) {
                        _result = value;
                        changed = true;
                    }
                }

                if (changed) {
                    notify_property ("result");
                }
            }
        }
        private Result _result;

        public signal void finished (Result res);

        public void cancel () {
            bool changed = false;
            lock (_lock) {
                if (_state <= State.RUNNING) {
                    changed = true;
                    _state = State.CANCELLED;
                }
            }
            if (changed) {
                notify_property ("state");
            }
        }

        public async Result wait () {
            Job.Result res = 0;
            SourceFunc func = wait.callback;
            ulong id = 0;

            lock (_lock) {
                if (_state > State.RUNNING) {
                    return _result;
                }

                id = finished.connect (r => {
                    res = r;
                    Idle.add ((owned)func);
                });
            }

            yield;

            lock (_lock) {
                disconnect (id);
            }

            return res;
        }

        public async void run () {
            lock (_lock) {
                if (_state >= State.RUNNING) {
                    return;
                }

                _state = State.RUNNING;
            }
            notify_property ("state");

            var res = yield run_internal ();

            lock (_lock) {
                if (_state == State.CANCELLED) {
                    res = Result.CANCELLED;
                } else {
                    _state = State.COMPLETED;
                }
                _result = res;
                finished (res);
            }
            notify_property ("result");
            notify_property ("state");
        }

        protected abstract async Result run_internal ();

        private Object _lock = new Object ();
    }
}
